#ifndef LUDII_UTILITY_MATRIX_CONTAINER_INL
#define LUDII_UTILITY_MATRIX_CONTAINER_INL

template <typename T>
MatrixContainer<T>::MatrixContainer()
{
	_columnCount = 0;
	_rowCount = 0;
}

template <typename T>
MatrixContainer<T>::MatrixContainer( ludii::uInt32 columns, ludii::uInt32 rows, const T& defaultValue )
{
	create( columns, rows, defaultValue );
}

//template <typename T>
//MatrixContainer<T>::~MatrixContainer()
//{
//}

template <typename T>
void MatrixContainer<T>::create( ludii::uInt32 columns, ludii::uInt32 rows, const T& defaultValue )
{
	_matrix.clear();
	_columnCount = columns;
	_rowCount = rows;

	std::vector<T> defaultRow( columns, defaultValue );
	_matrix.resize( rows, defaultRow );
}

template <typename T>
T& MatrixContainer<T>::getElementAt( ludii::uInt32 column, ludii::uInt32 row )
{
	return _matrix[row][column];
}

template <typename T>
const T& MatrixContainer<T>::getElementAtConst( ludii::uInt32 column, ludii::uInt32 row ) const
{
	return _matrix[row][column];
}

template <typename T>
void MatrixContainer<T>::setElementAt( ludii::uInt32 column, ludii::uInt32 row, const T& value )
{
	_matrix[row][column] = value;
}

template <typename T>
void MatrixContainer<T>::setAllElements( const T& value )
{
	for( ludii::uInt32 x = 0; x < _columnCount; x++ )
	{
		for( ludii::uInt32 y = 0; y < _rowCount; y++ )
		{
			setElementAt( x, y, value );
		}
	}
}

template <typename  T>
std::vector<T>& MatrixContainer<T>::getRow( ludii::uInt32 row )
{
	return _matrix[row];
}

template <typename  T>
const std::vector<T>& MatrixContainer<T>::getRowConst( ludii::uInt32 row ) const
{
	return _matrix[row];
}

template <typename T>
void MatrixContainer<T>::setColumnCount( ludii::uInt32 columnCount, const T& defaultValueOfNewElements )
{
	_columnCount = columnCount;

	for ( ludii::uInt32 i = 0; i < _matrix.size(); i++ )
	{
		_matrix[i].resize( columnCount, defaultValueOfNewElements );
	}
}

template <typename T>
ludii::uInt32 MatrixContainer<T>::getColumnCount() const
{
	return _columnCount;
}

template <typename T>
void MatrixContainer<T>::setRowCount( ludii::uInt32 rowCount, const T& defaultValueOfNewElements )
{
	_matrix.resize( rowCount, defaultValueOfNewElements );
	for ( ludii::uInt32 i = 0; i < _matrix.size(); i++ )
	{
		_matrix[i].resize( _columnCount, defaultValueOfNewElements );
	}
}

template <typename T>
ludii::uInt32 MatrixContainer<T>::getRowCount() const
{
	return _rowCount;
}

template <typename T>
void MatrixContainer<T>::appendColumnsToTheLeft( ludii::uInt32 columnCount, const T& defaultValueOfNewElements )
{
	_columnCount += columnCount;

	std::vector<T> newVector( columnCount, defaultValueOfNewElements );
	for ( ludii::uInt32 i = 0; i < _matrix.size(); i++ )
	{
		_matrix[i].insert( _matrix.begin(), newVector.begin(), newVector.end() );
	}
}

template <typename T>
void MatrixContainer<T>::appendColumnsToTheRight( ludii::uInt32 columnCount, const T& defaultValueOfNewElements )
{
	_columnCount += columnCount;

	std::vector<T> newVector( columnCount, defaultValueOfNewElements );
	for ( ludii::uInt32 i = 0; i < _matrix.size(); i++ )
	{
		_matrix[i].insert( _matrix.end(), newVector.begin(), newVector.end() );
	}
}

template <typename T>
void MatrixContainer<T>::appendRowsToTheTop( ludii::uInt32 rowCount, const T& defaultValueOfNewElements )
{
	_rowCount += rowCount;

	std::vector<std::vector<T>> newRows( rowCount, defaultValueOfNewElements );
	for ( ludii::uInt32 i = 0; i < newRows.size(); i++ )
	{
		_matrix[i].resize( _columnCount, defaultValueOfNewElements );
	}

	_matrix.insert( _matrix.begin(), newRows.begin(), newRows.end() );
}

template <typename T>
void MatrixContainer<T>::appendRowsToTheBottom( ludii::uInt32 rowCount, const T& defaultValueOfNewElements )
{
	_rowCount += rowCount;

	std::vector<std::vector<T>> newRows( rowCount, defaultValueOfNewElements );
	for ( ludii::uInt32 i = 0; i < newRows.size(); i++ )
	{
		_matrix[i].resize( _columnCount, defaultValueOfNewElements );
	}

	_matrix.insert( _matrix.end(), newRows.begin(), newRows.end() );
}

template <typename T>
void MatrixContainer<T>::removeColumnsFromTheLeft( ludii::uInt32 columnCount )
{
	for ( ludii::uInt32 i = 0; i < _matrix.size(); i++ )
	{
		// Column index is zero based, so reduce by one.
		ludii::uInt32 lastIndexToRemove = columnCount - 1;
		typename std::vector<T>::iterator lastIteratorToRemove = _matrix[i].begin() + lastIndexToRemove;

		_matrix[i].erase( _matrix[i].begin(), lastIteratorToRemove );
	}
}

template <typename T>
void MatrixContainer<T>::removeColumnsFromTheRight( ludii::uInt32 columnCount )
{
	for ( ludii::uInt32 i = 0; i < _matrix.size(); i++ )
	{
		// Column index is zero based, so reduce by one.
		ludii::uInt32 firstIndexToRemove = _matrix[i].size() - ( columnCount - 1 );
		typename std::vector<T>::iterator firstIteratorToRemove = _matrix[i].begin() + firstIndexToRemove;

		_matrix[i].erase( firstIteratorToRemove, _matrix[i].end() );
	}
}

template <typename T>
void MatrixContainer<T>::removeRowsFromTheTop( ludii::uInt32 rowCount )
{
	// Row index is zero based, so reduce by one.
	ludii::uInt32 lastIndexToRemove = rowCount - 1;
	typename std::vector<std::vector<T>>::iterator lastIteratorToRemove = _matrix.begin() + lastIndexToRemove;

	_matrix.erase( _matrix.begin(), lastIteratorToRemove );
}

template <typename T>
void MatrixContainer<T>::removeRowsFromTheBottom( ludii::uInt32 rowCount )
{
	// Row index is zero based, so reduce by one.
	ludii::uInt32 firstIndexToRemove = _matrix.size() - ( rowCount - 1 );
	typename std::vector<std::vector<T>>::iterator firstIteratorToRemove = _matrix.begin() + firstIndexToRemove;

	_matrix.erase( firstIteratorToRemove, _matrix.end() );
}

template <typename T>
void MatrixContainer<T>::clear()
{
	_matrix.clear();
}

#endif // LUDII_UTILITY_MATRIX_CONTAINER_INL