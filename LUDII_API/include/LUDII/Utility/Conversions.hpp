﻿#ifndef LUDII_UTILITY_CONVERSIONS_HPP
#define LUDII_UTILITY_CONVERSIONS_HPP

#include <string>
#include <sstream>

namespace ludii
{
	template <typename T>
	T stringToNumberDefault( const std::string& string, T defaultValue = 0 )
	{
		std::stringstream ss( string );
		T result;
		return ss >> result ? result : defaultValue;
	}

	template <typename T>
	bool stringToNumber( const std::string& string, T& outValue, T defaultValue = 0 )
	{
		std::stringstream ss( string );
		T result;
		if( ss >> result )
		{
			outValue = result;
			return true;
		}
		outValue = defaultValue;
		return false;
	}
}

#endif // LUDII_UTILITY_CONVERSIONS_HPP