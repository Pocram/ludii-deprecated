﻿template <typename R>
Event<R>::Event()
{
}

template <typename R>
void Event<R>::invoke()
{
	typename std::vector<std::function<R()>>::iterator it = _listeners.begin();
	for ( it; it != _listeners.end(); ++it )
	{
		( *it )();
	}
}

template <typename R>
void Event<R>::addListener( std::function<R()> function )
{
	_listeners.push_back( function );
}

template <typename R>
void Event<R>::removeListener( std::function<R()> function )
{
	typename std::vector<std::function<R()>>::iterator it = _listeners.begin();
	while ( it != _listeners.end() )
	{
		if ( it->target_type() == function.target_type() )
		{
			typedef R( fnType )();
			if ( getAddressOfFunction( *it ) == getAddressOfFunction( function ) )
			{
				// Only remove the first found function.
				_listeners.erase( it );
				return;
			}
		}

		++it;
	}
}

template <typename R>
std::size_t Event<R>::getAddressOfFunction( std::function<R()> function )
{
	typedef R( functionType )();
	functionType** functionPointer = function.template target<functionType*>();
	return reinterpret_cast<size_t>( *functionPointer );
}


template <typename R, typename ... Arguments>
ParameterEvent<R, Arguments...>::ParameterEvent()
{
}

template <typename R, typename ... Arguments>
void ParameterEvent<R, Arguments...>::invoke( Arguments ... arguments )
{
	typename std::vector<std::function<R( Arguments ... )>>::iterator it = _listeners.begin();
	for( it; it != _listeners.end(); ++it )
	{
		( *it )( arguments... );
	}
}

template <typename R, typename ... Arguments>
void ParameterEvent<R, Arguments...>::addListener( std::function<R( Arguments ... )> function )
{
	_listeners.push_back( function );
}

template <typename R, typename ... Arguments>
void ParameterEvent<R, Arguments...>::removeListener( std::function<R( Arguments ... )> function )
{
	typename std::vector<std::function<R( Arguments ... )>>::iterator it = _listeners.begin();
	while ( it != _listeners.end() )
	{
		if ( it->target_type() == function.target_type() )
		{
			typedef R( fnType )( Arguments... );
			if( getAddressOfFunction( *it ) == getAddressOfFunction( function ) )
			{
				// Only remove the first found function.
				_listeners.erase( it );
				return;
			}
		}

		++it;
	}
}

template <typename R, typename ... Arguments>
std::size_t ParameterEvent<R, Arguments...>::getAddressOfFunction( std::function<R(Arguments...)> function )
{
	typedef R( functionType )( Arguments... );
	functionType** functionPointer = function.template target<functionType*>();
	return reinterpret_cast<size_t>( *functionPointer );
}