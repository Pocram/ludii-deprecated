#ifndef LUDII_UTILITY_TIME_HPP
#define LUDII_UTILITY_TIME_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

namespace ludii
{
	struct LUDII_API_EXPORT TimeDelta
	{
		float seconds;
		ludii::int32 milliseconds;
		ludii::int64 microseconds;

		float scaledSeconds;
	};
}

#endif // LUDII_UTILITY_TIME_HPP