#ifndef LUDII_UTILITY_STRING_HPP
#define LUDII_UTILITY_STRING_HPP

#include <LUDII/Config.hpp>

#include <string>

namespace ludii
{
	////////////////////////////////////////////////////////////
	/// \brief Replaces the first occurence in a string with a string.
	///
	/// This function looks for a segment in a string and replaces it with a given string.  
	/// It alters the string by reference.
	///
	/// \param string		The string to change.
	/// \param stringToFind	The string to look for. This string will be removed from the given string.
	/// \param replacement	The string to insert into the given string.
	///
	/// \returns Whether the string hhas been replaced successfully.
	////////////////////////////////////////////////////////////
	bool LUDII_API_EXPORT replaceFirstOccurenceInString( std::string& string, const std::string& stringToFind, const std::string& replacement );

	void LUDII_API_EXPORT removeExcessWhitespace( std::string& string );
}

#endif // LUDII_UTILITY_STRING_HPP