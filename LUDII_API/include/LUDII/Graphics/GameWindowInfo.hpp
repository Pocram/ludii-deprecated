#ifndef LUDII_GRAPHICS_GAME_WINDOW_INFO_HPP
#define LUDII_GRAPHICS_GAME_WINDOW_INFO_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <LUDII/Math/Vector2.hpp>

namespace ludii
{
	////////////////////////////////////////////////////////////
	/// \brief Describes the mode of a window.
	////////////////////////////////////////////////////////////
	enum WindowMode
	{
		Windowed = 0,	//!< The window appears as a usual window with a titlebar, a minimize and a close button.
		Borderless = 1,	//!< The window fills up the entire screen but acts as a normal window. Other windows may appear in front of it.
		Fullscreen = 2	//!< The window fills up the entire screen. Other windows may not appear in front of it.
	};

	////////////////////////////////////////////////////////////
	/// \brief Describes how the game is rendered to the window.
	////////////////////////////////////////////////////////////
	enum WindowFillMode
	{
		Dynamic = 0,		//!< The glyphs are rendered at their original size. The window will fit as many glyphs as possible, as not to cut any off. The game may have black borders on the sides.
		DynamicStretch = 1,	//!< The game is rendered using ludii:WindowFillMode::Dynamic, but is then stretched to fill the window. This removes the black bars but may introduce artifacts and unevenness.
		Fixed = 2			//!< The game is rendered at a fixed size.
	};

	////////////////////////////////////////////////////////////
	/// \brief Contains information about the game window.
	////////////////////////////////////////////////////////////
	struct LUDII_API_EXPORT GameWindowInfo
	{
		ludii::Vector2u			dimensions;		//!< The dimensions of the window in pixels.
		ludii::WindowMode		windowMode;		//!< The window's mode. Used to describe whether the window is fullscreen or not.
		ludii::WindowFillMode	windowFillMode;	//!< Describes how the game is being rendered to the window.

		ludii::Vector2u			fixedGlyphCount;		//!< If the window is in fixed mode, describes how many glyphs are rendered on both axes. See ludii::WindowFillMode.
		ludii::Vector2u			effectiveGlyphCount;	//!< Describes how many glyphs are rendered on both axes.


		////////////////////////////////////////////////////////////
		/// \brief Returns the video mode used for SFML objects.
		////////////////////////////////////////////////////////////
		ludii::int8 getWindowStyle() const;
	};
}

#endif // LUDII_GRAPHICS_GAME_WINDOW_INFO_HPP