#ifndef LUDII_GRAPHICS_RENDERER_RENDER_BUFFER_CELL_HPP
#define LUDII_GRAPHICS_RENDERER_RENDER_BUFFER_CELL_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <LUDII/Graphics/Glyph.hpp>
#include <LUDII/Graphics/Color.hpp>

namespace ludii
{
	namespace renderer
	{
		////////////////////////////////////////////////////////////
		/// \brief This struct describes the content of a single cell in the render buffer.
		////////////////////////////////////////////////////////////
		struct LUDII_API_EXPORT RenderBufferCell
		{
			ludii::uInt16 glyphIndex;		//!< The index of the glyph itself.
			
			ludii::Color foregroundColor;	//!< The colour of the glyph.
			ludii::Color backgroundColor;	//!< The colour of the glyph's background.
			
			ludii::int32 drawOrderIndex;	//!< The cell's order in the draw index. Higher orders are drawn later.
		};
	}
}

#endif // LUDII_GRAPHICS_RENDERER_RENDER_BUFFER_CELL_HPP