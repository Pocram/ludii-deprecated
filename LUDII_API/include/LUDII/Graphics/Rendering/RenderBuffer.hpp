#ifndef LUDII_GRAPHICS_RENDERING_RENDER_BUFFER_HPP
#define LUDII_GRAPHICS_RENDERING_RENDER_BUFFER_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <LUDII/Graphics/Rendering/RenderBufferCell.hpp>
#include <LUDII/Utility/MatrixContainer.hpp>

#include <SFML/Graphics.hpp>
#include <LUDII/Graphics/GlyphSheet.hpp>

namespace ludii
{
	namespace renderer
	{
		typedef ludii::containers::MatrixContainer<std::vector<ludii::renderer::RenderBufferCell>> BufferMatrix;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		class LUDII_API_EXPORT RenderBuffer final
		{
		public:
			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			RenderBuffer();

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			RenderBuffer( ludii::uInt32 width, ludii::uInt32 height );

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			~RenderBuffer();


			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void create( ludii::uInt32 width, ludii::uInt32 height );

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void writeAtPosition( const ludii::renderer::RenderBufferCell& value, ludii::uInt32 xPosition, ludii::uInt32 yPosition );

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void renderToImage( sf::Image& targetImage, const ludii::GlyphSheet& glyphSheet );

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void clear();


		private:
			ludii::renderer::BufferMatrix _bufferMatrix;	//!< The matrix containing all the buffered cells.
			
			ludii::uInt8* _pixels;							//!< The array of pixels that contain all the rendered pixels.
			ludii::uInt32 _pixelArrayLength;				//!< The length of the pixels array. The size is calculated every frame. If it differs, the array is reallocated.
		};
	}
}

#endif // LUDII_GRAPHICS_RENDERING_RENDER_BUFFER_HPP