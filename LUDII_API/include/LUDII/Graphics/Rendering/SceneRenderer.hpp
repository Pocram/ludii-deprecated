#ifndef LUDII_GRAPHICS_RENDERING_SCENE_RENDERER_HPP
#define LUDII_GRAPHICS_RENDERING_SCENE_RENDERER_HPP

#include <LUDII/Config.hpp>

#include <LUDII/Graphics/Rendering/RenderBuffer.hpp>

namespace ludii {
	class Camera;
	class Scene;
	struct GlyphSheetInfo;
	struct GameWindowInfo;
}

namespace sf {
	class RenderWindow;
	class Image;
}

namespace ludii
{
	namespace renderer
	{
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		class LUDII_API_EXPORT SceneRenderer final
		{
		public:
			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			SceneRenderer();

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			SceneRenderer( sf::RenderWindow* const renderTarget, ludii::GameWindowInfo* const gameWindowInfo, ludii::GlyphSheetInfo* const glyphSheetInfo );

			
			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void create( sf::RenderWindow* const renderTarget, ludii::GameWindowInfo* const gameWindowInfo, ludii::GlyphSheetInfo* const glyphSheetInfo );

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void clearBuffer();

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void renderToBuffer( const ludii::Scene& scene, const ludii::Camera& camera );

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void renderBufferToImage( sf::Image& outImage, const ludii::GlyphSheet& glyphSheet );


		private:
			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			void calculateGlyphCount() const;

			sf::RenderWindow* _renderTarget;		//!< The SFML render target the scene will be rendered to. Can be a window or a widget.
			ludii::GameWindowInfo* _gameWindowInfo;	//!< The struct that contains info on how to render the scene.
			ludii::GlyphSheetInfo* _glyphSheetInfo;	//!< The struct that contains info about the currently used glyph sheet.

			ludii::renderer::RenderBuffer _buffer;	//!< The buffer that is later drawn to the target.
		};
	}
}

#endif // LUDII_GRAPHICS_RENDERING_SCENE_RENDERER_HPP