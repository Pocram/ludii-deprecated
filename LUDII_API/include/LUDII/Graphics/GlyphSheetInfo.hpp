﻿#ifndef LUDII_GRAPHICS_GLYPH_SHEET_INFO_HPP
#define LUDII_GRAPHICS_GLYPH_SHEET_INFO_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <string>

namespace ludii
{
	struct LUDII_API_EXPORT GlyphSheetInfo
	{
		std::string pathToFile;		//!< The path to the glyph sheet file.
		
		ludii::uInt8 glyphWidth;	//!< The width of a single glyph in pixels.
		ludii::uInt8 glyphHeight;	//!< The height of a single glyph in pixels.
	};
}

#endif // LUDII_GRAPHICS_GLYPH_SHEET_INFO_HPP