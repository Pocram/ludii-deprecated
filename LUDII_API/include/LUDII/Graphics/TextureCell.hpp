#ifndef LUDII_GRAPHICS_TEXTURE_CELL_HPP
#define LUDII_GRAPHICS_TEXTURE_CELL_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <LUDII/Graphics/Color.hpp>

namespace ludii
{
	////////////////////////////////////////////////////////////
	/// TODO: Document me pls :(
	/// \class ludii::TextureCell
	////////////////////////////////////////////////////////////
	class LUDII_API_EXPORT TextureCell
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		TextureCell();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		TextureCell( ludii::int32 glyphIndex, const ludii::Color& foregroundColor, const ludii::Color& backgroundColor );


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		void setForegroundColor( const ludii::Color& color );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		const ludii::Color& getForegroundColor() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		void setBackgroundColor( const ludii::Color& color );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		const ludii::Color& getBackgroundColor() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		void setGlyphIndex( ludii::int32 indexInTexture );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		ludii::int32 getGlyphIndex() const;


	private:
		ludii::int32 _glyphIndexInTexture;	//< TODO: Document
		ludii::Color _foregroundColor;		//< TODO: Document
		ludii::Color _backgroundColor;		//< TODO: Document
	};

	inline bool operator==( const ludii::TextureCell& left, const ludii::TextureCell& right )
	{
		return left.getGlyphIndex() == right.getGlyphIndex() &&
			left.getForegroundColor() == right.getForegroundColor() &&
			left.getBackgroundColor() == right.getBackgroundColor();
	}

	inline bool operator!=( const ludii::TextureCell& left, const ludii::TextureCell& right )
	{
		return !( left == right );
	}

}

#endif // LUDII_GRAPHICS_TEXTURE_CELL_HPP