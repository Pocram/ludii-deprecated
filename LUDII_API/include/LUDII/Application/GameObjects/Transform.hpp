#ifndef LUDII_APPLICATION_GAME_OBJECTS_TRANSFORM_HPP
#define LUDII_APPLICATION_GAME_OBJECTS_TRANSFORM_HPP

#include <LUDII/Config.hpp>

#include <LUDII/Math/Vector2.hpp>
#include <LUDII/Utility/Event.hpp>

namespace ludii
{
	class GameObject;

	////////////////////////////////////////////////////////////
	/// \brief TODO: Document
	////////////////////////////////////////////////////////////
	class LUDII_API_EXPORT Transform final
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		Transform();


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		ludii::GameObject* const getParentGameObject() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		const ludii::Vector2f& getLocalPosition() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		void setLocalPosition( const ludii::Vector2f& position );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		ludii::Vector2f getGlobalPosition() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		void setGlobalPosition( const ludii::Vector2f& position );


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		ludii::Vector2i getLocalCellPosition() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		void setLocalCellPosition( const ludii::Vector2i& position );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		ludii::Vector2i getGlobalCellPosition() const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		void setGlobalCellPosition( const ludii::Vector2i& position );


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		ludii::Event<void> onPositionChangeEvent;


	private:
		ludii::GameObject* _parentGameObject;
		ludii::Vector2f _localFloatPosition;
	};
}

#endif // LUDII_APPLICATION_GAME_OBJECTS_TRANSFORM_HPP