#ifndef LUDII_APPLICATION_GAME_OBJECTS_OBJECT_COMPONENT_HPP
#define LUDII_APPLICATION_GAME_OBJECTS_OBJECT_COMPONENT_HPP

#include <LUDII/Config.hpp>

namespace ludii
{
	////////////////////////////////////////////////////////////
	/// \brief TODO: Document
	////////////////////////////////////////////////////////////
	class LUDII_API_EXPORT ObjectComponent
	{
		friend class GameObject;

	public:
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		virtual ~ObjectComponent();


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		GameObject* const getParentGameObject() const;


	protected:
		////////////////////////////////////////////////////////////
		/// \brief This method is called whenever the component is 
		///		   attached to a GameObject.
		////////////////////////////////////////////////////////////
		virtual void onGameObjectAttach( ludii::GameObject* const parentGameObject );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		virtual void onObjectInstantiate();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		virtual void onStart();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		virtual void onUpdate();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		virtual void onEnable();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		virtual void onDisable();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		ObjectComponent();


	private:
		GameObject* _parentGameObject;	//!< The game object this component belongs to.
		bool _isActive;	//!< Whether the component is currently active or not.
	};
}

#endif // LUDII_APPLICATION_GAME_OBJECTS_OBJECT_COMPONENT_HPP