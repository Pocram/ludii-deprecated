#ifndef LUDII_APPLICATION_INI_GRAPHICS_INI_FILE_HPP
#define LUDII_APPLICATION_INI_GRAPHICS_INI_FILE_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <LUDII/Application/Ini/IniFile.hpp>

#include <LUDII/Graphics/GameWindowInfo.hpp>
#include <LUDII/Graphics/GlyphSheetInfo.hpp>

namespace ludii
{
	namespace ini
	{
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		class LUDII_API_EXPORT GraphicsIniFile : public IniFile
		{
		public:
			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			GraphicsIniFile();

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			~GraphicsIniFile();


			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			bool initialize( const std::string& path ) override;


			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			ludii::GameWindowInfo& getGameWindowInfo();

			////////////////////////////////////////////////////////////
			/// \brief TODO: Document
			////////////////////////////////////////////////////////////
			ludii::GlyphSheetInfo& getGlyphSheetInfo();


		private:
			ludii::GameWindowInfo _windowInfo;		//!< Information regarding the main window.
			ludii::GlyphSheetInfo _glyphSheetInfo;	//!< Information regarding the glyph sheet.
		};
	}
}

#endif // LUDII_APPLICATION_INI_GRAPHICS_INI_FILE_HPP