#ifndef LUDII_API_CONFIG_HPP
#define LUDII_API_CONFIG_HPP

////////////////////////////////////////////////////////////
// Define LUDII versioning numbers.
////////////////////////////////////////////////////////////
#define LUDII_VERSION_MAJOR 0
#define LUDII_VERSION_MINOR 0
#define LUDII_VERSION_BUILD 0


////////////////////////////////////////////////////////////
// Identify the operating system.
////////////////////////////////////////////////////////////
#if defined(_WIN32)
	#define LUDII_SYSTEM_WINDOWS
	// Prevent <windows.h> from defining min and max macros.
	#ifndef NOMINMAX
		#define NOMINMAX
	#endif
//#elif defined(__APPLE__) && defined(__MACH__)
//	#define LUDII_SYSTEM_MACOS
//#elif defined(__unix__)
//	// There are multiple unix systems.
//	#if defined(__ANDROID__)
//		#define LUDII_SYSTEM_ANDROID
//	#elif defined(__linux__)
//		#define LUDII_SYSTEM_LINUX
//	#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
//		#define LUDII_SYSTEM_FREEBSD
//	#else
//		#error This UNIX operating system is not supported by LUDII.
//	#endif
#else
	#error This operating system is not supported by LUDII.
#endif


////////////////////////////////////////////////////////////
// Define portable helpers for dllimport and dllexport macros.
////////////////////////////////////////////////////////////
#if defined(LUDII_SYSTEM_WINDOWS)
	#define LUDII_API_EXPORT __declspec(dllexport)
	#define LUDII_API_IMPORT __declspec(dllimport)
	// Disable warning for Visual C++ compilers.
	#ifdef _MSC_VER
		#pragma warning(disable: 4251)
	#endif
#else // Mac, Lunux and FreeBSD
	#if __GNUC__ >= 4
		#define LUDII_API_EXPORT __attribute__ ((__visibility__ ("default")))
		#define LUDII_API_IMPORT __attribute__ ((__visibility__ ("default")))
	#else
		#define LUDII_API_EXPORT
		#define LUDII_API_IMPORT
	#endif
#endif


////////////////////////////////////////////////////////////
// Define portable deprecation macros.
////////////////////////////////////////////////////////////
#if defined(LUDII_DISABLE_DEPRECATED_WARNINGS)
	#define LUDII_DEPRECATED
#elif defined(_MSC_VER)
	#define LUDII_DEPRECATED __declspec(deprecated)
#elif defined(__GNUC__)
	#define LUDII_DEPRECATED __attribute__ ((deprecated))
#else
	#pragma message("LUDII_DEPRECATED is not supported for your compiler. It will still be defined, but will not do anything.")
	#define LUDII_DEPRECATED
#endif

#endif // LUDII_API_CONFIG_HPP