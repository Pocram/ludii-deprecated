#ifndef LUDII_THIRD_PARTY_RAPID_XML_RAPID_XML_UTILITY_HPP
#define LUDII_THIRD_PARTY_RAPID_XML_RAPID_XML_UTILITY_HPP

#include <LUDII/Config.hpp>

#include <LUDII/ThirdParty/RapidXml/rapidxml.hpp>

#include <string>

namespace ludii
{
	namespace rapid_xml
	{
		rapidxml::xml_node<>* addNodeToDocument( rapidxml::xml_document<>& document, const std::string& name );
	}
}

#endif // LUDII_THIRD_PARTY_RAPID_XML_RAPID_XML_UTILITY_HPP