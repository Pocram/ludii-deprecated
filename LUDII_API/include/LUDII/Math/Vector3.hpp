#ifndef LUDII_MATH_VECTOR_3_HPP
#define LUDII_MATH_VECTOR_3_HPP

#include <LUDII/Config.hpp>

// TODO: Document class

namespace ludii
{
	////////////////////////////////////////////////////////////
	/// \class ludii::Vector3
	///
	/// \brief Represents a vector with three coordinates.
	///
	/// ludii::Vector3 defines a mathematical vector with three
	/// coordinates: X, Y and Z.   
	/// It can be used to describe a position, a direction, velocity, ...
	///
	/// The template parameter T is the type of the coordinates.
	/// It can be any type that supports arithmetic operators, such as
	/// addition, subtraction, multiplication and division.  
	/// Generally, you would use `int` or `float`.
	///
	/// For convenience, the most common vector types have special typedefs:
	/// \li ludii::Vector3f for `float` vectors
	/// \li ludii::Vector3i for `int` vectors
	/// \li ludii::Vector3u for `unsigned int` vectors
	////////////////////////////////////////////////////////////
	template <typename T>
	class LUDII_API_EXPORT Vector3
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief The default constructor.
		///
		/// The default values are set to (0, 0, 0).
		////////////////////////////////////////////////////////////
		Vector3();

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from given parameters.
		///
		/// \param x The X coordinate.
		/// \param y The Y coordinate.
		/// \param z The Z coordinate.
		////////////////////////////////////////////////////////////
		Vector3( T x, T y, T z );

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from a vector of another type.
		///
		/// \param vector The other vector.
		////////////////////////////////////////////////////////////
		template< typename U >
		explicit Vector3( const Vector3<U>& vector );

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's length.
		///
		/// If you want to compare the lengths of two vectors, refer to
		/// ludii::Vector3::getSquaredMagnitude instead. In order to
		/// simply compare two magnitudes, the square root is not needed
		/// and takes more time to compute.
		////////////////////////////////////////////////////////////
		T getMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's squared length.
		///
		/// Compared to ludii::Vector3::getMagnitude this operation
		/// takes less time to compute since it does not calculate
		/// the square root.  
		/// If you want to compare the lengths of two vectors, this
		/// method should be preferred.
		////////////////////////////////////////////////////////////
		T getSquaredMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to a specific value.
		////////////////////////////////////////////////////////////
		void setMagnitude( T targetMagnitude );

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to one.
		////////////////////////////////////////////////////////////
		void normalize();

		////////////////////////////////////////////////////////////
		/// \brief Returns a normalized vector of the same direction.
		////////////////////////////////////////////////////////////
		Vector3<T> getNormalized() const;

		T x;	///< The vector's X coordinate.
		T y;	///< The vector's Y coordinate.
		T z;	///< The vector's Z coordinate.
	};

	template<typename T>
	Vector3<T> operator +( const Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator -( const Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator -( const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator +=( Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator -=( Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator *( const Vector3<T>& left, T right );

	template<typename T>
	Vector3<T> operator /( const Vector3<T>& left, T right );

	template<typename T>
	Vector3<T> operator *=( Vector3<T>& left, T right );

	template<typename T>
	Vector3<T> operator /=( Vector3<T>& left, T right );

	template<typename T>
	Vector3<T> operator ==( const Vector3<T>& left, const Vector3<T>& right );

	template<typename T>
	Vector3<T> operator !=( const Vector3<T>& left, const Vector3<T>& right );

	#include <LUDII/Math/Vector3.inl>

	typedef Vector3<int>	Vector3i;
	typedef Vector3<int>	Vector3u;
	typedef Vector3<float>	Vector3f;
}

#endif // LUDII_MATH_VECTOR_3_HPP