#ifndef LUDII_MATH_RECT_INL
#define LUDII_MATH_RECT_INL

template<typename T>
Rect<T>::Rect() :
	left( 0 ),
	top( 0 ),
	width( 0 ),
	height( 0 )
{
}

template <typename T>
Rect<T>::Rect( T left, T top, T width, T height ) : 
	left( left ), 
	top( top ), 
	width( width ), 
	height( height )
{
}

template <typename T>
template <typename U>
Rect<T>::Rect( const Rect<U>& rect ) :
	left( static_cast<T>( rect.left ) ),
	top( static_cast<T>( rect.top ) ),
	width( static_cast<T>( rect.width ) ),
	height( static_cast<T>( rect.height ) )
{
}

template <typename T>
bool Rect<T>::intersects( const Rect& rect ) const
{
	Rect<T> intersection;
	return intersects( rect, intersection );
}

template <typename T>
bool Rect<T>::intersects( const Rect& rect, Rect& outIntersection ) const
{
	T rectAMinX = std::min( left, static_cast<T>( left + width ) );
	T rectAMaxX = std::max( left, static_cast<T>( left + width ) );
	T rectAMinY = std::min( top, static_cast<T>( top - height ) );
	T rectAMaxY = std::max( top, static_cast<T>( top - height ) );

	T rectBMinX = std::min( rect.left, static_cast<T>( rect.left + rect.width ) );
	T rectBMaxX = std::max( rect.left, static_cast<T>( rect.left + rect.width ) );
	T rectBMinY = std::min( rect.top, static_cast<T>( rect.top - rect.height ) );
	T rectBMaxY = std::max( rect.top, static_cast<T>( rect.top - rect.height ) );

	// Calculate intersection.
	T intersectionLeft = std::max( rectAMinX, rectBMinX );
	T intersectionTop = std::max( rectAMinY, rectBMinY );
	T intersectionRight = std::min( rectAMaxX, rectBMaxX );
	T intersectionBottom = std::min( rectAMaxY, rectBMaxY );

	if ( ( intersectionLeft < intersectionRight ) && ( intersectionTop < intersectionBottom ) )
	{
		outIntersection = Rect<T>( intersectionLeft, intersectionTop, intersectionRight - intersectionLeft, intersectionBottom - intersectionTop );
		return true;
	}
	else
	{
		outIntersection = Rect<T>( 0, 0, 0, 0 );
		return false;
	}
}

template <typename T>
bool Rect<T>::containsPoint( T x, T y ) const
{
	T minX = std::min( left, static_cast<T>( left + width ) );
	T maxX = std::max( left, static_cast<T>( left + width ) );
	T minY = std::min( top, static_cast<T>( top - height ) );
	T maxY = std::max( top, static_cast<T>( top - height ) );

	return ( x >= minX ) && 
		   ( x < maxX ) && 
		   ( y >= minY ) && 
		   ( y < maxY );
}

template <typename T>
bool Rect<T>::containsPoint( const ludii::Vector2<T>& point ) const
{
	return containsPoint( point.x, point.y );
}

template <typename T>
bool operator ==( const Rect<T>& left, const Rect<T>& right )
{
	return ( left.left == right.left ) &&
		   ( left.top == right.top ) &&
		   ( left.width == right.width ) &&
		   ( left.height == right.height );
}

template <typename T>
bool operator!=( const Rect<T>& left, const Rect<T>& right )
{
	return !( left == right );
}
#endif // LUDII_MATH_RECT_INL
