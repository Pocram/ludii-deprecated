#ifndef LUDII_MATH_VECTOR_2_INL
#define LUDII_MATH_VECTOR_2_INL

template <typename T>
Vector2<T>::Vector2() : x( 0 ), y( 0 )
{
}

template <typename T>
Vector2<T>::Vector2( T x, T y ) : x( x ), y( y )
{
}

template <typename T>
template <typename U>
Vector2<T>::Vector2( const Vector2<U>& vector ) :
	x( static_cast<T>( vector.x ) ),
	y( static_cast<T>( vector.y ) )
{
}

template <typename T>
T Vector2<T>::getMagnitude() const
{
	return std::sqrt( getSquaredMagnitude() );
}

template <typename T>
T Vector2<T>::getSquaredMagnitude() const
{
	return x*x + y*y;
}

template <typename T>
void Vector2<T>::setMagnitude( T targetMagnitude )
{
	normalize();
	x *= targetMagnitude;
	y *= targetMagnitude;
}

template <typename T>
void Vector2<T>::normalize()
{
	T m = getMagnitude();
	x /= m;
	y /= m;
}

template <typename T>
Vector2<T> Vector2<T>::getNormalized() const
{
	Vector2<T> v = this;
	T m = getMagnitude();
	v.x /= m;
	v.y /= m;
	return v;
}

template <typename T>
Vector2<T> operator+( const Vector2<T>& left, const Vector2<T>& right )
{
	return Vector2<T>( left.x + right.x, left.y + right.y );
}

template <typename T>
Vector2<T> operator-( const Vector2<T>& left, const Vector2<T>& right )
{
	return Vector2<T>( left.x - right.x, left.y - right.y );
}

template <typename T>
Vector2<T> operator-( const Vector2<T>& right )
{
	return Vector2<T>( -right.x, -right.y );
}

template <typename T>
Vector2<T> operator+=( Vector2<T>& left, const Vector2<T>& right )
{
	left.x += right.x;
	left.y += right.y;

	return left;
}

template <typename T>
Vector2<T> operator-=( Vector2<T>& left, const Vector2<T>& right )
{
	left.x -= right.x;
	left.y -= right.y;

	return left;
}

template<typename T>
Vector2<T> operator *( const Vector2<T>& left, T right )
{
	return Vector2<T>( left.x * right, left.y * right );
}

template<typename T>
Vector2<T> operator /( const Vector2<T>& left, T right )
{
	return Vector2<T>( left.x / right, left.y / right );
}

template<typename T>
Vector2<T> operator *=( Vector2<T>& left, T right )
{
	left.x *= right.x;
	left.y *= right.y;

	return left;
}

template<typename T>
Vector2<T> operator /=( Vector2<T>& left, T right )
{
	left.x /= right.x;
	left.y /= right.y;

	return left;
}

template <typename T>
bool operator==( const Vector2<T>& left, const Vector2<T>& right )
{
	return ( left.x == right.x ) && ( left.y == right.y );
}

template <typename T>
bool operator!=( const Vector2<T>& left, const Vector2<T>& right )
{
	return ( left.x != right.x ) || ( left.y != right.y );
}

#endif // LUDII_MATH_VECTOR_2_INL