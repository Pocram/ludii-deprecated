#ifndef LUDII_MATH_VECTOR_2_HPP
#define LUDII_MATH_VECTOR_2_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <cmath>

namespace ludii
{
	////////////////////////////////////////////////////////////
	/// \class ludii::Vector2
	///
	/// \brief Represents a vector with two coordinates.
	///
	/// ludii::Vector2 defines a mathematical vector with two
	/// coordinates: X and Y.  
	/// It can be used to describe a position, a direction, velocity, ...
	///
	/// The template parameter T is the type of the coordinates.
	/// It can be any type that supports arithmetic operators, such as
	/// addition, subtraction, multiplication and division.  
	/// Generally, you would use `int` or `float`.
	///
	/// For convenience, the most common vector types have special typedefs:
	/// \li ludii::Vector2f for `float` vectors
	/// \li ludii::Vector2i for `int` vectors
	/// \li ludii::Vector2u for `unsigned int` vectors
	////////////////////////////////////////////////////////////
	template <typename T>
	class LUDII_API_EXPORT Vector2
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief The default constructor.
		///
		/// The default values are set to (0, 0).
		////////////////////////////////////////////////////////////
		Vector2();

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from given parameters.
		///
		/// \param x The X coordinate.
		/// \param y The Y coordinate.
		////////////////////////////////////////////////////////////
		Vector2( T x, T y );

		////////////////////////////////////////////////////////////
		/// \brief Creates a vector from a vector of another type.
		///
		/// \param vector The other vector.
		////////////////////////////////////////////////////////////
		template< typename U >
		explicit Vector2( const Vector2<U>& vector );

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's length.
		///
		/// If you want to compare the lengths of two vectors, refer to
		/// ludii::Vector2::getSquaredMagnitude instead. In order to
		/// simply compare two magnitudes, the square root is not needed
		/// and takes more time to compute.
		////////////////////////////////////////////////////////////
		T getMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Returns the vector's squared length.
		///
		/// Compared to ludii::Vector2::getMagnitude this operation
		/// takes less time to compute since it does not calculate
		/// the square root.  
		/// If you want to compare the lengths of two vectors, this
		/// method should be preferred.
		////////////////////////////////////////////////////////////
		T getSquaredMagnitude() const;

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to a specific value.
		////////////////////////////////////////////////////////////
		void setMagnitude( T targetMagnitude );

		////////////////////////////////////////////////////////////
		/// \brief Sets the vector's length to one.
		////////////////////////////////////////////////////////////
		void normalize();

		////////////////////////////////////////////////////////////
		/// \brief Returns a normalized vector of the same direction.
		////////////////////////////////////////////////////////////
		Vector2<T> getNormalized() const;

		T x;	///< The vector's X coordinate.
		T y;	///< The vector's Y coordinate.
	};

	template<typename T>
	Vector2<T> operator +( const Vector2<T>& left,  const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator -( const Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator -( const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator +=( Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator -=( Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	Vector2<T> operator *( const Vector2<T>& left, T right);

	template<typename T>
	Vector2<T> operator /( const Vector2<T>& left, T right );

	template<typename T>
	Vector2<T> operator *=( Vector2<T>& left, T right );

	template<typename T>
	Vector2<T> operator /=( Vector2<T>& left, T right );

	template<typename T>
	bool operator ==( const Vector2<T>& left, const Vector2<T>& right );

	template<typename T>
	bool operator !=( const Vector2<T>& left, const Vector2<T>& right );

	#include <LUDII/Math/Vector2.inl>

	typedef Vector2<ludii::int32>	Vector2i;
	typedef Vector2<ludii::uInt32>	Vector2u;
	typedef Vector2<float>			Vector2f;
}

#endif // LUDII_MATH_VECTOR_2_HPP