#ifndef LUDII_MATH_RECT_HPP
#define LUDII_MATH_RECT_HPP

#include <LUDII/Config.hpp>
#include <LUDII/IntDefines.hpp>

#include <LUDII/Math/Vector2.hpp>

#include <algorithm>

namespace ludii
{
	template <typename T>
	class LUDII_API_EXPORT Rect
	{
	public:
		////////////////////////////////////////////////////////////
		/// \brief The default constructor.
		////////////////////////////////////////////////////////////
		Rect();

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		Rect( T left, T top, T width, T height );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		template< typename U >
		explicit Rect( const Rect<U>& rect );


		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		bool intersects( const Rect& rect ) const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		bool intersects( const Rect& rect, Rect& outIntersection ) const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		bool containsPoint( T x, T y ) const;

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		bool containsPoint( const ludii::Vector2<T>& point ) const;


		T left;		//!< The horizontal distance from (0,0).
		T top;		//!< The vertical distance from (0,0).
		T width;	//!< The rect's width.
		T height;	//!< The rect's height.
	};

	template <typename T>
	bool operator ==( const Rect<T>& left, const Rect<T>& right );

	template <typename T>
	bool operator !=( const Rect<T>& left, const Rect<T>& right );

	#include <LUDII/Math/Rect.inl>

	typedef Rect<ludii::int32>	IntRect;
	typedef Rect<float>			FloatRect;
}

#endif // LUDII_MATH_RECT_HPP