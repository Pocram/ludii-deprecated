#ifndef LUDII_MATH_MATH_HPP
#define LUDII_MATH_MATH_HPP

#include <math.h>

namespace ludii
{
	namespace math
	{
		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		template<typename  T>
		T lerp( T base, T target, float time, bool doExtrapolate = false );

		////////////////////////////////////////////////////////////
		/// \brief TODO: Document
		////////////////////////////////////////////////////////////
		template<typename T>
		T clamp( T value, T minimum, T maximum );
	}
}

template<typename  T>
T ludii::math::lerp( T base, T target, float time, bool doExtrapolate )
{
	if ( doExtrapolate )
	{
		time = ludii::math::clamp<float>( time, 0.0f, 1.0f );
	}

	return ::round( ( 1.0f - time ) * static_cast<float>( base ) + time * static_cast<float>( target ) );
}

template<typename T>
T ludii::math::clamp( T value, T minimum, T maximum )
{
	if( value < minimum )
	{
		return minimum;
	}

	if ( value > maximum )
	{
		return maximum;
	}

	return value;
}

#endif // LUDII_MATH_MATH_HPP