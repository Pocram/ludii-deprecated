#ifndef LUDII_MATH_VECTOR_3_INL
#define LUDII_MATH_VECTOR_3_INL

#include <math.h>

template <typename T>
Vector3<T>::Vector3() :x( 0 ), y( 0 ), z( 0 )
{
}

template <typename T>
Vector3<T>::Vector3( T x, T y, T z ) : x( x ), y( y ), z( z )
{
}

template <typename T>
template <typename U>
Vector3<T>::Vector3( const Vector3<U>& vector ) :
	x( static_cast<T>( vector.x ) ),
	y( static_cast<T>( vector.y ) ),
	z( static_cast<T>( vector.z ) )
{
}

template <typename T>
T Vector3<T>::getMagnitude() const
{
	return sqrt( getSquaredMagnitude() );
}

template <typename T>
T Vector3<T>::getSquaredMagnitude() const
{
	return x*x + y*y + z*z;
}

template <typename T>
void Vector3<T>::setMagnitude( T targetMagnitude )
{
	normalize();
	x *= targetMagnitude;
	y *= targetMagnitude;
	z *= targetMagnitude;
}

template <typename T>
void Vector3<T>::normalize()
{
	T m = getMagnitude();
	x /= m;
	y /= m;
	z /= m;
}

template <typename T>
Vector3<T> Vector3<T>::getNormalized() const
{
	Vector3<T> v = this;
	T m = getMagnitude();
	v.x /= m;
	v.y /= m;
	v.z /= m;
	return v;
}

template <typename T>
Vector3<T> operator+( const Vector3<T>& left, const Vector3<T>& right )
{
	return Vector3<T>( left.x + right.x, left.y + right.y, left.z + right.z );
}

template <typename T>
Vector3<T> operator-( const Vector3<T>& left, const Vector3<T>& right )
{
	return Vector3<T>( left.x - right.x, left.y - right.y, left.z - right.z );
}

template <typename T>
Vector3<T> operator-( const Vector3<T>& right )
{
	return Vector3<T>( -right.x, -right.y, -right.z );
}

template <typename T>
Vector3<T> operator+=( Vector3<T>& left, const Vector3<T>& right )
{
	left.x += right.x;
	left.y += right.y;
	left.z += right.z;

	return left;
}

template <typename T>
Vector3<T> operator-=( Vector3<T>& left, const Vector3<T>& right )
{
	left.x -= right.x;
	left.y -= right.y;
	left.z -= right.z;

	return left;
}

template<typename T>
Vector3<T> operator *( const Vector3<T>& left, T right )
{
	return Vector3<T>( left.x * right, left.y * right, left.z * right );
}

template<typename T>
Vector3<T> operator /( const Vector3<T>& left, T right )
{
	return Vector3<T>( left.x / right, left.y / right, left.z / right );
}

template<typename T>
Vector3<T> operator *=( Vector3<T>& left, T right )
{
	left.x *= right;
	left.y *= right;
	left.z *= right;

	return left;
}

template<typename T>
Vector3<T> operator /=( Vector3<T>& left, T right )
{
	left.x /= right;
	left.y /= right;
	left.z /= right;

	return left;
}

template <typename T>
Vector3<T> operator==( const Vector3<T>& left, const Vector3<T>& right )
{
	return ( left.x == right.x ) && ( left.y == right.y ) && ( left.z == right.z );
}

template <typename T>
Vector3<T> operator!=( const Vector3<T>& left, const Vector3<T>& right )
{
	return ( left.x != right.x ) || ( left.y != right.y ) || ( left.z != right.z );
}

#endif // LUDII_MATH_VECTOR_3_INL