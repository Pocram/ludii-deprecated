﻿#ifndef LUDII_API_INT_DEFINES_HPP
#define LUDII_API_INT_DEFINES_HPP

namespace ludii
{
	// 8 bits integer types.
	typedef signed char int8;
	typedef unsigned char uInt8;

	// 16 bits integer types.
	typedef signed short int16;
	typedef unsigned short uInt16;

	// 32 bits integer types.
	typedef signed int int32;
	typedef unsigned int uInt32;

	// 64 bits integer types.
	#if defined(_MSC_VER)
		typedef signed __int64 int64;
		typedef unsigned __int64 uInt64;
	#else
		typedef signed long long int64;
		typedef unsigned long long uInt64;
	#endif
}

#endif // LUDII_API_INT_DEFINES_HPP