#include <LUDII/Utility/FileStreams.hpp>

void ludii::file_streams::writeTextToStream( std::ofstream& fileStream, const char* value )
{
	fileStream.write( value, strlen( value ) );
}

void ludii::file_streams::writeCharToStream( std::ofstream& fileStream, ludii::int8 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

void ludii::file_streams::writeShortToStream( std::ofstream& fileStream, ludii::int16 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

void ludii::file_streams::writeIntToStream( std::ofstream& fileStream, ludii::int32 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

void ludii::file_streams::writeLongToStream( std::ofstream& fileStream, ludii::int64 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

void ludii::file_streams::writeUnsignedCharToStream( std::ofstream& fileStream, ludii::uInt8 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

void ludii::file_streams::writeUnsignedShortToStream( std::ofstream& fileStream, ludii::uInt16 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

void ludii::file_streams::writeUnsignedIntToStream( std::ofstream& fileStream, ludii::uInt32 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

void ludii::file_streams::writeUnsignedLongToStream( std::ofstream& fileStream, ludii::uInt64 value )
{
	fileStream.write( reinterpret_cast<char*>( &value ), sizeof( value ) );
}

bool ludii::file_streams::getStringAtPosition( std::ifstream& fileStream, ludii::uInt32 positionInFile, ludii::uInt32 length, std::string& outValue )
{
	if ( fileStream.is_open() == false )
	{
		return false;
	}

	fileStream.seekg( positionInFile, std::ios::beg );
	char* buffer = new char[length + 1];
	memset( buffer, '\0', length + 1 );
	fileStream.read( buffer, length );

	if ( fileStream )
	{
		outValue = std::string( buffer );
		delete[] buffer;
		return true;
	}

	delete[] buffer;
	return false;
}
