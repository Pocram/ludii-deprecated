#include <LUDII/Utility/String.hpp>

#include <algorithm>
#include <iterator>

bool ludii::replaceFirstOccurenceInString( std::string& string, const std::string& stringToFind, const std::string& replacement )
{
	std::string::size_type position = string.find( stringToFind );
	if ( position == std::string::npos ) return false;
	string.replace( position, stringToFind.size(), replacement );
	return true;
}

void ludii::removeExcessWhitespace( std::string& string )
{
	std::string temp;
	std::unique_copy( string.begin(), string.end(), std::back_insert_iterator<std::string>( temp ), []( char a, char b )
	{
		return isspace( a ) && isspace( b );
	} );
	string = temp;
}
