#include <LUDII/Application/Ini/GraphicsIniFile.hpp>
#include <LUDII/FileSystem/FileManager.hpp>
#include <LUDII/Utility/Conversions.hpp>

#include <SFML/Window/VideoMode.hpp>
#include "LUDII/Utility/String.hpp"

ludii::ini::GraphicsIniFile::GraphicsIniFile()
{
	_pathToIni = "";

	_glyphSheetInfo.glyphWidth = 0;
	_glyphSheetInfo.glyphHeight = 0;
}

ludii::ini::GraphicsIniFile::~GraphicsIniFile()
{
}

bool ludii::ini::GraphicsIniFile::initialize( const std::string& path )
{
	_pathToIni = path;

	bool fileAlreadyExists = ludii::file_system::FileManager::doesFileExist( _pathToIni );
	if( fileAlreadyExists )
	{
		SI_Error result = _iniFile.LoadFile( _pathToIni.c_str() );
		if ( result < 0 ) return false;
	}

	_iniFile.SetUnicode( true );


	////////////////////////////////////////////////////////////
	// WINDOW
	// Main Window Width
	_windowInfo.dimensions.x = ludii::stringToNumberDefault<ludii::uInt32>( getAndSetMissingValue( "window", "width", "1024" ) );
	if( _windowInfo.dimensions.x <= 0 )
	{
		_windowInfo.dimensions.x = 1024;
		SI_Error result = _iniFile.SetValue( "window", "width", "1024" );
		if( result < 0 ) return false;
	}

	// Main Window Height
	_windowInfo.dimensions.y = ludii::stringToNumberDefault<ludii::uInt32>( getAndSetMissingValue( "window", "height", "768" ) );
	if ( _windowInfo.dimensions.y <= 0 )
	{
		_windowInfo.dimensions.y = 768;
		SI_Error result = _iniFile.SetValue( "window", "height", "768" );
		if ( result < 0 ) return false;
	}

	// Window Mode
	ludii::int16 windowModeContent = ludii::stringToNumberDefault<ludii::int16>( getAndSetMissingValue( "window", "window_mode", "2" ) );
	if ( windowModeContent < 0 || windowModeContent > 2 )
	{
		windowModeContent = 2;
		SI_Error result = _iniFile.SetValue( "window", "window_mode", "2" );
		if ( result < 0 ) return false;
	}
	_windowInfo.windowMode = static_cast<ludii::WindowMode>( windowModeContent );

	// Alter dimensions in fullscreen modes.
	if( _windowInfo.windowMode == ludii::WindowMode::Fullscreen || _windowInfo.windowMode == ludii::WindowMode::Borderless )
	{
		sf::VideoMode desktopInfo = sf::VideoMode::getDesktopMode();
		_windowInfo.dimensions = ludii::Vector2u( desktopInfo.width, desktopInfo.height );
	}

	// Fill Mode
	ludii::int16 fillModeContent = ludii::stringToNumberDefault<ludii::int16>( getAndSetMissingValue( "window", "fill_mode", "0" ) );
	if ( fillModeContent < 0 || fillModeContent > 2 )
	{
		fillModeContent = 0;
		SI_Error result = _iniFile.SetValue( "window", "fill_mode", "0" );
		if ( result < 0 ) return false;
	}
	_windowInfo.windowFillMode = static_cast<ludii::WindowFillMode>( fillModeContent );



	////////////////////////////////////////////////////////////
	// SPRITESHEET
	// Spritesheet path
	std::string spritesheetPath = getAndSetMissingValue( "spritesheet", "path", "{ASSETS}\\spritesheet.png" );
	ludii::replaceFirstOccurenceInString( spritesheetPath, "{ASSETS}", ludii::file_system::FileManager::getAssetsDirectory() );
	_glyphSheetInfo.pathToFile = spritesheetPath;

	// Glyph Width
	_glyphSheetInfo.glyphWidth = ludii::stringToNumberDefault<ludii::uInt32>( getAndSetMissingValue( "spritesheet", "glyph_width", "8" ) );
	if( _glyphSheetInfo.glyphWidth <= 0 )
	{
		_glyphSheetInfo.glyphWidth = 8;
		SI_Error result = _iniFile.SetValue( "spritesheet", "glyph_width", "8" );
		if ( result < 0 ) return false;
	}

	// Glyph Height
	_glyphSheetInfo.glyphHeight = ludii::stringToNumberDefault<ludii::uInt32>( getAndSetMissingValue( "spritesheet", "glyph_height", "8" ) );
	if ( _glyphSheetInfo.glyphHeight <= 0 )
	{
		_glyphSheetInfo.glyphHeight = 8;
		SI_Error result = _iniFile.SetValue( "spritesheet", "glyph_height", "8" );
		if ( result < 0 ) return false;
	}


	SI_Error result = _iniFile.SaveFile( _pathToIni.c_str() );
	return result >= 0;
}

ludii::GameWindowInfo& ludii::ini::GraphicsIniFile::getGameWindowInfo()
{
	return _windowInfo;
}

ludii::GlyphSheetInfo& ludii::ini::GraphicsIniFile::getGlyphSheetInfo()
{
	return _glyphSheetInfo;
}
