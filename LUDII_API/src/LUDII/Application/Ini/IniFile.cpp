#include <LuDII/Application/Ini/IniFile.hpp>

ludii::ini::IniFile::IniFile()
{
}

ludii::ini::IniFile::~IniFile()
{
}

std::string ludii::ini::IniFile::getAndSetMissingValue( const char* section, const char* key, const char* defaultValue )
{
	const char* value = _iniFile.GetValue( section, key, nullptr );
	if( value == nullptr )
	{
		_iniFile.SetValue( section, key, defaultValue );
		return defaultValue;
	}
	return value;
}