#include <LUDII/Application/Scene.hpp>
#include <LUDII/Application/GameObjects/GameObject.hpp>
#include <LUDII/Utility/TimeDelta.hpp>

#include <LUDII/Application/GameObjects/RendererComponent.hpp>

#include <algorithm>
#include <iostream>

ludii::Scene::Scene()
{
}

void ludii::Scene::instantiateGameObject( ludii::GameObject* const gameObject )
{
	// No null pointers!
	if ( gameObject == nullptr )
	{
		return;
	}

	// An object may only exist once per scene.
	if ( std::find( _allGameObjectsInScene.begin(), _allGameObjectsInScene.end(), gameObject ) != _allGameObjectsInScene.end() )
	{
		std::cout << "Unable to instantiate GameObject " << gameObject->getName() << ": Object already exists in scene!\n";
		return;
	}

	gameObject->_parentScene = this;
	_allGameObjectsInScene.push_back( gameObject );
	std::cout << "GameObject " << gameObject->getName() << " has been instantiated into the current scene!\n";

	// Check if the components already contain a renderer component.
	const std::vector<ludii::ObjectComponent*>& components = gameObject->getComponents();
	for ( ludii::int32 i = 0; i < components.size(); i++ )
	{
		if ( ludii::RendererComponent* renderer = dynamic_cast<ludii::RendererComponent*>( components[i] ) )
		{
			// A renderer may only exist once per scene.
			if ( std::find( _renderersInScene.begin(), _renderersInScene.end(), renderer ) == _renderersInScene.end() )
			{
				_renderersInScene.push_back( renderer );
			}
		}
	}
}

std::vector<ludii::GameObject*> ludii::Scene::findGameObjectsByName( const std::string& name ) const
{
	std::vector<ludii::GameObject*> output;
	for ( int i = 0; i < _allGameObjectsInScene.size(); i++ )
	{
		if( _allGameObjectsInScene[i] == nullptr )
		{
			continue;
		}

		if( _allGameObjectsInScene[i]->getName() == name )
		{
			output.push_back( _allGameObjectsInScene[i] );
		}
	}
	return output;
}

const std::vector<ludii::RendererComponent*>& ludii::Scene::getRenderers() const
{
	return _renderersInScene;
}

const ludii::Camera& ludii::Scene::getSceneCamera() const
{
	return _sceneCamera;
}

void ludii::Scene::startGameObjects()
{
	_sceneCamera.onSceneStart();

	for ( ludii::int32 i = 0; i < _allGameObjectsInScene.size(); i++ )
	{
		if ( _allGameObjectsInScene[i] == nullptr )
		{
			continue;
		}

		_allGameObjectsInScene[i]->onSceneStart();
	}
}

void ludii::Scene::updateAllGameObjects( ludii::TimeDelta elapsedTime )
{
	for( ludii::int32 i = 0; i < _allGameObjectsInScene.size(); i++ )
	{
		if( _allGameObjectsInScene[i] == nullptr || _allGameObjectsInScene[i]->isActive() == false )
		{
			printf( "222\n" );
			continue;
		}

		_allGameObjectsInScene[i]->onUpdate( elapsedTime );
		printf( "UPDATE\n" );
	}
}

void ludii::Scene::unloadGameObjects()
{
	for ( std::vector<GameObject*>::iterator it = _allGameObjectsInScene.begin(); it != _allGameObjectsInScene.end(); )
	{
		GameObject* gameObject = *it;
		// The user can flag game objects to survive a scene load.
		if( gameObject->getIsDestroyedOnSceneLoad() )
		{
			delete gameObject;
			it = _allGameObjectsInScene.erase( it );
		}
		else
		{
			++it;
		}
	}
}
