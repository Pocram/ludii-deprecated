#include <LUDII/Application/GameObjects/GameObject.hpp>

ludii::GameObject::GameObject() :
	_name( "Unnamed GameObject" ),
	_isDestroyedOnSceneLoad( false ),
	_parentScene( nullptr )
{
}

ludii::GameObject::GameObject( const std::string& name ) :
	_name( name ),
	_isDestroyedOnSceneLoad( false ),
	_parentScene( nullptr )
{
}

ludii::GameObject::~GameObject()
{
	printf( "Destroying %s\n", _name.c_str() );
}

ludii::Transform& ludii::GameObject::getTransform()
{
	return _transform;
}

const ludii::Transform& ludii::GameObject::getTransform() const
{
	return _transform;
}

const ludii::Transform& ludii::GameObject::getTransformConst() const
{
	return _transform;
}

void ludii::GameObject::setTransform( const ludii::Transform& transform )
{
	_transform.setLocalPosition( transform.getLocalPosition() );
}

void ludii::GameObject::setActive( bool isActive )
{
	_isActive = isActive;
}

bool ludii::GameObject::isActive() const
{
	return _isActive;
}

const std::string& ludii::GameObject::getName() const
{
	return _name;
}

void ludii::GameObject::setName( const std::string& name )
{
	_name = name;
}

const bool& ludii::GameObject::getIsDestroyedOnSceneLoad() const
{
	return _isDestroyedOnSceneLoad;
}

void ludii::GameObject::setIsDestroyedOnSceneLoad( const bool& isDestroyedOnSceneLoad )
{
	_isDestroyedOnSceneLoad = isDestroyedOnSceneLoad;
}

void ludii::GameObject::addComponent( ludii::ObjectComponent* const component )
{
	_components.push_back( component );
	
	component->_parentGameObject = this;
	component->onGameObjectAttach( this );
}

const std::vector<ludii::ObjectComponent*>& ludii::GameObject::getComponents() const
{
	return _components;
}

const ludii::Scene* ludii::GameObject::getParentScene() const
{
	return _parentScene;
}

ludii::Scene* ludii::GameObject::getParentScene()
{
	return _parentScene;
}

void ludii::GameObject::onSceneLoad()
{
}

void ludii::GameObject::onSceneStart()
{
}

void ludii::GameObject::onUpdate( ludii::TimeDelta elapsedTime )
{
}

void ludii::GameObject::onActivate()
{
}

void ludii::GameObject::onDisable()
{
}
