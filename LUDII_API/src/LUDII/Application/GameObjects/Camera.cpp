#include <LUDII/Application/GameObjects/Camera.hpp>
#include <LUDII/Application/Game.hpp>

ludii::Camera::Camera()
{
	setName( "Camera" );
}

const ludii::IntRect& ludii::Camera::getCellBounds() const
{
	return _cellBounds;
}

ludii::Vector2i ludii::Camera::worldPositionToScreenPosition( const ludii::Vector2i& globalCellPosition ) const
{
	ludii::Vector2i relativePosition = globalCellPosition - getTransform().getGlobalCellPosition();
	relativePosition.y *= -1;

	return relativePosition;
}

void ludii::Camera::onSceneStart()
{
	recalculateCellBounds();
	getTransform().onPositionChangeEvent.addListener( std::bind( &Camera::recalculateCellBounds, this ) );
}

void ludii::Camera::recalculateCellBounds()
{
	const ludii::Game& game = ludii::Game::getInstance();
	const ludii::GameWindowInfo& info = game.getGameWindowInfo();

	ludii::Vector2u glyphCount = info.effectiveGlyphCount;

	_cellBounds.left = getTransformConst().getGlobalCellPosition().x;
	_cellBounds.top = getTransformConst().getGlobalCellPosition().y;
	_cellBounds.width = glyphCount.x;
	_cellBounds.height = glyphCount.y;
}
