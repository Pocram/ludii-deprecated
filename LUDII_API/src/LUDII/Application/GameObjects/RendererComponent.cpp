#include <LUDII/Application/GameObjects/RendererComponent.hpp>
#include <LUDII/Application/GameObjects/Transform.hpp>
#include <LUDII/Application/GameObjects/GameObject.hpp>

ludii::RendererComponent::RendererComponent() :
	_drawOrderIndex( 0 )
{
}

ludii::RendererComponent::~RendererComponent()
{
}

const ludii::IntRect& ludii::RendererComponent::getCellBounds() const
{
	return _cellBounds;
}

void ludii::RendererComponent::setDrawOrderIndex( ludii::int32 newIndex )
{
	_drawOrderIndex = newIndex;
}

ludii::int32 ludii::RendererComponent::getDrawOrderIndex() const
{
	return _drawOrderIndex;
}

void ludii::RendererComponent::onGameObjectAttach( ludii::GameObject* const parentGameObject )
{
	if( parentGameObject == nullptr )
	{
		// Wat? Is not possible!
		return;
	}

	recalculateBounds();

	ludii::Transform& transform = parentGameObject->getTransform();
	transform.onPositionChangeEvent.addListener( std::bind( &RendererComponent::recalculateBounds, this ) );
}

void ludii::RendererComponent::setCellBounds( const ludii::IntRect& cellBounds )
{
	_cellBounds = cellBounds;
}
