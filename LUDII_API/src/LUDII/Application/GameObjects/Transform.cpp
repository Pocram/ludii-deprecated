#include <LUDII/Application/GameObjects/Transform.hpp>

#include <cmath>

ludii::Transform::Transform() : _parentGameObject( nullptr ), _localFloatPosition( 0.5f, 0.5f )
{
}

ludii::GameObject* const ludii::Transform::getParentGameObject() const
{
	return _parentGameObject;
}

const ludii::Vector2f& ludii::Transform::getLocalPosition() const
{
	return _localFloatPosition;
}

void ludii::Transform::setLocalPosition( const ludii::Vector2f& position )
{
	_localFloatPosition = position;
	onPositionChangeEvent.invoke();
}

ludii::Vector2f ludii::Transform::getGlobalPosition() const
{
	if( _parentGameObject == nullptr )
	{
		return _localFloatPosition;
	}

	// TODO: Calculate global position.
	return _localFloatPosition;
}

void ludii::Transform::setGlobalPosition( const ludii::Vector2f& position )
{
	if( _parentGameObject == nullptr )
	{
		setLocalPosition( position );
		return;
	}

	// TODO: Calculate global position.
	setLocalPosition( position );
}

ludii::Vector2i ludii::Transform::getLocalCellPosition() const
{
	return ludii::Vector2i( std::trunc( _localFloatPosition.x ), std::trunc( _localFloatPosition.y ) );
}

void ludii::Transform::setLocalCellPosition( const ludii::Vector2i& position )
{
	ludii::Vector2f offset( 0.5f, 0.5f );
	if ( position.x < 0 ) offset.x = -0.5f;
	if ( position.y < 0 ) offset.y = -0.5f;
	setLocalPosition( static_cast<Vector2f>( position ) + offset );
}

ludii::Vector2i ludii::Transform::getGlobalCellPosition() const
{
	if( _parentGameObject == nullptr )
	{
		return getLocalCellPosition();
	}

	// TODO: Calculate global position.
	return getLocalCellPosition();
}

void ludii::Transform::setGlobalCellPosition( const ludii::Vector2i& position )
{
	if ( _parentGameObject == nullptr )
	{
		setLocalCellPosition( position );
		return;
	}

	// TODO: Calculate global position.
	setLocalCellPosition( position );
}
