#include <LUDII/Application/GameObjects/ObjectComponent.hpp>

ludii::ObjectComponent::~ObjectComponent()
{
}

ludii::GameObject* const ludii::ObjectComponent::getParentGameObject() const
{
	return _parentGameObject;
}

void ludii::ObjectComponent::onGameObjectAttach( ludii::GameObject* const parentGameObject )
{
	// For child classes only.
}

void ludii::ObjectComponent::onObjectInstantiate()
{
	// For child classes only.
}

void ludii::ObjectComponent::onStart()
{
	// For child classes only.
}

void ludii::ObjectComponent::onUpdate()
{
	// For child classes only.
}

void ludii::ObjectComponent::onEnable()
{
	// For child classes only.
}

void ludii::ObjectComponent::onDisable()
{
	// For child classes only.
}

ludii::ObjectComponent::ObjectComponent() : _parentGameObject( nullptr ), _isActive( true )
{
}