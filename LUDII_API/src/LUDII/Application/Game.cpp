#include <LUDII/Application/Game.hpp>
#include <LUDII/FileSystem/FileManager.hpp>

#include <SFML/Graphics.hpp>

#include <sstream>
#include <iostream>

ludii::Game::Game() :
	_isInitialized( false ),
	_applicationArgument( nullptr ),
	_currentScene( new ludii::Scene() ),
	_sceneToLoad( nullptr ),
	_glyphSheet( nullptr )
{
}

ludii::Game& ludii::Game::getInstance()
{
	static Game instance;
	return instance;
}

ludii::Game::~Game()
{
	delete _currentScene;
	delete _sceneToLoad;
	delete _glyphSheet;
	delete _applicationArgument;
}

void ludii::Game::initialize( int argc, char* argv[] )
{
	if ( argc <= 0 )
	{
		_isInitialized = false;
		_applicationArgument = "";
	}
	else
	{
		_isInitialized = true;
		_applicationArgument = argv[0];

		if ( initialize() == false )
		{
			// TODO: Log as error
			std::cout << "The application has run into a problem during the initialization process!\n";
			_isInitialized = false;
		}
	}
}

void ludii::Game::start()
{
	if( _isInitialized == false )
	{
		return;
	}

	_mainWindowInfo = _graphicsIni.getGameWindowInfo();

	_mainWindow.create( sf::VideoMode( _mainWindowInfo.dimensions.x, _mainWindowInfo.dimensions.y ), "LUDII Game", _mainWindowInfo.getWindowStyle() );
	_mainWindow.display();

	_sceneRenderer.create( &_mainWindow, &_mainWindowInfo, &_graphicsIni.getGlyphSheetInfo() );

	std::cout << "Glyph counts: " << _mainWindowInfo.effectiveGlyphCount.x << ", " << _mainWindowInfo.effectiveGlyphCount.y << "\n";

	runGameLoop();
}

void ludii::Game::loadScene( ludii::Scene* scene )
{
	_sceneToLoad = scene;
}

void LUDII_DEPRECATED ludii::Game::loadScene( const std::string& pathToScene )
{
	// TODO: Parse scene file into scene object and call loadScene.
}

ludii::Scene* const ludii::Game::getScene() const
{
	return _currentScene;
}

const ludii::Scene* const ludii::Game::getSceneConst() const
{
	return _currentScene;
}

const ludii::GlyphSheet* ludii::Game::getGlyphSheet() const
{
	return _glyphSheet;
}

const ludii::GameWindowInfo& ludii::Game::getGameWindowInfo() const
{
	return _mainWindowInfo;
}

bool ludii::Game::initialize()
{
	bool isInitializedCorrectly = true;

	isInitializedCorrectly = ludii::file_system::FileManager::initialize( _applicationArgument );
	if( isInitializedCorrectly == false )
	{
		// TODO: Log as error
		std::cout << "Error: File System could not be initialized properly!\n";
		return false;
	}

	isInitializedCorrectly &= initializeConfigIni();
	isInitializedCorrectly &= initializeGraphicsIni();

	if( isInitializedCorrectly == false )
	{
		// TODO: Log as error
		std::cout << "Error: Ini System could not be initialized properly!\n";
		return false;
	}

	_glyphSheet = new ludii::GlyphSheet();
	ludii::GlyphSheetInfo& glyphSheetInfo = _graphicsIni.getGlyphSheetInfo();
	_glyphSheet->loadFromFile( glyphSheetInfo.pathToFile, glyphSheetInfo.glyphWidth, glyphSheetInfo.glyphHeight );

	return isInitializedCorrectly;
}

bool ludii::Game::initializeConfigIni()
{
	// TODO: Init ini
	return true;
}

bool ludii::Game::initializeGraphicsIni()
{
	std::string pathToIni = ludii::file_system::FileManager::getPathInDataFolder( "LDGraphics.ini" );
	return _graphicsIni.initialize( pathToIni );
}

void ludii::Game::runGameLoop()
{
	while(_mainWindow.isOpen() )
	{
		sf::Event windowEvent;
		while( _mainWindow.pollEvent( windowEvent ) )
		{
			if( windowEvent.type == sf::Event::Closed )
			{
				_mainWindow.close();
			}
		}

		doGameLoopIteration();
	}
}

void ludii::Game::doGameLoopIteration()
{
	const ludii::int64 frameTime = 1000000 / 60;
	sf::Clock c;
	sf::Time t = c.getElapsedTime();
	ludii::int64 nextFrameTime = t.asMicroseconds() + frameTime;

	loadSceneInternal();
	if( _currentScene == nullptr )
	{
		return;
	}

	float averageElapsedTime = 0;
	ludii::int32 loopCount = 0;
	while( t.asMicroseconds() < nextFrameTime && loopCount < 5 )
	{
		sf::Time mainLoopTime = _mainUpdateClock.restart();
		ludii::TimeDelta d;
		d.seconds = mainLoopTime.asSeconds();
		d.milliseconds = mainLoopTime.asMilliseconds();
		d.microseconds = mainLoopTime.asMicroseconds();
		d.scaledSeconds = d.seconds;
		averageElapsedTime += d.seconds;

		_currentScene->updateAllGameObjects( d );
		t = c.getElapsedTime();
		loopCount++;
	}
	averageElapsedTime /= loopCount;

	_mainWindow.clear();
	renderGameState();
	sf::Font font;
	if ( font.loadFromFile( ludii::file_system::FileManager::getPathInDataFolder( "Fonts/OpenSans-Regular.ttf" ) ) )
	{
		std::stringstream ss;
		ss << static_cast<int>( 1.0f / averageElapsedTime ) << " fps\nHello world!";
		sf::Text fpsText( ss.str(), font, 11 );
		fpsText.setPosition( 5, 5 );
		fpsText.setColor( sf::Color( 255, 255, 0, 255 ) );
		fpsText.setOutlineColor( sf::Color( 0, 0, 0, 255 ) );
		fpsText.setOutlineThickness( 2.0f );
		_mainWindow.draw( fpsText );
	}
	_mainWindow.display();
}

void ludii::Game::renderGameState()
{
	// Render scene to screen.
	_sceneRenderer.clearBuffer();
	_sceneRenderer.renderToBuffer( *_currentScene, _currentScene->getSceneCamera() );
	sf::Image renderedScene;
	_sceneRenderer.renderBufferToImage( renderedScene, *_glyphSheet );
	sf::Texture sceneTexture;
	if( sceneTexture.loadFromImage( renderedScene ) )
	{
		sf::Vector2u outputDimensions = renderedScene.getSize();
		if ( _mainWindowInfo.windowFillMode == ludii::WindowFillMode::DynamicStretch )
		{
			outputDimensions = _mainWindow.getSize();
		}

		sf::RectangleShape outputShape( sf::Vector2f( outputDimensions.x, outputDimensions.y ) );
		outputShape.setTexture( &sceneTexture );

		// Center output on screen.
		outputShape.setOrigin( outputShape.getSize().x / 2.0f, outputShape.getSize().y / 2.0f );
		outputShape.setPosition( _mainWindow.getSize().x / 2.0f, _mainWindow.getSize().y / 2.0f );

		_mainWindow.draw( outputShape );
	}
}

void ludii::Game::loadSceneInternal()
{
	if ( _sceneToLoad == nullptr )
	{
		return;
	}

	// No need to do expensive operations if we can just copy it without loss.
	if ( _currentScene == nullptr )
	{
		_currentScene = _sceneToLoad;
		_currentScene->startGameObjects();
		_sceneToLoad = nullptr;
		return;
	}

	// You can not load the absolute same scene.
	if( _sceneToLoad == _currentScene )
	{
		_sceneToLoad = nullptr;
		return;
	}

	_currentScene->unloadGameObjects();
	_currentScene->_allGameObjectsInScene.insert( _currentScene->_allGameObjectsInScene.end(), _sceneToLoad->_allGameObjectsInScene.begin(), _sceneToLoad->_allGameObjectsInScene.end() );
	_currentScene->_renderersInScene.insert( _currentScene->_renderersInScene.end(), _sceneToLoad->_renderersInScene.begin(), _sceneToLoad->_renderersInScene.end() );
	_currentScene->startGameObjects();
	
	delete _sceneToLoad;
	_sceneToLoad = nullptr;
}
