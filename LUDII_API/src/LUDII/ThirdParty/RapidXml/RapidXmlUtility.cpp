#include <LUDII/ThirdParty/RapidXml/RapidXmlUtility.hpp>

#include <LUDII/ThirdParty/RapidXml/rapidxml_utils.hpp>

#include <fstream>
#include <sstream>
#include <iostream>

rapidxml::xml_node<>* ludii::rapid_xml::addNodeToDocument( rapidxml::xml_document<>& document, const std::string& name )
{
	rapidxml::xml_node<>* node = document.allocate_node( rapidxml::node_element, "Project " );
	document.append_node( node );
	return node;
}
