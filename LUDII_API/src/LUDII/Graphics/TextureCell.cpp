#include <LUDII/Graphics/TextureCell.hpp>

ludii::TextureCell::TextureCell() : 
	_glyphIndexInTexture( 0 ), 
	_foregroundColor( 0, 0, 0, 0 ), 
	_backgroundColor( 0, 0, 0, 0 )
{
}

ludii::TextureCell::TextureCell( int glyphIndex, const ludii::Color& foregroundColor, const ludii::Color& backgroundColor ) :
	_glyphIndexInTexture( glyphIndex ),
	_foregroundColor( foregroundColor ),
	_backgroundColor( backgroundColor )
{
}

void ludii::TextureCell::setForegroundColor( const ludii::Color& color )
{
	_foregroundColor = color;
}

const ludii::Color& ludii::TextureCell::getForegroundColor() const
{
	return _foregroundColor;
}

void ludii::TextureCell::setBackgroundColor( const ludii::Color& color )
{
	_backgroundColor = color;
}

const ludii::Color& ludii::TextureCell::getBackgroundColor() const
{
	return _backgroundColor;
}

void ludii::TextureCell::setGlyphIndex( ludii::int32 indexInTexture )
{
	_glyphIndexInTexture = indexInTexture;
}

ludii::int32 ludii::TextureCell::getGlyphIndex() const
{
	return _glyphIndexInTexture;
}