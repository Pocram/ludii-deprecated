#include <LUDII/Graphics/Sprite.hpp>

ludii::Sprite::Sprite() : _texture( nullptr )
{
}

void ludii::Sprite::setTexture( ludii::Texture* texture )
{
	_texture = texture;
	onSpriteChangeEvent.invoke();
}

ludii::Texture* ludii::Sprite::getTexture() const
{
	return _texture;
}

void ludii::Sprite::setTextureRect( const ludii::IntRect& rect )
{
	_textureRect = rect;
	onSpriteChangeEvent.invoke();
}

const ludii::IntRect& ludii::Sprite::getTextureRect() const
{
	return _textureRect;
}

ludii::TextureCellMatrix ludii::Sprite::getTextureCells() const
{
	if ( _texture == nullptr )
	{
		return ludii::containers::MatrixContainer<ludii::TextureCell>( 0, 0, ludii::TextureCell() );
	}

	return _texture->getCellsInRect( _textureRect );
}
