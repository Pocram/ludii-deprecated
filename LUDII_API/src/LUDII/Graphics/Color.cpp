#include <LUDII/Graphics/Color.hpp>
#include <LUDII/Math/Math.hpp>

#include <SFML/Graphics/Color.hpp>

#include <cmath>

ludii::Color::Color()
{
	r = 0;
	g = 0;
	b = 0;
	a = 0;
}

ludii::Color::Color( ludii::uInt8 red, ludii::uInt8 green, ludii::uInt8 blue, ludii::uInt8 alpha )
{
	r = red;
	g = green;
	b = blue;
	a = alpha;
}

const ludii::Color& ludii::Color::lerp( const ludii::Color& target, float time )
{
	r = ludii::math::lerp<ludii::uInt8>( r, target.r, time );
	g = ludii::math::lerp<ludii::uInt8>( g, target.g, time );
	b = ludii::math::lerp<ludii::uInt8>( b, target.b, time );
	a = ludii::math::lerp<ludii::uInt8>( a, target.a, time );

	return *this;
}

ludii::Color ludii::operator*( const Color& left, const Color& right )
{
	Color c;
	c.r = static_cast<ludii::uInt8>( ( ( left.r * right.r ) / 256.0f ) + 0.5f );
	c.g = static_cast<ludii::uInt8>( ( ( left.g * right.g ) / 256.0f ) + 0.5f );
	c.b = static_cast<ludii::uInt8>( ( ( left.b * right.b ) / 256.0f ) + 0.5f );
	c.a = static_cast<ludii::uInt8>( ( ( left.a * right.a ) / 256.0f ) + 0.5f );
	return c;
}

//bool ludii::operator==( const Color& left, const Color& right )
//{
//	return left.r == right.r && left.g == right.g && left.b == right.b && left.a == right.a;
//}

bool ludii::operator!=( const Color& left, const Color& right )
{
	return !( left == right );
}

bool ludii::operator==( const sf::Color& left, const Color& right )
{
	return left.r == right.r && left.g == right.g && left.b == right.b && left.a == right.a;
}

bool ludii::operator!=( const sf::Color& left, const Color& right )
{
	return !( left == right );
}

bool ludii::operator==( const Color& left, const sf::Color& right )
{
	return left.r == right.r && left.g == right.g && left.b == right.b && left.a == right.a;
}

bool ludii::operator!=( const Color& left, const sf::Color& right )
{
	return !( left == right );
}
