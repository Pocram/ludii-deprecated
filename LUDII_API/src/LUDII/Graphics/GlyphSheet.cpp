#include <LUDII/Graphics/GlyphSheet.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include <cmath>

ludii::GlyphSheet::GlyphSheet()
{
	_glyphWidth = 0;
	_glyphHeight = 0;
}

bool ludii::GlyphSheet::loadFromFile( const std::string pathToFile, ludii::uInt8 glyphWidth, ludii::uInt8 glyphHeight )
{
	_glyphWidth = glyphWidth;
	_glyphHeight = glyphHeight;

	if(_baseTexture.loadFromFile( pathToFile ) == false)
	{
		return false;
	}

	// Set up matrix.
	ludii::uInt16 glyphsPerRow = static_cast<ludii::uInt16>( std::floor( ( _baseTexture.getSize().x / static_cast<float>( _glyphWidth ) ) ) );
	ludii::uInt16 glyphsPerColumn = static_cast<ludii::uInt16>( std::floor( ( _baseTexture.getSize().y / static_cast<float>( _glyphHeight ) ) ) );
	_glyphMatrix.create( glyphsPerColumn, glyphsPerColumn, ludii::Glyph() );

	// Get all glyphs.
	for ( ludii::uInt16 x = 0; x < ( glyphsPerRow - 1 ); x++ )
	{
		for ( ludii::uInt16 y = 0; y < ( glyphsPerColumn - 1 ); y++ )
		{
			// Create glyph from texture rect.
			sf::IntRect glyphSegment( _glyphWidth * x, _glyphHeight * y, _glyphWidth, _glyphHeight );
			ludii::Glyph glyph( _baseTexture, glyphSegment );

			// Add it to the matrix.
			_glyphMatrix.setElementAt( x, y, glyph );
		}
	}

	return true;
}

ludii::uInt8 ludii::GlyphSheet::getGlyphWidth() const
{
	return _glyphWidth;
}

ludii::uInt8 ludii::GlyphSheet::getGlyphHeight() const
{
	return _glyphHeight;
}

ludii::uInt16 ludii::GlyphSheet::getGlyphCount() const
{
	return _glyphMatrix.getRowCount() * _glyphMatrix.getColumnCount();
}

const ludii::Glyph& ludii::GlyphSheet::getGlyphAtIndex( ludii::uInt16 index ) const
{
	ludii::int16 xIndex = index % _glyphMatrix.getColumnCount();
	ludii::int16 yIndex = index / _glyphMatrix.getColumnCount();
	return _glyphMatrix.getElementAtConst( xIndex, yIndex );
}
