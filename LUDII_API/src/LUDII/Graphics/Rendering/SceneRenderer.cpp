#include <LUDII/Graphics/Rendering/SceneRenderer.hpp>

#include <LUDII/Application/Scene.hpp>
#include <LUDII/Application/GameObjects/Camera.hpp>
#include <LUDII/Application/GameObjects/RendererComponent.hpp>

#include <LUDII/Graphics/GameWindowInfo.hpp>
#include <LUDII/Graphics/GlyphSheetInfo.hpp>

#include <SFML/Graphics.hpp>

ludii::renderer::SceneRenderer::SceneRenderer() : 
	_renderTarget( nullptr ), 
	_gameWindowInfo( nullptr ), 
	_glyphSheetInfo(nullptr)
{
	_buffer.create( 0, 0 );
}

ludii::renderer::SceneRenderer::SceneRenderer( sf::RenderWindow* const renderTarget, ludii::GameWindowInfo* const gameWindowInfo, ludii::GlyphSheetInfo* const glyphSheetInfo ) :
	_renderTarget( renderTarget ), 
	_gameWindowInfo( gameWindowInfo ),
	_glyphSheetInfo( glyphSheetInfo )
{
	calculateGlyphCount();
	_buffer.create( _gameWindowInfo->effectiveGlyphCount.x, _gameWindowInfo->effectiveGlyphCount.y );
}

void ludii::renderer::SceneRenderer::create( sf::RenderWindow* const renderTarget, ludii::GameWindowInfo* const gameWindowInfo, ludii::GlyphSheetInfo* const glyphSheetInfo )
{
	_renderTarget = renderTarget;
	_gameWindowInfo = gameWindowInfo;
	_glyphSheetInfo = glyphSheetInfo;

	calculateGlyphCount();
	_buffer.create( _gameWindowInfo->effectiveGlyphCount.x, _gameWindowInfo->effectiveGlyphCount.y );
}

void ludii::renderer::SceneRenderer::clearBuffer()
{
	_buffer.clear();
}

void ludii::renderer::SceneRenderer::renderToBuffer( const ludii::Scene& scene, const ludii::Camera& camera )
{
	if ( _renderTarget == nullptr || _gameWindowInfo == nullptr || _glyphSheetInfo == nullptr )
	{
		return;
	}

	const std::vector<ludii::RendererComponent*>& rendererObjects = scene.getRenderers();
	for( int i = 0; i < rendererObjects.size(); i++ )
	{
		if( rendererObjects[i] == nullptr || rendererObjects[i]->getParentGameObject() == nullptr )
		{
			continue;
		}

		// Only render objects that the camera can see.
		ludii::IntRect objectBounds = rendererObjects[i]->getCellBounds();
		if( camera.getCellBounds().intersects( objectBounds ) == false )
		{
			continue;
		}

		BufferMatrix renderedObject = rendererObjects[i]->renderToBuffer();
		ludii::Vector2i objectOriginPositionOnScreen = camera.worldPositionToScreenPosition( rendererObjects[i]->getParentGameObject()->getTransform().getGlobalCellPosition() );
		
		// Iterate over the cells of the object.
		for ( ludii::uInt32 x = 0; x < renderedObject.getColumnCount(); x++ )
		{
			for ( ludii::uInt32 y = 0; y < renderedObject.getRowCount(); y++ )
			{
				ludii::Vector2u coordinatesInObjectBuffer( x, y );
				ludii::Vector2u coordinatesOnScreen = static_cast<ludii::Vector2u>( objectOriginPositionOnScreen ) + coordinatesInObjectBuffer;

				// Only draw to buffer if the cell is on the screen.
				if( (coordinatesOnScreen.x >= camera.getCellBounds().width) || 
					(coordinatesOnScreen.y >= camera.getCellBounds().height) ||
					(coordinatesOnScreen.x < 0) ||
					(coordinatesOnScreen.y < 0) )
				{
					continue;
				}

				// Iterate over the layers on the object.
				for( ludii::uInt32 l = 0; l < renderedObject.getElementAt( x, y ).size(); l++ )
				{
					_buffer.writeAtPosition( renderedObject.getElementAt( x, y )[l], coordinatesOnScreen.x, coordinatesOnScreen.y );
				}
			}
		}
	}
}

void ludii::renderer::SceneRenderer::renderBufferToImage( sf::Image& outImage, const ludii::GlyphSheet& glyphSheet )
{
	ludii::Vector2u glyphCount = _gameWindowInfo->effectiveGlyphCount;
	outImage.create( glyphCount.x * _glyphSheetInfo->glyphWidth, glyphCount.y * _glyphSheetInfo->glyphHeight, sf::Color( 255, 0, 255 ) );

	_buffer.renderToImage( outImage, glyphSheet );
}

void ludii::renderer::SceneRenderer::calculateGlyphCount() const
{
	if ( _renderTarget == nullptr || _gameWindowInfo == nullptr || _glyphSheetInfo == nullptr )
	{
		return;
	}

	sf::Vector2u renderDimensions = _renderTarget->getSize();
	ludii::Vector2u glyphsPerAxis;

	glyphsPerAxis.x = static_cast<ludii::uInt32>( std::floor( ( renderDimensions.x / static_cast<float>( _glyphSheetInfo->glyphWidth ) ) ) );
	glyphsPerAxis.y = static_cast<ludii::uInt32>( std::floor( ( renderDimensions.y / static_cast<float>( _glyphSheetInfo->glyphHeight ) ) ) );

	_gameWindowInfo->effectiveGlyphCount = glyphsPerAxis;
}
