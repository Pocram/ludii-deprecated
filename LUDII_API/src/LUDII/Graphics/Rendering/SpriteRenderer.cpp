#include <LUDII/Graphics/Rendering/SpriteRenderer.hpp>

#include <LUDII/Graphics/Sprite.hpp>
#include <LUDII/Application/Game.hpp>

ludii::SpriteRenderer::SpriteRenderer() : 
	_sprite( nullptr )
{
}

ludii::renderer::BufferMatrix ludii::SpriteRenderer::renderToBuffer() const
{
	ludii::Game& game = ludii::Game::getInstance();
	if( getSprite() == nullptr || game.getGlyphSheet() == nullptr )
	{
		return ludii::renderer::BufferMatrix();
	}

	ludii::TextureCellMatrix cellMatrix = getSprite()->getTextureCells();
	ludii::renderer::BufferMatrix outputMatrix( cellMatrix.getColumnCount(), cellMatrix.getRowCount(), std::vector<ludii::renderer::RenderBufferCell>() );

	for( ludii::uInt32 x = 0; x < outputMatrix.getColumnCount(); x++ )
	{
		for ( ludii::uInt32 y = 0; y < outputMatrix.getRowCount(); y++ )
		{
			std::vector<ludii::renderer::RenderBufferCell>& newCellStack = outputMatrix.getElementAt( x, y );
			ludii::TextureCell& baseCell = cellMatrix.getElementAt( x, y );
						
			ludii::renderer::RenderBufferCell newCell;
			newCell.glyphIndex = baseCell.getGlyphIndex();
			newCell.backgroundColor = baseCell.getBackgroundColor();
			newCell.foregroundColor = baseCell.getForegroundColor();
			newCell.drawOrderIndex = getDrawOrderIndex();

			newCellStack.push_back( newCell );
		}
	}

	return outputMatrix;
}

void ludii::SpriteRenderer::setSprite( ludii::Sprite* const sprite )
{
	// Unsubscribe from previously existing events.
	if ( _sprite != nullptr )
	{
		_sprite->onSpriteChangeEvent.removeListener( std::bind( &SpriteRenderer::recalculateBounds, this ) );
	}

	_sprite = sprite;
	recalculateBounds();

	_sprite->onSpriteChangeEvent.addListener( std::bind( &SpriteRenderer::recalculateBounds, this ) );
}

const ludii::Sprite* ludii::SpriteRenderer::getSprite() const
{
	return _sprite;
}

void ludii::SpriteRenderer::recalculateBounds()
{
	if( getSprite() == nullptr || getParentGameObject() == nullptr )
	{
		return;
	}

	const ludii::Transform& parentTransform = getParentGameObject()->getTransform();
	const ludii::Vector2i globalCellPosition = parentTransform.getGlobalCellPosition();
	ludii::IntRect spriteTextureRect = getSprite()->getTextureRect();
	
	// If the rect has no size, use the whole texture instead.
	if ( spriteTextureRect.width == 0 || spriteTextureRect.height == 0 )
	{
		const ludii::Texture& spriteTexture = *( getSprite()->getTexture() );
		spriteTextureRect.width = spriteTexture.getDimensions().x;
		spriteTextureRect.height = spriteTexture.getDimensions().y;
	}

	ludii::IntRect newCellBounds( globalCellPosition.x, globalCellPosition.y, getSprite()->getTextureRect().width, getSprite()->getTextureRect().height );
	setCellBounds( newCellBounds );
}
