#include <LUDII/Graphics/Rendering/RenderBuffer.hpp>
#include <LUDII/Application/Game.hpp>

#include <SFML/Graphics.hpp>

#define START_TIMER sf::Clock c;
#define END_TIMER static float totalTime; totalTime += c.getElapsedTime().asSeconds(); static unsigned int times; times++; printf( "%f\t%f\t(%d)\n", c.getElapsedTime().asSeconds(), totalTime / times, times );

ludii::renderer::RenderBuffer::RenderBuffer() :
	_pixels( nullptr ),
	_pixelArrayLength( 0 )
{
}

ludii::renderer::RenderBuffer::RenderBuffer( ludii::uInt32 width, ludii::uInt32 height ) :
	_pixels( nullptr ),
	_pixelArrayLength( 0 )
{
	create( width, height );
}

void ludii::renderer::RenderBuffer::create( ludii::uInt32 width, ludii::uInt32 height )
{
	ludii::renderer::RenderBufferCell defaultCell;
	std::vector<ludii::renderer::RenderBufferCell> defaultVector( 0, defaultCell );

	_bufferMatrix.create( width, height, defaultVector );
	for( ludii::uInt32 i = 0; i < height; i++ )
	{
		std::vector<std::vector<RenderBufferCell>>& row = _bufferMatrix.getRow( i );
		row.reserve( 20 );
	}
}

void ludii::renderer::RenderBuffer::writeAtPosition( const ludii::renderer::RenderBufferCell& value, ludii::uInt32 xPosition, ludii::uInt32 yPosition )
{
	_bufferMatrix.getElementAt( xPosition, yPosition ).push_back( value );
}

void ludii::renderer::RenderBuffer::renderToImage( sf::Image& targetImage, const ludii::GlyphSheet& glyphSheet )
{
	const ludii::Game& game = ludii::Game::getInstance();

	const ludii::uInt32 pixelsLength = targetImage.getSize().x * targetImage.getSize().y * 4;
	if ( pixelsLength != _pixelArrayLength || _pixels == nullptr )
	{
		delete[] _pixels;
		_pixels = new ludii::uInt8[pixelsLength];
		std::memset( _pixels, 0, pixelsLength );
		_pixelArrayLength = pixelsLength;
	}
	else if( _pixels != nullptr )
	{
		std::memset( _pixels, 0, pixelsLength );
	}

	// Loop over each cell stack.
	for (ludii::uInt32 cellsX = 0; cellsX < _bufferMatrix.getColumnCount(); cellsX++)
	{
		for (ludii::uInt32 cellsY = 0; cellsY < _bufferMatrix.getRowCount(); cellsY++)
		{
			const std::vector<RenderBufferCell>& cells = _bufferMatrix.getElementAtConst( cellsX, cellsY );

			// Loop over every cell in the stack.
			for (ludii::uInt32 i = 0; i < cells.size(); i++)
			{
				const ludii::renderer::RenderBufferCell& cell = cells[i];

				const ludii::Glyph& currentGlyph = glyphSheet.getGlyphAtIndex( cell.glyphIndex );
				const ludii::containers::MatrixContainer<ludii::uInt8>& glyphPixelMatrix = currentGlyph.getPixelMatrix();

				// Loop over every pixel in the cell.
				for (ludii::uInt32 x = 0; x < currentGlyph.getWidth(); x++)
				{
					for (ludii::uInt32 y = 0; y < currentGlyph.getHeight(); y++)
					{
						ludii::uInt32 xPositionInImage = cellsX * currentGlyph.getWidth() + x;
						ludii::uInt32 yPositionInImage = cellsY * currentGlyph.getHeight() + y;

						ludii::uInt8 pixelValue = glyphPixelMatrix.getElementAtConst( x, y );
						ludii::Color color = cell.backgroundColor;
						color.lerp( cell.foregroundColor, pixelValue );

						_pixels[( xPositionInImage + yPositionInImage * targetImage.getSize().x ) * 4 + 0] = color.r;
						_pixels[( xPositionInImage + yPositionInImage * targetImage.getSize().x ) * 4 + 1] = color.g;
						_pixels[( xPositionInImage + yPositionInImage * targetImage.getSize().x ) * 4 + 2] = color.b;
						_pixels[( xPositionInImage + yPositionInImage * targetImage.getSize().x ) * 4 + 3] = color.a;
					}
				}
			}
		}
	}

	targetImage.create( targetImage.getSize().x, targetImage.getSize().y, _pixels );
}

void ludii::renderer::RenderBuffer::clear()
{
	for( ludii::uInt32 x = 0; x < _bufferMatrix.getColumnCount(); x++ )
	{
		for ( ludii::uInt32 y = 0; y < _bufferMatrix.getRowCount(); y++ )
		{
			_bufferMatrix.getElementAt( x, y ).clear();
		}
	}
}

ludii::renderer::RenderBuffer::~RenderBuffer()
{
	delete[] _pixels;
}
