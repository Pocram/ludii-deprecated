#include <LUDII/Graphics/GameWindowInfo.hpp>
#include <SFML/Graphics.hpp>

ludii::int8 ludii::GameWindowInfo::getWindowStyle() const
{
	switch( windowMode )
	{
		case ludii::WindowMode::Fullscreen:
			return sf::Style::Fullscreen;
		case ludii::WindowMode::Borderless:
			return sf::Style::None;
		case ludii::WindowMode::Windowed:
			return sf::Style::Titlebar | sf::Style::Close;
		default:
			return sf::Style::Fullscreen;
	}
}