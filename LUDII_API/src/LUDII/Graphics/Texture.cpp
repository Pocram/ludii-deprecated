#include <LUDII/Graphics/Texture.hpp>

#include <LUDII/Utility/FileStreams.hpp>
#include <LUDII/Graphics/GlyphSheet.hpp>
#include <LUDII/Graphics/Rendering/RenderBuffer.hpp>

#include <fstream>

ludii::Texture::Texture()
{
}

ludii::Texture::Texture( ludii::int32 width, ludii::int32 height )
{
	create( width, height );
}

void ludii::Texture::create( ludii::int32 width, ludii::int32 height )
{
	ludii::TextureCell defaultCell( 0, ludii::Color( 0, 0, 0, 255 ), ludii::Color( 0, 0, 0, 255 ) );
	_cellMatrix.create( width, height, defaultCell );
}

void ludii::Texture::getCellAt( ludii::int32 xPosition, ludii::int32 yPosition, ludii::TextureCell& outCellData )
{
	outCellData = _cellMatrix.getElementAt( xPosition, yPosition );
}

void ludii::Texture::setCellAt( ludii::int32 xPosition, ludii::int32 yPosition, const ludii::TextureCell& cellData )
{
	_cellMatrix.setElementAt( xPosition, yPosition, cellData );
}

void ludii::Texture::setAllCells( const ludii::TextureCell& cellData )
{
	ludii::Vector2u dimensions = getDimensions();
	for ( ludii::uInt32 x = 0; x < getDimensions().x; x++ )
	{
		for ( ludii::uInt32 y = 0; y < getDimensions().y; y++ )
		{
			setCellAt( x, y, cellData );
		}
	}
}

bool ludii::Texture::loadFromFile( const std::string& pathToFile )
{
	std::ifstream file( pathToFile, std::ios::binary );
	if( file.good() == false )
	{
		return false;
	}

	ludii::uInt32 c = 0; // Cursor in file.
	ludii::Texture tempTexture;

	// HEADER
	////////////////////////////////////////////////////////////
	std::string fileType;
	if ( ludii::file_streams::getStringAtPosition( file, c, 6, fileType ) == false )
	{
		file.close();
		return false;
	}
	else if( fileType != "LUDTEX" )
	{
		file.close();
		return false;
	}
	c += 6;

	ludii::uInt8 fileVersion = 0;
	if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, fileVersion ) == false )
	{
		file.close();
		return false;
	}
	c += sizeof( ludii::uInt8 );

	ludii::uInt32 textureWidth = 0;
	if ( ludii::file_streams::getNumberAtPosition<ludii::uInt32>( file, c, textureWidth ) == false )
	{
		file.close();
		return false;
	}
	c += sizeof( ludii::uInt32 );

	ludii::uInt32 textureHeight = 0;
	if ( ludii::file_streams::getNumberAtPosition<ludii::uInt32>( file, c, textureHeight ) == false )
	{
		file.close();
		return false;
	}
	c += sizeof( ludii::uInt32 );

	tempTexture.create( textureWidth, textureHeight );

	// CELLS
	////////////////////////////////////////////////////////////
	ludii::uInt32 cellCount = textureWidth * textureHeight;
	for( ludii::uInt32 i = 0; i < cellCount; i++ )
	{
		ludii::uInt32 cellGlyphIndex = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt32>( file, c, cellGlyphIndex ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt32 );
		

		// Foreground Colour
		////////////////////////////////////////////////////////////
		ludii::uInt8 cellForegroundRed = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellForegroundRed ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt8 cellForegroundGreen = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellForegroundGreen ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt8 cellForegroundBlue = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellForegroundBlue ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt8 cellForegroundAlpha = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellForegroundAlpha ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );


		// Background Colour
		////////////////////////////////////////////////////////////
		ludii::uInt8 cellBackgroundRed = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellBackgroundRed ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt8 cellBackgroundGreen = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellBackgroundGreen ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt8 cellBackgroundBlue = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellBackgroundBlue ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt8 cellBackgroundAlpha = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellBackgroundAlpha ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt8 cellRenderMode = 0;
		if ( ludii::file_streams::getNumberAtPosition<ludii::uInt8>( file, c, cellRenderMode ) == false )
		{
			file.close();
			return false;
		}
		c += sizeof( ludii::uInt8 );

		ludii::uInt16 x = i % textureWidth;
		ludii::uInt16 y = i / textureWidth;
		ludii::Color foregroundColor( cellForegroundRed, cellForegroundGreen, cellForegroundBlue, cellForegroundAlpha );
		ludii::Color backgroundColor( cellBackgroundRed, cellBackgroundGreen, cellBackgroundBlue, cellBackgroundAlpha );
		ludii::TextureCell cell( cellGlyphIndex, foregroundColor, backgroundColor );
		tempTexture.setCellAt( x, y, cell );

		*this = tempTexture;
	}

	return true;
}

bool ludii::Texture::saveToFile( const std::string& pathToFile ) const
{
	const char* fileType = "LUDTEX";
	const ludii::uInt8 fileTypeVersion = 1;

	std::ofstream file( pathToFile, std::ios::binary | std::ios::trunc );
	if( file.is_open() )
	{
		ludii::file_streams::writeTextToStream( file, fileType );
		ludii::file_streams::writeUnsignedCharToStream( file, fileTypeVersion );

		ludii::Vector2u dimensions = getDimensions();
		ludii::file_streams::writeUnsignedIntToStream( file, dimensions.x );
		ludii::file_streams::writeUnsignedIntToStream( file, dimensions.y );

		for ( ludii::uInt32 y = 0; y < dimensions.y; y++ )
		{
			for ( ludii::uInt32 x = 0; x < dimensions.x; x++ )
			{
				const ludii::TextureCell& cell = _cellMatrix.getElementAtConst( x, y );

				ludii::file_streams::writeIntToStream( file, cell.getGlyphIndex() );

				ludii::file_streams::writeUnsignedCharToStream( file, cell.getForegroundColor().r );
				ludii::file_streams::writeUnsignedCharToStream( file, cell.getForegroundColor().g );
				ludii::file_streams::writeUnsignedCharToStream( file, cell.getForegroundColor().b );
				ludii::file_streams::writeUnsignedCharToStream( file, cell.getForegroundColor().a );

				ludii::file_streams::writeUnsignedCharToStream( file, cell.getBackgroundColor().r );
				ludii::file_streams::writeUnsignedCharToStream( file, cell.getBackgroundColor().g );
				ludii::file_streams::writeUnsignedCharToStream( file, cell.getBackgroundColor().b );
				ludii::file_streams::writeUnsignedCharToStream( file, cell.getBackgroundColor().a );

				ludii::file_streams::writeUnsignedCharToStream( file, 0 );	// Render mode.
			}
		}
		file.close();
	}

	if ( !file )
	{
		return false;
	}
	return true;
}

ludii::Vector2u ludii::Texture::getDimensions() const
{
	return ludii::Vector2u( _cellMatrix.getColumnCount(), _cellMatrix.getRowCount() );
}

ludii::TextureCellMatrix ludii::Texture::getCellsInRect( const ludii::IntRect& rect ) const
{
	ludii::TextureCellMatrix outputContainer( rect.width, rect.height, ludii::TextureCell() );

	for ( ludii::int32 x = rect.left; x < rect.width; x++ )
	{
		for ( ludii::int32 y = rect.top; y < rect.height; y++ )
		{
			ludii::int32 outputX = x - rect.left;
			ludii::int32 outputY = y - rect.top;

			outputContainer.setElementAt( outputX, outputY, _cellMatrix.getElementAtConst( x, y ) );

		}
	}

	return outputContainer;
}

bool ludii::Texture::hasCoordinate( const ludii::Vector2i& coordinate ) const
{
	return coordinate.x >= 0 && coordinate.y >= 0 && coordinate.x < getDimensions().x && coordinate.y < getDimensions().y;
}

sf::Image ludii::Texture::renderToImage( const ludii::GlyphSheet& glyphSheet ) const
{
	ludii::renderer::RenderBuffer buffer( getDimensions().x, getDimensions().y );
	
	for ( ludii::uInt32 x = 0; x < getDimensions().x; x++ )
	{
		for ( ludii::uInt32 y = 0; y < getDimensions().y; y++ )
		{
			ludii::TextureCell cell = _cellMatrix.getElementAtConst( x, y );

			ludii::renderer::RenderBufferCell newCell;
			newCell.glyphIndex		= cell.getGlyphIndex();
			newCell.foregroundColor	= cell.getForegroundColor();
			newCell.backgroundColor	= cell.getBackgroundColor();
			newCell.drawOrderIndex	= 0;

			buffer.writeAtPosition( newCell, x, y );
		}
	}

	sf::Image outImage;
	outImage.create( getDimensions().x * glyphSheet.getGlyphWidth(), getDimensions().y * glyphSheet.getGlyphHeight() );
	buffer.renderToImage( outImage, glyphSheet );
	return outImage;
}
