#include <LUDII/Graphics/Glyph.hpp>

#include <SFML/Graphics.hpp>
#include <LUDII/Math/Vector2.hpp>

ludii::Glyph::Glyph()
{
}

ludii::Glyph::Glyph( const sf::Texture& sourceTexture, const sf::IntRect& glyphRect )
{
	// Create empty matrix of transparent pixels.
	_pixelMatrix.create( glyphRect.width, glyphRect.height, 0);

	const sf::Image sourceImage = sourceTexture.copyToImage();
	for ( ludii::uInt16 x = 0; x < glyphRect.width; x++ )
	{
		for ( ludii::uInt16 y = 0; y < glyphRect.height; y++ )
		{
			ludii::Vector2u pixelPosition( glyphRect.left + x, glyphRect.top + y );
			const sf::Color c = sourceImage.getPixel( pixelPosition.x, pixelPosition.y );
			// Only add completely opaque white pixels.
			if ( c.r == 255 && c.g == 255 && c.b == 255 && c.a == 255 )
			{
				_pixelMatrix.setElementAt( x, y, 1 );
			}
		}
	}
}

ludii::uInt16 ludii::Glyph::getWidth() const
{
	return _pixelMatrix.getColumnCount();
}

ludii::uInt16 ludii::Glyph::getHeight() const
{
	return _pixelMatrix.getRowCount();
}

const ludii::containers::MatrixContainer<ludii::uInt8>& ludii::Glyph::getPixelMatrix() const
{
	return _pixelMatrix;
}
