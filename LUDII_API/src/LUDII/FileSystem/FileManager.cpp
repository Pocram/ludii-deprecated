#include <LUDII/FileSystem/FileManager.hpp>

#include <iostream>

////////////////////////////////////////////////////////////
// Static Members
boost::filesystem::path ludii::file_system::FileManager::_applicationDirectoryPath = boost::filesystem::path();

////////////////////////////////////////////////////////////
// Methods
bool ludii::file_system::FileManager::initialize( char* argv )
{
	bool isInitialized = true;
	_applicationDirectoryPath = boost::filesystem::system_complete( argv ).parent_path();
	std::cout << "FileManager initialized to: " << getApplicationDirectory() << "\n";

	isInitialized & initializeAssetsDirectory();
	isInitialized & initializeDataDirectory();

	return isInitialized;
}

std::string ludii::file_system::FileManager::getApplicationDirectory()
{
	return _applicationDirectoryPath.string();
}

std::string ludii::file_system::FileManager::getAssetsDirectory()
{
	boost::filesystem::path tempPath = _applicationDirectoryPath;
	tempPath += "\\Assets";
	return tempPath.string();
}

std::string ludii::file_system::FileManager::getDataDirectory()
{
	boost::filesystem::path tempPath = _applicationDirectoryPath;
	tempPath += "\\Data";
	return tempPath.string();
}

bool ludii::file_system::FileManager::doesDirectoryExist( const std::string& pathToDirectory )
{
	bool exists = boost::filesystem::exists( pathToDirectory );
	if( exists == false )
	{
		return false;
	}

	bool isDirectory = boost::filesystem::is_directory( pathToDirectory );

	return exists && isDirectory;
}

bool ludii::file_system::FileManager::createDirectory( const std::string& pathToDirectory )
{
	return boost::filesystem::create_directory( pathToDirectory );
}

bool ludii::file_system::FileManager::doesFileExist( const std::string& pathToFile )
{
	bool exists = boost::filesystem::exists( pathToFile );
	if ( exists == false )
	{
		return false;
	}

	bool isFile = boost::filesystem::is_regular_file( pathToFile );

	return exists && isFile;
}

std::string ludii::file_system::FileManager::getPathInAssetsFolder( const std::string& relativePathInAssetsFolder )
{
	return getAssetsDirectory() + "\\" + relativePathInAssetsFolder;
}

std::string ludii::file_system::FileManager::getPathInDataFolder( const std::string& relativePathInDataFolder )
{
	return getDataDirectory() + "\\" + relativePathInDataFolder;
}

bool ludii::file_system::FileManager::initializeAssetsDirectory()
{
	if ( doesDirectoryExist( getAssetsDirectory() ) )
	{
		std::cout << "Assets directory exists.\n";
		return true;
	}

	std::cout << "Assets directory does not exist.\n";

	if( createDirectory( getAssetsDirectory() ) )
	{
		std::cout << "Assets directory has been created.\n";
		return true;
	}

	return false;
}

bool ludii::file_system::FileManager::initializeDataDirectory()
{
	if ( doesDirectoryExist( getDataDirectory() ) )
	{
		std::cout << "Data directory exists.\n";
		return true;
	}

	std::cout << "Data directory does not exist.\n";

	if ( createDirectory( getDataDirectory() ) )
	{
		std::cout << "Data directory has been created.\n";
		return true;
	}

	return false;
}
