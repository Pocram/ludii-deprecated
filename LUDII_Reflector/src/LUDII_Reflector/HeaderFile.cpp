﻿#include "HeaderFile.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/tokenizer.hpp>
#include <LUDII/Utility/String.hpp>

#include <fstream>
#include <iostream>
#include <LUDII/IntDefines.hpp>

ludii::reflector::HeaderFile::HeaderFile( const std::string& path ) :
	_path( path ),
	_currentScopeLevel( 0 )
{
	_delimiterEncounterFunctions.insert_or_assign( "{", &ludii::reflector::HeaderFile::onDelimiterEncounter_openCurly );
	_delimiterEncounterFunctions.insert_or_assign( "}", &ludii::reflector::HeaderFile::onDelimiterEncounter_closeCurly );
	_delimiterEncounterFunctions.insert_or_assign( "namespace", &ludii::reflector::HeaderFile::onDelimiterEncounter_namespace );
	_delimiterEncounterFunctions.insert_or_assign( "class", &ludii::reflector::HeaderFile::onDelimiterEncounter_class );
	_delimiterEncounterFunctions.insert_or_assign( ";", &ludii::reflector::HeaderFile::onDelimiterEncounter_semicolon );
}

void ludii::reflector::HeaderFile::makeNormalizedFile()
{
	std::string currentLine;
	std::fstream file( _path );
	if ( file.is_open() )
	{
		// Normalise the file content. This way, it's much easier to process.
		while( std::getline( file, currentLine ) )
		{
			if( currentLine.find( "#include" ) == currentLine.npos )
			{
				std::string s = currentLine;
				// Remove comments.
				std::size_t pos = s.find( "//" );
				if ( pos != s.npos )
				{
					s = currentLine.substr( 0, pos );
				}

				_normalizedFile += s;
				_normalizedFile += '\n';
			}
		}

		ludii::removeExcessWhitespace( _normalizedFile );
		file.close();
	}
	else
	{
		std::cout << "Error: File at \"" << _path << "\" could not be opened!\n";
	}
}

bool ludii::reflector::HeaderFile::reflect()
{
	if( _normalizedFile.find( "LUDII_REFLECTABLE" ) == _normalizedFile.npos )
	{
		std::cout << "File at \"" << _path << "\" is not marked for reflection.\n";
		return false;
	}

	// Divide normalised file into chunks.
	boost::char_separator<char> delimiters( " \n", "{}:;" );
	boost::tokenizer<boost::char_separator<char>> tokens( _normalizedFile, delimiters );
	for ( std::string t : tokens )
	{
		_normalizedFileSegments.push_back( t );
	}

	for( ludii::uInt64 i = 0; i < _normalizedFileSegments.size(); i++ )
	{
		if ( i < 2 || ( i + 2 ) >= _normalizedFileSegments.size() )
		{
			continue;
		}

		if ( _normalizedFileSegments[i] == ":" && _normalizedFileSegments[i + 1] == ":" )
		{
			_normalizedFileSegments[i - 1] += "::";
			_normalizedFileSegments[i - 1] += _normalizedFileSegments[i + 2];

			// We have to delete the nodes from back to front, otherwise the indexes no longer match.
			_normalizedFileSegments.erase( _normalizedFileSegments.begin() + i + 2 );
			_normalizedFileSegments.erase( _normalizedFileSegments.begin() + i + 1 );
			_normalizedFileSegments.erase( _normalizedFileSegments.begin() + i );

			// Jump back a few indexes.
			// This prevents skipping a namespace delimiter in a template object, e.g. std::vector<std::string>.
			i -= 3;
		}
	}

	for( ludii::uInt64 i = 0; i < _normalizedFileSegments.size(); i++ )
	{
		const std::string& s = _normalizedFileSegments[i];
		//std::cout << "[" << s << "]\n";

		// Find delimiter in delimiter map and call associated function.
		if ( _delimiterEncounterFunctions.find( s ) != _delimiterEncounterFunctions.end() )
		{
			reflectionDelimiterEncounterFunction function = _delimiterEncounterFunctions[s];

			onDelimiterEncounterInfo info;
			info.delimiter = s;
			info.indexInNormalizedFile = i;
			bool success = ( this->*function )( info );
			if( success == false )
			{
				return false;
			}
		}
	}

	if( _currentScopeLevel != 0 )
	{
		std::cout << "Error: Final scope level is not zero in file \"" << _path << "\"!\n";
		return false;
	}

	return true;
}

const std::vector<ludii::reflector::ReflectedClass>& ludii::reflector::HeaderFile::getClasses() const
{
	return _reflectedClasses;
}

bool ludii::reflector::HeaderFile::onDelimiterEncounter_openCurly( onDelimiterEncounterInfo info )
{
	return true;
}

bool ludii::reflector::HeaderFile::onDelimiterEncounter_closeCurly( onDelimiterEncounterInfo info )
{
	if( _currentScopeStack.top() == ludii::reflector::ScopeType::NamespaceScope )
	{
		_currentNamespaceHierarchy.pop_back();
	}
	else if( _currentScopeStack.top() == ludii::reflector::ScopeType::ClassScope )
	{
		_reflectedClasses.push_back( _currentClassStack.top() );
		_currentClassStack.pop();
	}

	_currentScopeStack.pop();
	_currentScopeLevel--;
	return true;
}

bool ludii::reflector::HeaderFile::onDelimiterEncounter_namespace( onDelimiterEncounterInfo info )
{
	if ( ( info.indexInNormalizedFile + 2 ) >= _normalizedFile.size() )
	{
		return false;
	}

	if( _normalizedFileSegments[info.indexInNormalizedFile + 2] != "{" )
	{
		return false;
	}

	std::string namespaceName = _normalizedFileSegments[info.indexInNormalizedFile + 1];
	_currentNamespaceHierarchy.push_back( namespaceName );

	_currentScopeLevel++;
	_currentScopeStack.push( ludii::reflector::ScopeType::NamespaceScope );

	return true;
}

bool ludii::reflector::HeaderFile::onDelimiterEncounter_class( onDelimiterEncounterInfo info )
{
	if ( ( info.indexInNormalizedFile + 2 ) >= _normalizedFile.size() )
	{
		return false;
	}

	if ( _normalizedFileSegments[info.indexInNormalizedFile + 2] != "{" )
	{
		return false;
	}

	std::string className = _normalizedFileSegments[info.indexInNormalizedFile + 1];
	std::string namespaceName = boost::join( _currentNamespaceHierarchy, "::" );

	_currentScopeLevel++;
	_currentScopeStack.push( ludii::reflector::ScopeType::ClassScope );
	_currentClassStack.push( ludii::reflector::ReflectedClass( className, namespaceName ) );
	_currentClassStack.top().setPathToFile( _path );

	return true;
}

bool ludii::reflector::HeaderFile::onDelimiterEncounter_semicolon( onDelimiterEncounterInfo info )
{
	if ( info.indexInNormalizedFile < 3 )
	{
		return false;
	}

	// Members can only be contained within the class scope.
	if( _currentScopeStack.top() == ludii::reflector::ScopeType::ClassScope )
	{
		std::string nameElement = _normalizedFileSegments[info.indexInNormalizedFile - 1];
		std::vector<std::string> wrongElements = { "{", "}", "(", ")" };

		// Check if the name element is NOT contained in the wrongElements list.
		// This indicates that the element might be a member!
		if ( std::find( wrongElements.begin(), wrongElements.end(), nameElement ) == wrongElements.end() )
		{
			// Check if the name contains brackets (indicates a function).
			if( nameElement.find_first_of( "(){}" ) == nameElement.npos )
			{
				std::string typeName = _normalizedFileSegments[info.indexInNormalizedFile - 2];
				ludii::reflector::ReflectedMember member( nameElement, typeName );
				_currentClassStack.top().addMember( member );
			}
		}
	}

	return true;
}