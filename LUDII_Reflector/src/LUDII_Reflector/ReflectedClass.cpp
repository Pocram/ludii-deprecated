﻿#include "ReflectedClass.hpp"

ludii::reflector::ReflectedClass::ReflectedClass()
{
}

ludii::reflector::ReflectedClass::ReflectedClass( const std::string& name, const std::string& fullNamespace ) :
	_name( name ),
	_fullNamespace( fullNamespace )
{
}

const std::string& ludii::reflector::ReflectedClass::getName() const
{
	return _name;
}

const std::string& ludii::reflector::ReflectedClass::getNamespaceName() const
{
	return _fullNamespace;
}

void ludii::reflector::ReflectedClass::setName( const std::string& name )
{
	_name = name;
}

void ludii::reflector::ReflectedClass::setNamespaceName( const std::string& name )
{
	_fullNamespace = name;
}

const std::string& ludii::reflector::ReflectedClass::getPathToFile() const
{
	return _pathToContainingFile;
}

void ludii::reflector::ReflectedClass::setPathToFile( const std::string& path )
{
	_pathToContainingFile = path;
}

const std::vector<ludii::reflector::ReflectedMember>& ludii::reflector::ReflectedClass::getMembers() const
{
	return _members;
}

std::vector<ludii::reflector::ReflectedMember>& ludii::reflector::ReflectedClass::getMembers()
{
	return _members;
}

void ludii::reflector::ReflectedClass::addMember( const ludii::reflector::ReflectedMember& member )
{
	_members.push_back( member );
}
