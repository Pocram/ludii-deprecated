#ifndef LUDII_REFLECTOR_SCOPES_HPP
#define LUDII_REFLECTOR_SCOPES_HPP

namespace ludii
{
	namespace reflector
	{
		enum ScopeType
		{
			NoScope,
			NamespaceScope,
			ClassScope,
			FunctionScope,
			OtherScope
		};
	}
}

#endif // LUDII_REFLECTOR_SCOPES_HPP