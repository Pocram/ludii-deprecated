﻿#include "ReflectedMember.hpp"

ludii::reflector::ReflectedMember::ReflectedMember()
{
}

ludii::reflector::ReflectedMember::ReflectedMember( const std::string& name, const std::string& typeName ) :
	_name( name ),
	_typeName( typeName )
{
}

const std::string& ludii::reflector::ReflectedMember::getName() const
{
	return _name;
}

const std::string& ludii::reflector::ReflectedMember::getTypeName() const
{
	return _typeName;
}

void ludii::reflector::ReflectedMember::setName( const std::string& name )
{
	_name = name;
}

void ludii::reflector::ReflectedMember::setTypeName( const std::string& name )
{
	_typeName = name;
}
