﻿#ifndef LUDII_REFLECTOR_REFLECTED_MEMBER_HPP
#define LUDII_REFLECTOR_REFLECTED_MEMBER_HPP

#include <string>

namespace ludii
{
	namespace reflector
	{
		class ReflectedMember
		{
		public:
			ReflectedMember();
			ReflectedMember( const std::string& name, const std::string& typeName );

			const std::string& getName() const;
			const std::string& getTypeName() const;

			void setName( const std::string& name );
			void setTypeName( const std::string& name );

		private:
			std::string _name;
			std::string _typeName;
		};
	}
}

#endif // LUDII_REFLECTOR_REFLECTED_MEMBER_HPP