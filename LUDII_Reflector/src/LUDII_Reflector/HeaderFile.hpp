﻿#ifndef LUDII_REFLECTOR_HEADER_FILE_HPP
#define LUDII_REFLECTOR_HEADER_FILE_HPP

#include <LUDII/IntDefines.hpp>

#include <LUDII_Reflector/Scopes.hpp>
#include <LUDII_Reflector/ReflectedClass.hpp>

#include <string>
#include <vector>
#include <map>
#include <stack>

namespace ludii
{
	namespace reflector
	{
		struct onDelimiterEncounterInfo
		{
			std::string delimiter;
			ludii::uInt32 indexInNormalizedFile;
		};

		class HeaderFile
		{
		public:
			HeaderFile( const std::string& path );

			void makeNormalizedFile();
			bool reflect();

			const std::vector<ludii::reflector::ReflectedClass>& getClasses() const;

		private:
			typedef bool( ludii::reflector::HeaderFile::*reflectionDelimiterEncounterFunction )( onDelimiterEncounterInfo info );
			bool onDelimiterEncounter_openCurly( onDelimiterEncounterInfo info );
			bool onDelimiterEncounter_closeCurly( onDelimiterEncounterInfo info );
			bool onDelimiterEncounter_namespace( onDelimiterEncounterInfo info );
			bool onDelimiterEncounter_class( onDelimiterEncounterInfo info );
			bool onDelimiterEncounter_semicolon( onDelimiterEncounterInfo info );
			std::map<std::string, reflectionDelimiterEncounterFunction> _delimiterEncounterFunctions;

			std::string _path;
			std::string _normalizedFile;
			std::vector<std::string> _normalizedFileSegments;
			std::vector<std::string> _currentNamespaceHierarchy;

			ludii::uInt32 _currentScopeLevel;
			std::stack<ludii::reflector::ScopeType> _currentScopeStack;
			
			std::stack<ludii::reflector::ReflectedClass> _currentClassStack;
			std::vector<ludii::reflector::ReflectedClass> _reflectedClasses;
		};
	}
}

#endif // LUDII_REFLECTOR_HEADER_FILE_HPP