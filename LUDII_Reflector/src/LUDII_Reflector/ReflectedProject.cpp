﻿#include "ReflectedProject.hpp"

#include <LUDII_Reflector/Reflector.hpp>
#include <boost/filesystem/operations.hpp>
#include <LUDII/IntDefines.hpp>

#include <iostream>

ludii::reflector::ReflectedProject::ReflectedProject()
{
}

bool ludii::reflector::ReflectedProject::loadProjectSourceFiles( const std::string& pathToSourceDirectory )
{
	_headerFiles.clear();
	boost::filesystem::path projectSourcePath( pathToSourceDirectory );
	bool headerFilesfound = false;

	for ( boost::filesystem::directory_entry it : ludii::reflector::recursive_directory_range( projectSourcePath ) )
	{
		boost::filesystem::path p = it.path();
		if ( ludii::reflector::Reflector::isPathHeaderFile( p ) )
		{
			std::cout << "Header file found: " << p << "\n";
			
			headerFilesfound = true;
			_headerFiles.push_back( ludii::reflector::HeaderFile( p.string() ) );
		}
	}

	return headerFilesfound;
}

void ludii::reflector::ReflectedProject::reflectHeaders()
{
	for( ludii::uInt32 i = 0; i < _headerFiles.size(); i++ )
	{
		_headerFiles[i].makeNormalizedFile();
		_headerFiles[i].reflect();
	}
}

std::vector<ludii::reflector::ReflectedClass> ludii::reflector::ReflectedProject::getClasses() const
{
	std::vector<ludii::reflector::ReflectedClass> o;
	for ( ludii::uInt32 h = 0; h < _headerFiles.size(); h++ )
	{
		const std::vector<ludii::reflector::ReflectedClass>& classes = _headerFiles[h].getClasses();
		for( ludii::uInt32 c = 0; c < classes.size(); c++ )
		{
			o.push_back( classes[c] );
		}
	}
	return o;
}
