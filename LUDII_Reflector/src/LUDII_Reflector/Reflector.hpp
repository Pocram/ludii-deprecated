#ifndef LUDII_REFLECTOR_REFLECTOR_HPP
#define LUDII_REFLECTOR_REFLECTOR_HPP

#include <LUDII_Reflector/ReflectedProject.hpp>

#include <string>

namespace boost {
	namespace filesystem {
		class path;
	}
}

namespace ludii
{
	namespace reflector
	{
		static class Reflector {
		public:
			Reflector() = delete;

			static bool isPathHeaderFile( const boost::filesystem::path& path );
		};
	}
}

#endif // LUDII_REFLECTOR_REFLECTOR_HPP