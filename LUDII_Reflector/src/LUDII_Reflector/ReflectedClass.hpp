﻿#ifndef LUDII_REFLECTOR_REFLECTED_CLASS_HPP
#define LUDII_REFLECTOR_REFLECTED_CLASS_HPP

#include <LUDII_Reflector/ReflectedMember.hpp>

#include <string>
#include <vector>

namespace ludii
{
	namespace reflector
	{
		class ReflectedClass
		{
		public:
			ReflectedClass();
			ReflectedClass( const std::string& name, const std::string& fullNamespace );

			const std::string& getName() const;
			const std::string& getNamespaceName() const;

			void setName( const std::string& name );
			void setNamespaceName( const std::string& name );

			const std::string& getPathToFile() const;
			void setPathToFile( const std::string& path );

			const std::vector<ludii::reflector::ReflectedMember>& getMembers() const;
			std::vector<ludii::reflector::ReflectedMember>& getMembers();
			void addMember( const ludii::reflector::ReflectedMember& member );

		private:
			std::string _name;
			std::string _fullNamespace;
			std::string _pathToContainingFile;

			std::vector<ludii::reflector::ReflectedMember> _members;
		};
	}
}

#endif // LUDII_REFLECTOR_REFLECTED_CLASS_HPP