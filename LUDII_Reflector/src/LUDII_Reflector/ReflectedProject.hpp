﻿#ifndef LUDII_REFLECTOR_REFLECTED_PROJECT_HPP
#define LUDII_REFLECTOR_REFLECTED_PROJECT_HPP

#include <LUDII_Reflector/HeaderFile.hpp>
#include <LUDII_Reflector/ReflectedClass.hpp>

#include <boost/filesystem.hpp>

#include <string>
#include <vector>

namespace ludii
{
	namespace reflector
	{
		struct recursive_directory_range
		{
			typedef boost::filesystem::recursive_directory_iterator iterator;
			recursive_directory_range( boost::filesystem::path p ) : p( p )
			{
			}

			iterator begin()
			{
				return boost::filesystem::recursive_directory_iterator( p );
			}

			iterator end()
			{
				return boost::filesystem::recursive_directory_iterator();
			}

			boost::filesystem::path p;
		};

		class ReflectedProject
		{
		public:
			ReflectedProject();

			bool loadProjectSourceFiles( const std::string& pathToSourceDirectory );
			void reflectHeaders();

			std::vector<ludii::reflector::ReflectedClass> getClasses() const;


		private:
			std::vector<ludii::reflector::HeaderFile> _headerFiles;
		};
	}
}

#endif // LUDII_REFLECTOR_REFLECTED_PROJECT_HPP