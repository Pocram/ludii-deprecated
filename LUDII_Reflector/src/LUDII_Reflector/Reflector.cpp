#include "Reflector.hpp"

#include <boost/filesystem.hpp>

bool ludii::reflector::Reflector::isPathHeaderFile( const boost::filesystem::path& path )
{
	return boost::filesystem::exists( path ) && boost::filesystem::is_regular_file( path ) &&
		 ( path.extension() == ".hpp" || path.extension() == ".h" );
}
