#include <cstdio>
#include <iostream>

#include <boost/filesystem.hpp>

#include <LUDII_Reflector/Reflector.hpp>

int main( int argc, char* argv[] )
{
	if( argc < 2 )
	{
		std::cout << ":(\n";
		getchar();
		return 1;
	}

	boost::filesystem::path pathToProjectFile( argv[1] );
	if( boost::filesystem::exists( pathToProjectFile ) == false )
	{
		std::cout << pathToProjectFile << " does not lead to a valid path!\n";
		getchar();
		return 1;
	}

	if( boost::filesystem::is_regular_file( pathToProjectFile ) == false )
	{
		std::cout << pathToProjectFile << " does not lead to a valid file!\n";
		getchar();
		return 1;
	}

	if( pathToProjectFile.extension() != ".ludiiproject" )
	{
		std::cout << pathToProjectFile << " does not lead to a LUDII project file!\n";
		getchar();
		return 1;
	}

	boost::filesystem::path projectDirectory = pathToProjectFile.parent_path();
	boost::filesystem::path sourceDirectory  = pathToProjectFile.parent_path().string() + "\\src";

	// Check if src folder exists
	if ( boost::filesystem::exists( sourceDirectory ) == false )
	{
		std::cout << sourceDirectory << " does not eixst!\n";
		getchar();
		return 1;
	}

	ludii::reflector::ReflectedProject project;
	project.loadProjectSourceFiles( sourceDirectory.string() );
	std::cout << "----------------------------------------------\n";
	project.reflectHeaders();
	
	std::vector<ludii::reflector::ReflectedClass> classes = project.getClasses();
	for ( int i = 0; i < classes.size(); i++ )
	{
		std::cout << "Class: " << classes[i].getNamespaceName() << "::" << classes[i].getName() << " in: \"" << classes[i].getPathToFile() << "\"\n";
		std::vector<ludii::reflector::ReflectedMember> members = classes[i].getMembers();
		for ( int k = 0; k < members.size(); k++ )
		{
			std::cout << "\t\t" << members[k].getTypeName() << " " << members[k].getName() << "\n";
		}
	}

	getchar();
	return 0;
}
