/****************************************************************************
** Meta object code from reading C++ file 'LoadGlyphSheetDialog.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/LUDII_TEXTURE_EDITOR/Dialogs/LoadGlyphSheetDialog.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LoadGlyphSheetDialog.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ludii__texture_editor__LoadGlyphSheetDialog_t {
    QByteArrayData data[10];
    char stringdata0[163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ludii__texture_editor__LoadGlyphSheetDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ludii__texture_editor__LoadGlyphSheetDialog_t qt_meta_stringdata_ludii__texture_editor__LoadGlyphSheetDialog = {
    {
QT_MOC_LITERAL(0, 0, 43), // "ludii::texture_editor::LoadGl..."
QT_MOC_LITERAL(1, 44, 16), // "onGlyphSheetLoad"
QT_MOC_LITERAL(2, 61, 0), // ""
QT_MOC_LITERAL(3, 62, 25), // "OnGlyphSheetLoadEventInfo"
QT_MOC_LITERAL(4, 88, 4), // "info"
QT_MOC_LITERAL(5, 93, 17), // "onPathButtonClick"
QT_MOC_LITERAL(6, 111, 7), // "checked"
QT_MOC_LITERAL(7, 119, 19), // "onDialogButtonClick"
QT_MOC_LITERAL(8, 139, 16), // "QAbstractButton*"
QT_MOC_LITERAL(9, 156, 6) // "button"

    },
    "ludii::texture_editor::LoadGlyphSheetDialog\0"
    "onGlyphSheetLoad\0\0OnGlyphSheetLoadEventInfo\0"
    "info\0onPathButtonClick\0checked\0"
    "onDialogButtonClick\0QAbstractButton*\0"
    "button"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ludii__texture_editor__LoadGlyphSheetDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   32,    2, 0x08 /* Private */,
       7,    1,   35,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, 0x80000000 | 8,    9,

       0        // eod
};

void ludii::texture_editor::LoadGlyphSheetDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LoadGlyphSheetDialog *_t = static_cast<LoadGlyphSheetDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onGlyphSheetLoad((*reinterpret_cast< OnGlyphSheetLoadEventInfo(*)>(_a[1]))); break;
        case 1: _t->onPathButtonClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->onDialogButtonClick((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractButton* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (LoadGlyphSheetDialog::*_t)(OnGlyphSheetLoadEventInfo );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LoadGlyphSheetDialog::onGlyphSheetLoad)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject ludii::texture_editor::LoadGlyphSheetDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ludii__texture_editor__LoadGlyphSheetDialog.data,
      qt_meta_data_ludii__texture_editor__LoadGlyphSheetDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ludii::texture_editor::LoadGlyphSheetDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ludii::texture_editor::LoadGlyphSheetDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ludii__texture_editor__LoadGlyphSheetDialog.stringdata0))
        return static_cast<void*>(const_cast< LoadGlyphSheetDialog*>(this));
    if (!strcmp(_clname, "Ui::LoadGlyphSheetDialog"))
        return static_cast< Ui::LoadGlyphSheetDialog*>(const_cast< LoadGlyphSheetDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ludii::texture_editor::LoadGlyphSheetDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void ludii::texture_editor::LoadGlyphSheetDialog::onGlyphSheetLoad(OnGlyphSheetLoadEventInfo _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
