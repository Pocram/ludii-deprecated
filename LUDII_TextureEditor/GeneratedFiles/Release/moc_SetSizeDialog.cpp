/****************************************************************************
** Meta object code from reading C++ file 'SetSizeDialog.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/LUDII_TEXTURE_EDITOR/Dialogs/SetSizeDialog.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SetSizeDialog.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ludii__texture_editor__SetSizeDialog_t {
    QByteArrayData data[10];
    char stringdata0[153];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ludii__texture_editor__SetSizeDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ludii__texture_editor__SetSizeDialog_t qt_meta_stringdata_ludii__texture_editor__SetSizeDialog = {
    {
QT_MOC_LITERAL(0, 0, 36), // "ludii::texture_editor::SetSiz..."
QT_MOC_LITERAL(1, 37, 9), // "onSizeSet"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 31), // "SetSizeDialogOnSizeSetEventArgs"
QT_MOC_LITERAL(4, 80, 4), // "args"
QT_MOC_LITERAL(5, 85, 19), // "onDialogButtonClick"
QT_MOC_LITERAL(6, 105, 16), // "QAbstractButton*"
QT_MOC_LITERAL(7, 122, 6), // "button"
QT_MOC_LITERAL(8, 129, 9), // "getAnchor"
QT_MOC_LITERAL(9, 139, 13) // "SizeSetAnchor"

    },
    "ludii::texture_editor::SetSizeDialog\0"
    "onSizeSet\0\0SetSizeDialogOnSizeSetEventArgs\0"
    "args\0onDialogButtonClick\0QAbstractButton*\0"
    "button\0getAnchor\0SizeSetAnchor"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ludii__texture_editor__SetSizeDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   32,    2, 0x08 /* Private */,
       8,    0,   35,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 6,    7,
    0x80000000 | 9,

       0        // eod
};

void ludii::texture_editor::SetSizeDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SetSizeDialog *_t = static_cast<SetSizeDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onSizeSet((*reinterpret_cast< SetSizeDialogOnSizeSetEventArgs(*)>(_a[1]))); break;
        case 1: _t->onDialogButtonClick((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 2: { SizeSetAnchor _r = _t->getAnchor();
            if (_a[0]) *reinterpret_cast< SizeSetAnchor*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractButton* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SetSizeDialog::*_t)(SetSizeDialogOnSizeSetEventArgs );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SetSizeDialog::onSizeSet)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject ludii::texture_editor::SetSizeDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ludii__texture_editor__SetSizeDialog.data,
      qt_meta_data_ludii__texture_editor__SetSizeDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ludii::texture_editor::SetSizeDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ludii::texture_editor::SetSizeDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ludii__texture_editor__SetSizeDialog.stringdata0))
        return static_cast<void*>(const_cast< SetSizeDialog*>(this));
    if (!strcmp(_clname, "Ui::SetSizeDialog"))
        return static_cast< Ui::SetSizeDialog*>(const_cast< SetSizeDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ludii::texture_editor::SetSizeDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void ludii::texture_editor::SetSizeDialog::onSizeSet(SetSizeDialogOnSizeSetEventArgs _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
