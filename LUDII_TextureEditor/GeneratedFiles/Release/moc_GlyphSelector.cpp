/****************************************************************************
** Meta object code from reading C++ file 'GlyphSelector.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/LUDII_Texture_Editor/Widgets/GlyphSelector.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GlyphSelector.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ludii__texture_editor__GlyphSelector_t {
    QByteArrayData data[17];
    char stringdata0[262];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ludii__texture_editor__GlyphSelector_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ludii__texture_editor__GlyphSelector_t qt_meta_stringdata_ludii__texture_editor__GlyphSelector = {
    {
QT_MOC_LITERAL(0, 0, 36), // "ludii::texture_editor::GlyphS..."
QT_MOC_LITERAL(1, 37, 13), // "onGlyphSelect"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 13), // "ludii::uInt16"
QT_MOC_LITERAL(4, 66, 7), // "glyphId"
QT_MOC_LITERAL(5, 74, 13), // "setGlyphSheet"
QT_MOC_LITERAL(6, 88, 18), // "ludii::GlyphSheet*"
QT_MOC_LITERAL(7, 107, 10), // "glyphSheet"
QT_MOC_LITERAL(8, 118, 17), // "createGlyphImages"
QT_MOC_LITERAL(9, 136, 13), // "ludii::uInt32"
QT_MOC_LITERAL(10, 150, 5), // "scale"
QT_MOC_LITERAL(11, 156, 12), // "ludii::Color"
QT_MOC_LITERAL(12, 169, 15), // "foregroundColor"
QT_MOC_LITERAL(13, 185, 15), // "backgroundColor"
QT_MOC_LITERAL(14, 201, 17), // "onBrushInfoChange"
QT_MOC_LITERAL(15, 219, 32), // "ludii::texture_editor::BrushInfo"
QT_MOC_LITERAL(16, 252, 9) // "brushInfo"

    },
    "ludii::texture_editor::GlyphSelector\0"
    "onGlyphSelect\0\0ludii::uInt16\0glyphId\0"
    "setGlyphSheet\0ludii::GlyphSheet*\0"
    "glyphSheet\0createGlyphImages\0ludii::uInt32\0"
    "scale\0ludii::Color\0foregroundColor\0"
    "backgroundColor\0onBrushInfoChange\0"
    "ludii::texture_editor::BrushInfo\0"
    "brushInfo"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ludii__texture_editor__GlyphSelector[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   42,    2, 0x0a /* Public */,
       8,    3,   45,    2, 0x0a /* Public */,
       8,    3,   52,    2, 0x0a /* Public */,
      14,    1,   59,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 9, 0x80000000 | 11, 0x80000000 | 11,   10,   12,   13,
    QMetaType::Void, 0x80000000 | 9, QMetaType::QColor, QMetaType::QColor,   10,   12,   13,
    QMetaType::Void, 0x80000000 | 15,   16,

       0        // eod
};

void ludii::texture_editor::GlyphSelector::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GlyphSelector *_t = static_cast<GlyphSelector *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onGlyphSelect((*reinterpret_cast< ludii::uInt16(*)>(_a[1]))); break;
        case 1: _t->setGlyphSheet((*reinterpret_cast< ludii::GlyphSheet*(*)>(_a[1]))); break;
        case 2: _t->createGlyphImages((*reinterpret_cast< ludii::uInt32(*)>(_a[1])),(*reinterpret_cast< const ludii::Color(*)>(_a[2])),(*reinterpret_cast< const ludii::Color(*)>(_a[3]))); break;
        case 3: _t->createGlyphImages((*reinterpret_cast< ludii::uInt32(*)>(_a[1])),(*reinterpret_cast< const QColor(*)>(_a[2])),(*reinterpret_cast< const QColor(*)>(_a[3]))); break;
        case 4: _t->onBrushInfoChange((*reinterpret_cast< ludii::texture_editor::BrushInfo(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (GlyphSelector::*_t)(ludii::uInt16 );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GlyphSelector::onGlyphSelect)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject ludii::texture_editor::GlyphSelector::staticMetaObject = {
    { &QSfmlCanvas::staticMetaObject, qt_meta_stringdata_ludii__texture_editor__GlyphSelector.data,
      qt_meta_data_ludii__texture_editor__GlyphSelector,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ludii::texture_editor::GlyphSelector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ludii::texture_editor::GlyphSelector::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ludii__texture_editor__GlyphSelector.stringdata0))
        return static_cast<void*>(const_cast< GlyphSelector*>(this));
    return QSfmlCanvas::qt_metacast(_clname);
}

int ludii::texture_editor::GlyphSelector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSfmlCanvas::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void ludii::texture_editor::GlyphSelector::onGlyphSelect(ludii::uInt16 _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
