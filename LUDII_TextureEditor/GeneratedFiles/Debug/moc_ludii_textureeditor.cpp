/****************************************************************************
** Meta object code from reading C++ file 'ludii_textureeditor.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ludii_textureeditor.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ludii_textureeditor.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ludii__texture_editor__LUDII_TextureEditor_t {
    QByteArrayData data[29];
    char stringdata0[550];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ludii__texture_editor__LUDII_TextureEditor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ludii__texture_editor__LUDII_TextureEditor_t qt_meta_stringdata_ludii__texture_editor__LUDII_TextureEditor = {
    {
QT_MOC_LITERAL(0, 0, 42), // "ludii::texture_editor::LUDII_..."
QT_MOC_LITERAL(1, 43, 18), // "onGlyphSheetLoaded"
QT_MOC_LITERAL(2, 62, 0), // ""
QT_MOC_LITERAL(3, 63, 18), // "ludii::GlyphSheet*"
QT_MOC_LITERAL(4, 82, 10), // "glyphSheet"
QT_MOC_LITERAL(5, 93, 15), // "onTextureLoaded"
QT_MOC_LITERAL(6, 109, 14), // "ludii::Texture"
QT_MOC_LITERAL(7, 124, 7), // "texture"
QT_MOC_LITERAL(8, 132, 17), // "onBrushInfoChange"
QT_MOC_LITERAL(9, 150, 32), // "ludii::texture_editor::BrushInfo"
QT_MOC_LITERAL(10, 183, 9), // "brushInfo"
QT_MOC_LITERAL(11, 193, 21), // "setSelectedGlyphIndex"
QT_MOC_LITERAL(12, 215, 13), // "ludii::uInt16"
QT_MOC_LITERAL(13, 229, 5), // "index"
QT_MOC_LITERAL(14, 235, 18), // "onNewTextureCreate"
QT_MOC_LITERAL(15, 254, 50), // "ludii::texture_editor::onCrea..."
QT_MOC_LITERAL(16, 305, 4), // "args"
QT_MOC_LITERAL(17, 310, 18), // "onMenuBarTriggered"
QT_MOC_LITERAL(18, 329, 8), // "QAction*"
QT_MOC_LITERAL(19, 338, 6), // "action"
QT_MOC_LITERAL(20, 345, 16), // "onGlyphSheetLoad"
QT_MOC_LITERAL(21, 362, 48), // "ludii::texture_editor::OnGlyp..."
QT_MOC_LITERAL(22, 411, 4), // "info"
QT_MOC_LITERAL(23, 416, 28), // "onForegroundColorButtonClick"
QT_MOC_LITERAL(24, 445, 7), // "checked"
QT_MOC_LITERAL(25, 453, 28), // "onBackgroundColorButtonClick"
QT_MOC_LITERAL(26, 482, 21), // "onRecenterButtonClick"
QT_MOC_LITERAL(27, 504, 23), // "onPencilToolButtonClick"
QT_MOC_LITERAL(28, 528, 21) // "onFillToolButtonClick"

    },
    "ludii::texture_editor::LUDII_TextureEditor\0"
    "onGlyphSheetLoaded\0\0ludii::GlyphSheet*\0"
    "glyphSheet\0onTextureLoaded\0ludii::Texture\0"
    "texture\0onBrushInfoChange\0"
    "ludii::texture_editor::BrushInfo\0"
    "brushInfo\0setSelectedGlyphIndex\0"
    "ludii::uInt16\0index\0onNewTextureCreate\0"
    "ludii::texture_editor::onCreateNewTextureEventArgs\0"
    "args\0onMenuBarTriggered\0QAction*\0"
    "action\0onGlyphSheetLoad\0"
    "ludii::texture_editor::OnGlyphSheetLoadEventInfo\0"
    "info\0onForegroundColorButtonClick\0"
    "checked\0onBackgroundColorButtonClick\0"
    "onRecenterButtonClick\0onPencilToolButtonClick\0"
    "onFillToolButtonClick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ludii__texture_editor__LUDII_TextureEditor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x06 /* Public */,
       5,    1,  102,    2, 0x06 /* Public */,
       8,    1,  105,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    1,  108,    2, 0x0a /* Public */,
      14,    1,  111,    2, 0x08 /* Private */,
      17,    1,  114,    2, 0x08 /* Private */,
      20,    1,  117,    2, 0x08 /* Private */,
      23,    1,  120,    2, 0x08 /* Private */,
      23,    0,  123,    2, 0x28 /* Private | MethodCloned */,
      25,    1,  124,    2, 0x08 /* Private */,
      25,    0,  127,    2, 0x28 /* Private | MethodCloned */,
      26,    1,  128,    2, 0x08 /* Private */,
      26,    0,  131,    2, 0x28 /* Private | MethodCloned */,
      27,    1,  132,    2, 0x08 /* Private */,
      27,    0,  135,    2, 0x28 /* Private | MethodCloned */,
      28,    1,  136,    2, 0x08 /* Private */,
      28,    0,  139,    2, 0x28 /* Private | MethodCloned */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 9,   10,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 18,   19,
    QMetaType::Void, 0x80000000 | 21,   22,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,

       0        // eod
};

void ludii::texture_editor::LUDII_TextureEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LUDII_TextureEditor *_t = static_cast<LUDII_TextureEditor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onGlyphSheetLoaded((*reinterpret_cast< ludii::GlyphSheet*(*)>(_a[1]))); break;
        case 1: _t->onTextureLoaded((*reinterpret_cast< ludii::Texture(*)>(_a[1]))); break;
        case 2: _t->onBrushInfoChange((*reinterpret_cast< ludii::texture_editor::BrushInfo(*)>(_a[1]))); break;
        case 3: _t->setSelectedGlyphIndex((*reinterpret_cast< ludii::uInt16(*)>(_a[1]))); break;
        case 4: _t->onNewTextureCreate((*reinterpret_cast< ludii::texture_editor::onCreateNewTextureEventArgs(*)>(_a[1]))); break;
        case 5: _t->onMenuBarTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 6: _t->onGlyphSheetLoad((*reinterpret_cast< ludii::texture_editor::OnGlyphSheetLoadEventInfo(*)>(_a[1]))); break;
        case 7: _t->onForegroundColorButtonClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->onForegroundColorButtonClick(); break;
        case 9: _t->onBackgroundColorButtonClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->onBackgroundColorButtonClick(); break;
        case 11: _t->onRecenterButtonClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->onRecenterButtonClick(); break;
        case 13: _t->onPencilToolButtonClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->onPencilToolButtonClick(); break;
        case 15: _t->onFillToolButtonClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->onFillToolButtonClick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (LUDII_TextureEditor::*_t)(ludii::GlyphSheet * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LUDII_TextureEditor::onGlyphSheetLoaded)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (LUDII_TextureEditor::*_t)(ludii::Texture );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LUDII_TextureEditor::onTextureLoaded)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (LUDII_TextureEditor::*_t)(ludii::texture_editor::BrushInfo );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LUDII_TextureEditor::onBrushInfoChange)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject ludii::texture_editor::LUDII_TextureEditor::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ludii__texture_editor__LUDII_TextureEditor.data,
      qt_meta_data_ludii__texture_editor__LUDII_TextureEditor,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ludii::texture_editor::LUDII_TextureEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ludii::texture_editor::LUDII_TextureEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ludii__texture_editor__LUDII_TextureEditor.stringdata0))
        return static_cast<void*>(const_cast< LUDII_TextureEditor*>(this));
    if (!strcmp(_clname, "Ui::LUDII_TextureEditorClass"))
        return static_cast< Ui::LUDII_TextureEditorClass*>(const_cast< LUDII_TextureEditor*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ludii::texture_editor::LUDII_TextureEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void ludii::texture_editor::LUDII_TextureEditor::onGlyphSheetLoaded(ludii::GlyphSheet * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ludii::texture_editor::LUDII_TextureEditor::onTextureLoaded(ludii::Texture _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ludii::texture_editor::LUDII_TextureEditor::onBrushInfoChange(ludii::texture_editor::BrushInfo _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
