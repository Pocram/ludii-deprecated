/********************************************************************************
** Form generated from reading UI file 'LoadGlyphSheetDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOADGLYPHSHEETDIALOG_H
#define UI_LOADGLYPHSHEETDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_LoadGlyphSheetDialog
{
public:
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QLineEdit *sheetPathLine;
    QToolButton *sheetPathSelector;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QFormLayout *formLayout_3;
    QLabel *label_4;
    QSpinBox *glyphWidthSpin;
    QFormLayout *formLayout_2;
    QLabel *label_3;
    QSpinBox *glyphHeightSpin;
    QLabel *fileWarningLabel;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *LoadGlyphSheetDialog)
    {
        if (LoadGlyphSheetDialog->objectName().isEmpty())
            LoadGlyphSheetDialog->setObjectName(QStringLiteral("LoadGlyphSheetDialog"));
        LoadGlyphSheetDialog->resize(600, 120);
        LoadGlyphSheetDialog->setMinimumSize(QSize(600, 120));
        LoadGlyphSheetDialog->setMaximumSize(QSize(600, 120));
        gridLayout = new QGridLayout(LoadGlyphSheetDialog);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setHorizontalSpacing(6);
        formLayout->setVerticalSpacing(6);
        label = new QLabel(LoadGlyphSheetDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, 0, -1);
        sheetPathLine = new QLineEdit(LoadGlyphSheetDialog);
        sheetPathLine->setObjectName(QStringLiteral("sheetPathLine"));
        sheetPathLine->setClearButtonEnabled(true);

        horizontalLayout->addWidget(sheetPathLine);

        sheetPathSelector = new QToolButton(LoadGlyphSheetDialog);
        sheetPathSelector->setObjectName(QStringLiteral("sheetPathSelector"));

        horizontalLayout->addWidget(sheetPathSelector);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);

        label_2 = new QLabel(LoadGlyphSheetDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        formLayout_3 = new QFormLayout();
        formLayout_3->setSpacing(6);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        formLayout_3->setContentsMargins(-1, -1, 10, -1);
        label_4 = new QLabel(LoadGlyphSheetDialog);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMargin(0);

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_4);

        glyphWidthSpin = new QSpinBox(LoadGlyphSheetDialog);
        glyphWidthSpin->setObjectName(QStringLiteral("glyphWidthSpin"));
        glyphWidthSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        glyphWidthSpin->setMinimum(1);
        glyphWidthSpin->setMaximum(256);
        glyphWidthSpin->setValue(8);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, glyphWidthSpin);


        horizontalLayout_2->addLayout(formLayout_3);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_3 = new QLabel(LoadGlyphSheetDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMargin(0);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_3);

        glyphHeightSpin = new QSpinBox(LoadGlyphSheetDialog);
        glyphHeightSpin->setObjectName(QStringLiteral("glyphHeightSpin"));
        glyphHeightSpin->setMinimumSize(QSize(0, 20));
        glyphHeightSpin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        glyphHeightSpin->setMinimum(1);
        glyphHeightSpin->setMaximum(256);
        glyphHeightSpin->setValue(8);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, glyphHeightSpin);


        horizontalLayout_2->addLayout(formLayout_2);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout_2);


        gridLayout->addLayout(formLayout, 0, 0, 1, 1);

        fileWarningLabel = new QLabel(LoadGlyphSheetDialog);
        fileWarningLabel->setObjectName(QStringLiteral("fileWarningLabel"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        fileWarningLabel->setFont(font);

        gridLayout->addWidget(fileWarningLabel, 1, 0, 1, 1);

        buttonBox = new QDialogButtonBox(LoadGlyphSheetDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 2, 0, 1, 1);


        retranslateUi(LoadGlyphSheetDialog);

        QMetaObject::connectSlotsByName(LoadGlyphSheetDialog);
    } // setupUi

    void retranslateUi(QDialog *LoadGlyphSheetDialog)
    {
        LoadGlyphSheetDialog->setWindowTitle(QApplication::translate("LoadGlyphSheetDialog", "LoadGlyphSheetDialog", 0));
        label->setText(QApplication::translate("LoadGlyphSheetDialog", "Glyph Sheet Path:", 0));
        sheetPathSelector->setText(QApplication::translate("LoadGlyphSheetDialog", "...", 0));
        label_2->setText(QApplication::translate("LoadGlyphSheetDialog", "Glyph Dimensions:", 0));
        label_4->setText(QApplication::translate("LoadGlyphSheetDialog", "Width:", 0));
        glyphWidthSpin->setSuffix(QApplication::translate("LoadGlyphSheetDialog", "px", 0));
        glyphWidthSpin->setPrefix(QString());
        label_3->setText(QApplication::translate("LoadGlyphSheetDialog", "Height:", 0));
        glyphHeightSpin->setSuffix(QApplication::translate("LoadGlyphSheetDialog", "px", 0));
        fileWarningLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class LoadGlyphSheetDialog: public Ui_LoadGlyphSheetDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOADGLYPHSHEETDIALOG_H
