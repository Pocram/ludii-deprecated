/********************************************************************************
** Form generated from reading UI file 'ludii_textureeditor.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LUDII_TEXTUREEDITOR_H
#define UI_LUDII_TEXTUREEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "src/LUDII_Texture_Editor/Widgets/GlyphSelector.hpp"
#include "src/LUDII_Texture_Editor/Widgets/TextureCanvas.hpp"

QT_BEGIN_NAMESPACE

class Ui_LUDII_TextureEditorClass
{
public:
    QAction *actionNew;
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionSave_as;
    QAction *actionClose;
    QAction *actionLoad_Glyph_Sheet;
    QAction *actionCanvas_Size;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *textureCanvasGroup;
    ludii::texture_editor::TextureCanvas *sfmlView;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *centerTextureButton;
    QPushButton *resetZoomButton;
    QVBoxLayout *toolBarGroup;
    ludii::texture_editor::GlyphSelector *glyphSelector;
    QGroupBox *toolBox;
    QGridLayout *gridLayout_2;
    QPushButton *pencilToolButton;
    QPushButton *fillToolButton;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *foregroundColorButton;
    QPushButton *backgroundColorButton;
    QMenuBar *mainMenuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuImage;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *LUDII_TextureEditorClass)
    {
        if (LUDII_TextureEditorClass->objectName().isEmpty())
            LUDII_TextureEditorClass->setObjectName(QStringLiteral("LUDII_TextureEditorClass"));
        LUDII_TextureEditorClass->resize(914, 530);
        QIcon icon;
        icon.addFile(QStringLiteral("../../icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        LUDII_TextureEditorClass->setWindowIcon(icon);
        actionNew = new QAction(LUDII_TextureEditorClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionOpen = new QAction(LUDII_TextureEditorClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(LUDII_TextureEditorClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave->setEnabled(false);
        actionSave_as = new QAction(LUDII_TextureEditorClass);
        actionSave_as->setObjectName(QStringLiteral("actionSave_as"));
        actionClose = new QAction(LUDII_TextureEditorClass);
        actionClose->setObjectName(QStringLiteral("actionClose"));
        actionLoad_Glyph_Sheet = new QAction(LUDII_TextureEditorClass);
        actionLoad_Glyph_Sheet->setObjectName(QStringLiteral("actionLoad_Glyph_Sheet"));
        actionCanvas_Size = new QAction(LUDII_TextureEditorClass);
        actionCanvas_Size->setObjectName(QStringLiteral("actionCanvas_Size"));
        centralWidget = new QWidget(LUDII_TextureEditorClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        textureCanvasGroup = new QVBoxLayout();
        textureCanvasGroup->setSpacing(6);
        textureCanvasGroup->setObjectName(QStringLiteral("textureCanvasGroup"));
        textureCanvasGroup->setSizeConstraint(QLayout::SetMinAndMaxSize);
        textureCanvasGroup->setContentsMargins(-1, -1, 0, -1);
        sfmlView = new ludii::texture_editor::TextureCanvas(centralWidget);
        sfmlView->setObjectName(QStringLiteral("sfmlView"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sfmlView->sizePolicy().hasHeightForWidth());
        sfmlView->setSizePolicy(sizePolicy);
        sfmlView->setMinimumSize(QSize(750, 256));
        sfmlView->setSizeIncrement(QSize(8, 8));
        sfmlView->setAutoFillBackground(false);
        sfmlView->setStyleSheet(QStringLiteral("background: #ff00ff;"));

        textureCanvasGroup->addWidget(sfmlView);

        textureCanvasGroup->setStretch(0, 1);

        gridLayout->addLayout(textureCanvasGroup, 0, 2, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        centerTextureButton = new QPushButton(centralWidget);
        centerTextureButton->setObjectName(QStringLiteral("centerTextureButton"));
        centerTextureButton->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout->addWidget(centerTextureButton);

        resetZoomButton = new QPushButton(centralWidget);
        resetZoomButton->setObjectName(QStringLiteral("resetZoomButton"));

        horizontalLayout->addWidget(resetZoomButton);


        gridLayout->addLayout(horizontalLayout, 1, 2, 1, 2);

        toolBarGroup = new QVBoxLayout();
        toolBarGroup->setSpacing(6);
        toolBarGroup->setObjectName(QStringLiteral("toolBarGroup"));
        toolBarGroup->setSizeConstraint(QLayout::SetFixedSize);
        toolBarGroup->setContentsMargins(-1, 0, -1, -1);
        glyphSelector = new ludii::texture_editor::GlyphSelector(centralWidget);
        glyphSelector->setObjectName(QStringLiteral("glyphSelector"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(glyphSelector->sizePolicy().hasHeightForWidth());
        glyphSelector->setSizePolicy(sizePolicy1);
        glyphSelector->setMinimumSize(QSize(8, 8));
        glyphSelector->setMaximumSize(QSize(512, 16777215));
        glyphSelector->setStyleSheet(QStringLiteral("background: #000;"));

        toolBarGroup->addWidget(glyphSelector);

        toolBox = new QGroupBox(centralWidget);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        gridLayout_2 = new QGridLayout(toolBox);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        pencilToolButton = new QPushButton(toolBox);
        pencilToolButton->setObjectName(QStringLiteral("pencilToolButton"));
        pencilToolButton->setEnabled(false);

        gridLayout_2->addWidget(pencilToolButton, 0, 0, 1, 1);

        fillToolButton = new QPushButton(toolBox);
        fillToolButton->setObjectName(QStringLiteral("fillToolButton"));

        gridLayout_2->addWidget(fillToolButton, 1, 0, 1, 1);


        toolBarGroup->addWidget(toolBox);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetFixedSize);
        foregroundColorButton = new QPushButton(centralWidget);
        foregroundColorButton->setObjectName(QStringLiteral("foregroundColorButton"));
        foregroundColorButton->setMinimumSize(QSize(50, 50));
        foregroundColorButton->setMaximumSize(QSize(50, 50));
        foregroundColorButton->setStyleSheet(QLatin1String("QPushButton {\n"
"	background: #FFF;\n"
"}"));

        horizontalLayout_2->addWidget(foregroundColorButton);

        backgroundColorButton = new QPushButton(centralWidget);
        backgroundColorButton->setObjectName(QStringLiteral("backgroundColorButton"));
        backgroundColorButton->setMinimumSize(QSize(50, 50));
        backgroundColorButton->setMaximumSize(QSize(50, 50));
        backgroundColorButton->setStyleSheet(QLatin1String("QPushButton {\n"
"	background: #000;\n"
"}"));

        horizontalLayout_2->addWidget(backgroundColorButton);


        toolBarGroup->addLayout(horizontalLayout_2);


        gridLayout->addLayout(toolBarGroup, 0, 0, 1, 1);

        LUDII_TextureEditorClass->setCentralWidget(centralWidget);
        mainMenuBar = new QMenuBar(LUDII_TextureEditorClass);
        mainMenuBar->setObjectName(QStringLiteral("mainMenuBar"));
        mainMenuBar->setGeometry(QRect(0, 0, 914, 21));
        menuFile = new QMenu(mainMenuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEdit = new QMenu(mainMenuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuImage = new QMenu(mainMenuBar);
        menuImage->setObjectName(QStringLiteral("menuImage"));
        LUDII_TextureEditorClass->setMenuBar(mainMenuBar);
        statusBar = new QStatusBar(LUDII_TextureEditorClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        LUDII_TextureEditorClass->setStatusBar(statusBar);

        mainMenuBar->addAction(menuFile->menuAction());
        mainMenuBar->addAction(menuEdit->menuAction());
        mainMenuBar->addAction(menuImage->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addSeparator();
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSave_as);
        menuFile->addSeparator();
        menuFile->addAction(actionClose);
        menuEdit->addAction(actionLoad_Glyph_Sheet);
        menuImage->addAction(actionCanvas_Size);

        retranslateUi(LUDII_TextureEditorClass);

        QMetaObject::connectSlotsByName(LUDII_TextureEditorClass);
    } // setupUi

    void retranslateUi(QMainWindow *LUDII_TextureEditorClass)
    {
        LUDII_TextureEditorClass->setWindowTitle(QApplication::translate("LUDII_TextureEditorClass", "LUDII Texture Editor", 0));
        actionNew->setText(QApplication::translate("LUDII_TextureEditorClass", "New...", 0));
        actionNew->setShortcut(QApplication::translate("LUDII_TextureEditorClass", "Ctrl+N", 0));
        actionOpen->setText(QApplication::translate("LUDII_TextureEditorClass", "Open...", 0));
        actionOpen->setShortcut(QApplication::translate("LUDII_TextureEditorClass", "Ctrl+O", 0));
        actionSave->setText(QApplication::translate("LUDII_TextureEditorClass", "Save", 0));
        actionSave->setShortcut(QApplication::translate("LUDII_TextureEditorClass", "Ctrl+S", 0));
        actionSave_as->setText(QApplication::translate("LUDII_TextureEditorClass", "Save as...", 0));
        actionSave_as->setShortcut(QApplication::translate("LUDII_TextureEditorClass", "Ctrl+Shift+S", 0));
        actionClose->setText(QApplication::translate("LUDII_TextureEditorClass", "Close", 0));
        actionLoad_Glyph_Sheet->setText(QApplication::translate("LUDII_TextureEditorClass", "Load Glyph Sheet...", 0));
        actionLoad_Glyph_Sheet->setShortcut(QApplication::translate("LUDII_TextureEditorClass", "Ctrl+G", 0));
        actionCanvas_Size->setText(QApplication::translate("LUDII_TextureEditorClass", "Canvas Size...", 0));
        actionCanvas_Size->setShortcut(QApplication::translate("LUDII_TextureEditorClass", "Ctrl+Alt+C", 0));
        centerTextureButton->setText(QApplication::translate("LUDII_TextureEditorClass", "Center Texture", 0));
        resetZoomButton->setText(QApplication::translate("LUDII_TextureEditorClass", "100%", 0));
        toolBox->setTitle(QApplication::translate("LUDII_TextureEditorClass", "Tools", 0));
        pencilToolButton->setText(QApplication::translate("LUDII_TextureEditorClass", "Pencil", 0));
        fillToolButton->setText(QApplication::translate("LUDII_TextureEditorClass", "Fill", 0));
        foregroundColorButton->setText(QString());
        backgroundColorButton->setText(QString());
        menuFile->setTitle(QApplication::translate("LUDII_TextureEditorClass", "File", 0));
        menuEdit->setTitle(QApplication::translate("LUDII_TextureEditorClass", "Edit", 0));
        menuImage->setTitle(QApplication::translate("LUDII_TextureEditorClass", "Image", 0));
    } // retranslateUi

};

namespace Ui {
    class LUDII_TextureEditorClass: public Ui_LUDII_TextureEditorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LUDII_TEXTUREEDITOR_H
