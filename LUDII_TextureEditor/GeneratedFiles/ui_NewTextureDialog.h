/********************************************************************************
** Form generated from reading UI file 'NewTextureDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWTEXTUREDIALOG_H
#define UI_NEWTEXTUREDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_NewTextureDialog
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox;
    QFormLayout *formLayout_2;
    QLabel *label;
    QLabel *label_2;
    QSpinBox *widthSpin;
    QSpinBox *heightSpin;

    void setupUi(QDialog *NewTextureDialog)
    {
        if (NewTextureDialog->objectName().isEmpty())
            NewTextureDialog->setObjectName(QStringLiteral("NewTextureDialog"));
        NewTextureDialog->setWindowModality(Qt::WindowModal);
        NewTextureDialog->setEnabled(true);
        NewTextureDialog->resize(310, 126);
        NewTextureDialog->setMinimumSize(QSize(310, 126));
        NewTextureDialog->setMaximumSize(QSize(310, 126));
        gridLayout = new QGridLayout(NewTextureDialog);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        buttonBox = new QDialogButtonBox(NewTextureDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);

        groupBox = new QGroupBox(NewTextureDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        formLayout_2 = new QFormLayout(groupBox);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_2);

        widthSpin = new QSpinBox(groupBox);
        widthSpin->setObjectName(QStringLiteral("widthSpin"));
        widthSpin->setMinimum(1);
        widthSpin->setMaximum(1024);
        widthSpin->setValue(10);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, widthSpin);

        heightSpin = new QSpinBox(groupBox);
        heightSpin->setObjectName(QStringLiteral("heightSpin"));
        heightSpin->setMinimum(1);
        heightSpin->setMaximum(1024);
        heightSpin->setValue(10);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, heightSpin);


        gridLayout->addWidget(groupBox, 0, 0, 1, 1);


        retranslateUi(NewTextureDialog);

        QMetaObject::connectSlotsByName(NewTextureDialog);
    } // setupUi

    void retranslateUi(QDialog *NewTextureDialog)
    {
        NewTextureDialog->setWindowTitle(QApplication::translate("NewTextureDialog", "New Texture", 0));
        groupBox->setTitle(QApplication::translate("NewTextureDialog", "Dimensions", 0));
        label->setText(QApplication::translate("NewTextureDialog", "Width", 0));
        label_2->setText(QApplication::translate("NewTextureDialog", "Height", 0));
        widthSpin->setSuffix(QApplication::translate("NewTextureDialog", " cells", 0));
        heightSpin->setSuffix(QApplication::translate("NewTextureDialog", " cells", 0));
    } // retranslateUi

};

namespace Ui {
    class NewTextureDialog: public Ui_NewTextureDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWTEXTUREDIALOG_H
