/********************************************************************************
** Form generated from reading UI file 'SetSizeDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETSIZEDIALOG_H
#define UI_SETSIZEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_SetSizeDialog
{
public:
    QGridLayout *gridLayout;
    QGroupBox *currentzSizeGroup;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *currentHeightText;
    QLabel *currentWidthText;
    QGroupBox *newSizeGroup;
    QFormLayout *formLayout_2;
    QLabel *label_5;
    QSpinBox *newWidthSpin;
    QLabel *label_6;
    QSpinBox *newHeightSpin;
    QGridLayout *anchorGroup;
    QRadioButton *topLeftAnchorButton;
    QRadioButton *topMiddleAnchorButton;
    QRadioButton *middleRightAnchorButton;
    QRadioButton *topRightAnchorButton;
    QRadioButton *bottomRightAnchorButton;
    QRadioButton *centerAnchorButton;
    QRadioButton *bottomMiddleAnchorButton;
    QRadioButton *middleLeftAnchorButton;
    QRadioButton *bottomLeftAnchorButton;
    QLabel *label_3;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SetSizeDialog)
    {
        if (SetSizeDialog->objectName().isEmpty())
            SetSizeDialog->setObjectName(QStringLiteral("SetSizeDialog"));
        SetSizeDialog->setWindowModality(Qt::WindowModal);
        SetSizeDialog->resize(350, 268);
        SetSizeDialog->setMinimumSize(QSize(350, 268));
        SetSizeDialog->setMaximumSize(QSize(350, 268));
        SetSizeDialog->setModal(true);
        gridLayout = new QGridLayout(SetSizeDialog);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        currentzSizeGroup = new QGroupBox(SetSizeDialog);
        currentzSizeGroup->setObjectName(QStringLiteral("currentzSizeGroup"));
        formLayout = new QFormLayout(currentzSizeGroup);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(currentzSizeGroup);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(currentzSizeGroup);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        currentHeightText = new QLabel(currentzSizeGroup);
        currentHeightText->setObjectName(QStringLiteral("currentHeightText"));

        formLayout->setWidget(1, QFormLayout::FieldRole, currentHeightText);

        currentWidthText = new QLabel(currentzSizeGroup);
        currentWidthText->setObjectName(QStringLiteral("currentWidthText"));

        formLayout->setWidget(0, QFormLayout::FieldRole, currentWidthText);


        gridLayout->addWidget(currentzSizeGroup, 0, 0, 1, 1);

        newSizeGroup = new QGroupBox(SetSizeDialog);
        newSizeGroup->setObjectName(QStringLiteral("newSizeGroup"));
        formLayout_2 = new QFormLayout(newSizeGroup);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_5 = new QLabel(newSizeGroup);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_5);

        newWidthSpin = new QSpinBox(newSizeGroup);
        newWidthSpin->setObjectName(QStringLiteral("newWidthSpin"));
        newWidthSpin->setMinimum(1);
        newWidthSpin->setMaximum(2048);
        newWidthSpin->setValue(1);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, newWidthSpin);

        label_6 = new QLabel(newSizeGroup);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_6);

        newHeightSpin = new QSpinBox(newSizeGroup);
        newHeightSpin->setObjectName(QStringLiteral("newHeightSpin"));
        newHeightSpin->setMinimum(1);
        newHeightSpin->setMaximum(2048);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, newHeightSpin);

        anchorGroup = new QGridLayout();
        anchorGroup->setSpacing(6);
        anchorGroup->setObjectName(QStringLiteral("anchorGroup"));
        anchorGroup->setContentsMargins(-1, -1, -1, 0);
        topLeftAnchorButton = new QRadioButton(newSizeGroup);
        topLeftAnchorButton->setObjectName(QStringLiteral("topLeftAnchorButton"));

        anchorGroup->addWidget(topLeftAnchorButton, 0, 0, 1, 1);

        topMiddleAnchorButton = new QRadioButton(newSizeGroup);
        topMiddleAnchorButton->setObjectName(QStringLiteral("topMiddleAnchorButton"));

        anchorGroup->addWidget(topMiddleAnchorButton, 0, 1, 1, 1);

        middleRightAnchorButton = new QRadioButton(newSizeGroup);
        middleRightAnchorButton->setObjectName(QStringLiteral("middleRightAnchorButton"));

        anchorGroup->addWidget(middleRightAnchorButton, 1, 2, 1, 1);

        topRightAnchorButton = new QRadioButton(newSizeGroup);
        topRightAnchorButton->setObjectName(QStringLiteral("topRightAnchorButton"));

        anchorGroup->addWidget(topRightAnchorButton, 0, 2, 1, 1);

        bottomRightAnchorButton = new QRadioButton(newSizeGroup);
        bottomRightAnchorButton->setObjectName(QStringLiteral("bottomRightAnchorButton"));

        anchorGroup->addWidget(bottomRightAnchorButton, 2, 2, 1, 1);

        centerAnchorButton = new QRadioButton(newSizeGroup);
        centerAnchorButton->setObjectName(QStringLiteral("centerAnchorButton"));
        centerAnchorButton->setChecked(true);

        anchorGroup->addWidget(centerAnchorButton, 1, 1, 1, 1);

        bottomMiddleAnchorButton = new QRadioButton(newSizeGroup);
        bottomMiddleAnchorButton->setObjectName(QStringLiteral("bottomMiddleAnchorButton"));

        anchorGroup->addWidget(bottomMiddleAnchorButton, 2, 1, 1, 1);

        middleLeftAnchorButton = new QRadioButton(newSizeGroup);
        middleLeftAnchorButton->setObjectName(QStringLiteral("middleLeftAnchorButton"));

        anchorGroup->addWidget(middleLeftAnchorButton, 1, 0, 1, 1);

        bottomLeftAnchorButton = new QRadioButton(newSizeGroup);
        bottomLeftAnchorButton->setObjectName(QStringLiteral("bottomLeftAnchorButton"));

        anchorGroup->addWidget(bottomLeftAnchorButton, 2, 0, 1, 1);


        formLayout_2->setLayout(2, QFormLayout::FieldRole, anchorGroup);

        label_3 = new QLabel(newSizeGroup);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_3);


        gridLayout->addWidget(newSizeGroup, 1, 0, 1, 1);

        buttonBox = new QDialogButtonBox(SetSizeDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 2, 0, 1, 1);


        retranslateUi(SetSizeDialog);

        QMetaObject::connectSlotsByName(SetSizeDialog);
    } // setupUi

    void retranslateUi(QDialog *SetSizeDialog)
    {
        SetSizeDialog->setWindowTitle(QApplication::translate("SetSizeDialog", "Set Canvas Size", 0));
        currentzSizeGroup->setTitle(QApplication::translate("SetSizeDialog", "Current Size", 0));
        label->setText(QApplication::translate("SetSizeDialog", "Width:", 0));
        label_2->setText(QApplication::translate("SetSizeDialog", "Height:", 0));
        currentHeightText->setText(QApplication::translate("SetSizeDialog", "0 Cells", 0));
        currentWidthText->setText(QApplication::translate("SetSizeDialog", "0 Cells", 0));
        newSizeGroup->setTitle(QApplication::translate("SetSizeDialog", "New Size", 0));
        label_5->setText(QApplication::translate("SetSizeDialog", "Width:", 0));
        newWidthSpin->setSuffix(QApplication::translate("SetSizeDialog", " Cells", 0));
        label_6->setText(QApplication::translate("SetSizeDialog", "Height:", 0));
        newHeightSpin->setSuffix(QApplication::translate("SetSizeDialog", " Cells", 0));
        topLeftAnchorButton->setText(QApplication::translate("SetSizeDialog", "Top Left", 0));
        topMiddleAnchorButton->setText(QApplication::translate("SetSizeDialog", "Top Middle", 0));
        middleRightAnchorButton->setText(QApplication::translate("SetSizeDialog", "Middle Right", 0));
        topRightAnchorButton->setText(QApplication::translate("SetSizeDialog", "Top Right", 0));
        bottomRightAnchorButton->setText(QApplication::translate("SetSizeDialog", "Bottom Right", 0));
        centerAnchorButton->setText(QApplication::translate("SetSizeDialog", "Center", 0));
        bottomMiddleAnchorButton->setText(QApplication::translate("SetSizeDialog", "Bottom Middle", 0));
        middleLeftAnchorButton->setText(QApplication::translate("SetSizeDialog", "Middle Left", 0));
        bottomLeftAnchorButton->setText(QApplication::translate("SetSizeDialog", "Bottom Left", 0));
        label_3->setText(QApplication::translate("SetSizeDialog", "Anchor:", 0));
    } // retranslateUi

};

namespace Ui {
    class SetSizeDialog: public Ui_SetSizeDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETSIZEDIALOG_H
