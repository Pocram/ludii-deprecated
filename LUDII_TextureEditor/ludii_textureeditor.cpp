#include "ludii_textureeditor.hpp"
#include <iostream>

#include <LUDII_TEXTURE_EDITOR/Dialogs/LoadGlyphSheetDialog.hpp>
#include <LUDII/Graphics/GlyphSheet.hpp>

#include <QFileDialog>
#include <QColorDialog>

#include <LUDII_TEXTURE_EDITOR/Tools/PencilTool.hpp>
#include <LUDII_TEXTURE_EDITOR/Tools/FillTool.hpp>

ludii::texture_editor::LUDII_TextureEditor::LUDII_TextureEditor( QWidget *parent )
	: QMainWindow( parent ),
	_glyphSheetDialog( nullptr ),
	_newFileDialog( nullptr ),
	_canvasSizeDialog( nullptr )
{
	_brushInfo.glyphIndex = 0;
	_brushInfo.foregroundColor = QColor( 255, 255, 255 );
	_brushInfo.backgroundColor = QColor( 0, 0, 0 );

	setupUi( this );

	QObject::connect( mainMenuBar, SIGNAL( triggered( QAction* ) ), this, SLOT( onMenuBarTriggered( QAction* ) ) );
	QObject::connect( this, &ludii::texture_editor::LUDII_TextureEditor::onTextureLoaded, sfmlView, &ludii::texture_editor::TextureCanvas::setTexture );
	QObject::connect( this, &ludii::texture_editor::LUDII_TextureEditor::onGlyphSheetLoaded, sfmlView, &ludii::texture_editor::TextureCanvas::setGlyphSheet );
	QObject::connect( this, &ludii::texture_editor::LUDII_TextureEditor::onGlyphSheetLoaded, glyphSelector, &ludii::texture_editor::GlyphSelector::setGlyphSheet );
	
	QObject::connect( foregroundColorButton, &QPushButton::clicked, this, &ludii::texture_editor::LUDII_TextureEditor::onForegroundColorButtonClick );
	QObject::connect( backgroundColorButton, &QPushButton::clicked, this, &ludii::texture_editor::LUDII_TextureEditor::onBackgroundColorButtonClick );
	QObject::connect( this, &ludii::texture_editor::LUDII_TextureEditor::onBrushInfoChange, sfmlView, &ludii::texture_editor::TextureCanvas::onBrushInfoChange );
	QObject::connect( this, &ludii::texture_editor::LUDII_TextureEditor::onBrushInfoChange, glyphSelector, &ludii::texture_editor::GlyphSelector::onBrushInfoChange );
	
	QObject::connect( centerTextureButton, &QPushButton::clicked, this, &ludii::texture_editor::LUDII_TextureEditor::onRecenterButtonClick );

	QObject::connect( glyphSelector, &ludii::texture_editor::GlyphSelector::onGlyphSelect, this, &ludii::texture_editor::LUDII_TextureEditor::setSelectedGlyphIndex );

	QObject::connect( pencilToolButton, &QPushButton::clicked, this, &ludii::texture_editor::LUDII_TextureEditor::onPencilToolButtonClick );
	QObject::connect( fillToolButton, &QPushButton::clicked, this, &ludii::texture_editor::LUDII_TextureEditor::onFillToolButtonClick );

	initializeMenuTriggerFunctions();

	onMenuClick_loadStyleSheet();
}

ludii::texture_editor::LUDII_TextureEditor::~LUDII_TextureEditor()
{

}

void ludii::texture_editor::LUDII_TextureEditor::onMenuClick_loadStyleSheet()
{
	if( _glyphSheetDialog != nullptr )
	{
		delete _glyphSheetDialog;
	}
	_glyphSheetDialog = new ludii::texture_editor::LoadGlyphSheetDialog( this );
	_glyphSheetDialog->setWindowModality( Qt::WindowModality::WindowModal );

	QObject::connect( _glyphSheetDialog, &LoadGlyphSheetDialog::onGlyphSheetLoad, this, &LUDII_TextureEditor::onGlyphSheetLoad );

	_glyphSheetDialog->show();
}

void ludii::texture_editor::LUDII_TextureEditor::onMenuClick_canvasSize()
{
	if( _canvasSizeDialog != nullptr )
	{
		delete _canvasSizeDialog;
	}
	_canvasSizeDialog = new ludii::texture_editor::SetSizeDialog( sfmlView->getTexture(), this );
	QObject::connect( _canvasSizeDialog, &ludii::texture_editor::SetSizeDialog::onSizeSet, sfmlView, &ludii::texture_editor::TextureCanvas::setCanvasSize );
	
	_canvasSizeDialog->show();
}

void ludii::texture_editor::LUDII_TextureEditor::onMenuClick_newFile()
{
	if( _newFileDialog != nullptr )
	{
		delete _newFileDialog;
	}

	_newFileDialog = new ludii::texture_editor::NewTextureDialog( this );
	QObject::connect( _newFileDialog, &NewTextureDialog::onCreateNewTexture, this, &LUDII_TextureEditor::onNewTextureCreate );
	
	_newFileDialog->show();
}

void ludii::texture_editor::LUDII_TextureEditor::onMenuClick_openFile()
{
	QString fileName = QFileDialog::getOpenFileName( this, tr( "Open LUDII Texture File" ), "", tr( "LUDII Texture Files (*.ludtex)" ) );
	ludii::Texture texture;
	if( texture.loadFromFile( fileName.toStdString() ) )
	{
		emit onTextureLoaded( texture );
	}
	else
	{
		std::cout << "Error: Could not load texture at " << fileName.toStdString() << "\n";
	}
}

void ludii::texture_editor::LUDII_TextureEditor::onMenuClick_exitEditor()
{
	close();
}

void ludii::texture_editor::LUDII_TextureEditor::onMenuClick_saveFile()
{
}

void ludii::texture_editor::LUDII_TextureEditor::onMenuClick_saveFileAs()
{
	QString targetFile = QFileDialog::getSaveFileName( this, tr( "Save Texture" ), QString(), tr( "LUDII Texture Files (*.ludtex)" ) );
	ludii::Texture texture = sfmlView->getTexture();
	if( texture.saveToFile( targetFile.toStdString() ) )
	{
		std::cout << "Texture has been saved successfully!\n";
	}
	else
	{
		std::cout << "Error: Could not save texture at " << targetFile.toStdString() << "\n";
	}
}

void ludii::texture_editor::LUDII_TextureEditor::setSelectedGlyphIndex( ludii::uInt16 index )
{
	_brushInfo.glyphIndex = index;
	std::cout << "New selected glyph index: " << index << "\n";

	emit onBrushInfoChange( _brushInfo );
}

void ludii::texture_editor::LUDII_TextureEditor::initializeMenuTriggerFunctions()
{
	// FILE
	_menuTriggerActions.insert_or_assign( "actionNew", &LUDII_TextureEditor::onMenuClick_newFile );
	_menuTriggerActions.insert_or_assign( "actionOpen", &LUDII_TextureEditor::onMenuClick_openFile );
	_menuTriggerActions.insert_or_assign( "actionClose", &LUDII_TextureEditor::onMenuClick_exitEditor );
	_menuTriggerActions.insert_or_assign( "actionSave", &LUDII_TextureEditor::onMenuClick_saveFile );
	_menuTriggerActions.insert_or_assign( "actionSave_as", &LUDII_TextureEditor::onMenuClick_saveFileAs );
	
	// EDIT
	_menuTriggerActions.insert_or_assign( "actionLoad_Glyph_Sheet", &LUDII_TextureEditor::onMenuClick_loadStyleSheet );

	// IMAGE
	_menuTriggerActions.insert_or_assign( "actionCanvas_Size", &LUDII_TextureEditor::onMenuClick_canvasSize );
}

void ludii::texture_editor::LUDII_TextureEditor::onNewTextureCreate( ludii::texture_editor::onCreateNewTextureEventArgs args )
{
	sfmlView->setTexture( ludii::Texture( args.width, args.height ) );
}

void ludii::texture_editor::LUDII_TextureEditor::onMenuBarTriggered( QAction* action )
{
	if ( _menuTriggerActions.find( action->objectName() ) != _menuTriggerActions.end() )
	{
		menuTriggerActionFunction function = _menuTriggerActions[action->objectName()];
		( this->*function )( );
	}
}

void ludii::texture_editor::LUDII_TextureEditor::onGlyphSheetLoad( ludii::texture_editor::OnGlyphSheetLoadEventInfo info )
{
	ludii::GlyphSheet* glyphSheet = new ludii::GlyphSheet();
	if( glyphSheet->loadFromFile( info.pathToFile, info.glyphDimensions.x, info.glyphDimensions.y ) )
	{
		//if( _glyphSheet != nullptr )
		//{
		//	delete _glyphSheet;
		//}
		_glyphSheet = glyphSheet;

		emit onGlyphSheetLoaded( _glyphSheet );
	}
	else
	{
		std::cout << "Error: Could not load GlyphSheet at " << info.pathToFile << "\n";
	}
}

void ludii::texture_editor::LUDII_TextureEditor::onForegroundColorButtonClick( bool checked )
{
	QColor newColor = QColorDialog::getColor( _brushInfo.foregroundColor, this, tr( "Set Foreground Color" ) );
	if( newColor.isValid() )
	{
		_brushInfo.foregroundColor = newColor;
		foregroundColorButton->setStyleSheet( "background: " + _brushInfo.foregroundColor.name() + ";" );

		emit onBrushInfoChange( _brushInfo );
	}
}

void ludii::texture_editor::LUDII_TextureEditor::onBackgroundColorButtonClick( bool checked )
{
	QColor newColor = QColorDialog::getColor( _brushInfo.backgroundColor, this, tr( "Set Background Color" ) );
	if ( newColor.isValid() )
	{
		_brushInfo.backgroundColor = newColor;
		backgroundColorButton->setStyleSheet( "background: " + _brushInfo.backgroundColor.name() + ";" );

		emit onBrushInfoChange( _brushInfo );
	}
}

void ludii::texture_editor::LUDII_TextureEditor::onRecenterButtonClick( bool checked )
{
	sfmlView->recenterTexture();
}

void ludii::texture_editor::LUDII_TextureEditor::onPencilToolButtonClick( bool checked )
{
	sfmlView->setTool( new ludii::texture_editor::PencilTool() );
	
	pencilToolButton->setEnabled( false );
	fillToolButton->setEnabled( true );
}

void ludii::texture_editor::LUDII_TextureEditor::onFillToolButtonClick( bool checked )
{
	sfmlView->setTool( new ludii::texture_editor::FillTool() );

	pencilToolButton->setEnabled( true );
	fillToolButton->setEnabled( false );
}
