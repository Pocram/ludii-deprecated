#ifndef LUDII_TEXTUREEDITOR_HPP
#define LUDII_TEXTUREEDITOR_HPP

#include <QtWidgets/QMainWindow>
#include "ui_ludii_textureeditor.h"

#include <LUDII/Graphics/Texture.hpp>
#include <LUDII/Graphics/GlyphSheet.hpp>

#include <LUDII_TEXTURE_EDITOR/Dialogs/LoadGlyphSheetDialog.hpp>
#include <LUDII_TEXTURE_EDITOR/Dialogs/NewTextureDialog.hpp>
#include <LUDII_TEXTURE_EDITOR/Dialogs/SetSizeDialog.hpp>

#include <map>
#include <QString>

#include <QColor>
#include "src/LUDII_Texture_Editor/BrushInfo.hpp"

namespace ludii
{
	namespace texture_editor
	{
		struct OnGlyphSheetLoadEventInfo;

		class LUDII_TextureEditor : public QMainWindow, public Ui::LUDII_TextureEditorClass
		{
			Q_OBJECT

		public:
			LUDII_TextureEditor( QWidget *parent = 0 );
			~LUDII_TextureEditor();


		signals:
			void onGlyphSheetLoaded( ludii::GlyphSheet* glyphSheet );
			void onTextureLoaded( ludii::Texture texture );
			void onBrushInfoChange( ludii::texture_editor::BrushInfo brushInfo );


		public slots:
			void setSelectedGlyphIndex( ludii::uInt16 index );


		private:
			void initializeMenuTriggerFunctions();
			
			void onMenuClick_newFile();
			void onMenuClick_openFile();
			void onMenuClick_exitEditor();
			void onMenuClick_saveFile();
			void onMenuClick_saveFileAs();

			void onMenuClick_loadStyleSheet();
			
			void onMenuClick_canvasSize();

			typedef void( LUDII_TextureEditor::*menuTriggerActionFunction )( void );
			std::map<QString, menuTriggerActionFunction> _menuTriggerActions;

			ludii::texture_editor::NewTextureDialog*		_newFileDialog;
			ludii::texture_editor::LoadGlyphSheetDialog*	_glyphSheetDialog;
			ludii::texture_editor::SetSizeDialog*			_canvasSizeDialog;

			ludii::GlyphSheet* _glyphSheet;
			ludii::texture_editor::BrushInfo _brushInfo;


		private slots:
			void onNewTextureCreate( ludii::texture_editor::onCreateNewTextureEventArgs args );

			void onMenuBarTriggered( QAction* action );
			void onGlyphSheetLoad( ludii::texture_editor::OnGlyphSheetLoadEventInfo info );

			void onForegroundColorButtonClick( bool checked = false );
			void onBackgroundColorButtonClick( bool checked = false );
			
			void onRecenterButtonClick( bool checked = false );

			void onPencilToolButtonClick( bool checked = false );
			void onFillToolButtonClick( bool checked = false );
		};
	}
}

#endif // LUDII_TEXTUREEDITOR_HPP
