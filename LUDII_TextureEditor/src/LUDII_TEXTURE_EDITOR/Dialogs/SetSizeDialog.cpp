#include "SetSizeDialog.hpp"

#include <iostream>
#include <sstream>
#include <string>

#include <LUDII/Graphics/Texture.hpp>

ludii::texture_editor::SetSizeDialog::SetSizeDialog( const ludii::Texture& texture, QWidget *parent )
	: QDialog( parent )
{
	setupUi( this );

	QObject::connect( buttonBox, SIGNAL( clicked( QAbstractButton* ) ), this, SLOT( onDialogButtonClick( QAbstractButton* ) ) );

	std::stringstream widthStream;
	widthStream << texture.getDimensions().x << " Cells";
	QString widthString = widthStream.str().c_str();
	currentWidthText->setText( widthString );
	newWidthSpin->setValue( texture.getDimensions().x );

	std::stringstream heightStream;
	heightStream << texture.getDimensions().y << " Cells";
	QString heightString = heightStream.str().c_str();
	currentHeightText->setText( heightString );
	newHeightSpin->setValue( texture.getDimensions().y );
}

ludii::texture_editor::SetSizeDialog::~SetSizeDialog()
{

}

void ludii::texture_editor::SetSizeDialog::onDialogButtonClick( QAbstractButton* button )
{
	QPushButton* pressedButton = reinterpret_cast<QPushButton*>( button );
	if ( pressedButton == buttonBox->button( QDialogButtonBox::Ok ) )
	{
		SetSizeDialogOnSizeSetEventArgs args;
		args.width	= newWidthSpin->value();
		args.height	= newHeightSpin->value();
		args.anchor	= getAnchor();

		emit onSizeSet( args );

		close();
	}
	else if ( pressedButton == buttonBox->button( QDialogButtonBox::Cancel ) )
	{
		close();
	}
}

ludii::texture_editor::SizeSetAnchor ludii::texture_editor::SetSizeDialog::getAnchor() const
{
	// Okay now please close your eyes and pretend this is okay.
	if ( topLeftAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::TopLeft;
	if ( topMiddleAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::TopMiddle;
	if ( topRightAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::TopRight;
	if ( middleLeftAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::MiddleLeft;
	if ( middleRightAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::MiddleRight;
	if ( bottomLeftAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::BottomLeft;
	if ( bottomMiddleAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::BottomMiddle;
	if ( bottomRightAnchorButton->isChecked() )
		return ludii::texture_editor::SizeSetAnchor::BottomRight;

	// Center is missing above and acts as the default value.
	return ludii::texture_editor::SizeSetAnchor::Center;
}
