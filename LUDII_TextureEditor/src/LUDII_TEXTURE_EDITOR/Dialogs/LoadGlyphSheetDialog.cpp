#include "LoadGlyphSheetDialog.hpp"

#include <QFiledialog>
#include <QFileInfo>

#include <iostream>

ludii::texture_editor::LoadGlyphSheetDialog::LoadGlyphSheetDialog( QWidget *parent )
	: QDialog( parent )
{
	setupUi( this );

	QObject::connect( sheetPathSelector, SIGNAL( clicked( bool ) ), this, SLOT( onPathButtonClick( bool ) ) );

	QObject::connect( buttonBox, SIGNAL( clicked( QAbstractButton* ) ), this, SLOT( onDialogButtonClick( QAbstractButton* ) ) );
}

ludii::texture_editor::LoadGlyphSheetDialog::~LoadGlyphSheetDialog()
{

}

void ludii::texture_editor::LoadGlyphSheetDialog::onPathButtonClick( bool checked )
{
	QString fileName = QFileDialog::getOpenFileName( this, tr( "Open Image" ), "", tr( "Image Files (*.png *.bmp)" ) );
	sheetPathLine->setText( fileName );
}

void ludii::texture_editor::LoadGlyphSheetDialog::onDialogButtonClick( QAbstractButton* button )
{
	QPushButton* pressedButton = reinterpret_cast<QPushButton*>( button );
	if ( pressedButton == buttonBox->button( QDialogButtonBox::Ok ) )
	{
		// Check if sheet path is valid.
		QFileInfo sheetFile( sheetPathLine->text() );
		if ( sheetFile.exists() && sheetFile.isFile() &&
			( sheetFile.completeSuffix() == "png" || sheetFile.completeSuffix() == "bmp" ) )
		{
			ludii::texture_editor::OnGlyphSheetLoadEventInfo glyphSheetInfo;
			glyphSheetInfo.pathToFile = sheetPathLine->text().toStdString();
			glyphSheetInfo.glyphDimensions = ludii::Vector2u( glyphWidthSpin->value(), glyphHeightSpin->value() );

			emit onGlyphSheetLoad( glyphSheetInfo );

			close();
			return;
		}
		else
		{
			// File is invalid!
			fileWarningLabel->setText( "The selected file is invalid!" );
			return;
		}
	}
	else if ( pressedButton == buttonBox->button( QDialogButtonBox::Cancel ) )
	{
		close();
	}
}
