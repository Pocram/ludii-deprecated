#ifndef SETSIZEDIALOG_HPP
#define SETSIZEDIALOG_HPP

#include <QDialog>
#include "ui_SetSizeDialog.h"

#include <LUDII/Graphics/Texture.hpp>
#include <LUDII/IntDefines.hpp>


namespace ludii
{
	namespace texture_editor
	{
		struct SetSizeDialogOnSizeSetEventArgs;
		enum SizeSetAnchor;

		class SetSizeDialog : public QDialog, public Ui::SetSizeDialog
		{
			Q_OBJECT

		public:
			SetSizeDialog( const ludii::Texture& texture, QWidget *parent = 0 );
			~SetSizeDialog();


		signals:
			void onSizeSet( SetSizeDialogOnSizeSetEventArgs args );


		private slots:
			void onDialogButtonClick( QAbstractButton* button );
			SizeSetAnchor getAnchor() const;
		};

		enum SizeSetAnchor
		{
			TopLeft,
			TopMiddle,
			TopRight,
			MiddleLeft,
			Center,
			MiddleRight,
			BottomLeft,
			BottomMiddle,
			BottomRight
		};

		struct SetSizeDialogOnSizeSetEventArgs
		{
			ludii::uInt32 width;
			ludii::uInt32 height;

			SizeSetAnchor anchor;
		};
	}
}

#endif // SETSIZEDIALOG_HPP
