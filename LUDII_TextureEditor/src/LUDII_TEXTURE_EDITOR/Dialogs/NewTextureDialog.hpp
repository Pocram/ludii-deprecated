#ifndef NEWTEXTUREDIALOG_HPP
#define NEWTEXTUREDIALOG_HPP

#include <QDialog>
#include "ui_NewTextureDialog.h"
#include <LUDII/IntDefines.hpp>

namespace ludii
{
	namespace texture_editor
	{
		struct onCreateNewTextureEventArgs;

		class NewTextureDialog : public QDialog, public Ui::NewTextureDialog
		{
			Q_OBJECT

		public:
			NewTextureDialog( QWidget *parent = 0 );
			~NewTextureDialog();


		signals:
			void onCreateNewTexture( onCreateNewTextureEventArgs args );


			private slots:
			void onDialogButtonClick( QAbstractButton* button );
		};

		struct onCreateNewTextureEventArgs
		{
			ludii::uInt32 width;
			ludii::uInt32 height;
		};
	}
}

#endif // NEWTEXTUREDIALOG_HPP
