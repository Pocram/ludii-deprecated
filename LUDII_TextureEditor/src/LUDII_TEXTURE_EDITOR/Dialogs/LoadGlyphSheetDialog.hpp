#ifndef LOADGLYPHSHEETDIALOG_HPP
#define LOADGLYPHSHEETDIALOG_HPP

#include <QDialog>
#include <GeneratedFiles/ui_LoadGlyphSheetDialog.h>

#include <string>
#include <LUDII/Math/Vector2.hpp>

class QAbstractButton;

namespace ludii
{
	namespace texture_editor
	{
		struct OnGlyphSheetLoadEventInfo
		{
			std::string pathToFile;
			ludii::Vector2u glyphDimensions;
		};


		class LoadGlyphSheetDialog : public QDialog, public Ui::LoadGlyphSheetDialog
		{
			Q_OBJECT

		public:
			LoadGlyphSheetDialog( QWidget *parent = 0 );
			~LoadGlyphSheetDialog();


		signals:
			void onGlyphSheetLoad( OnGlyphSheetLoadEventInfo info );


		private slots:
			void onPathButtonClick( bool checked );
			void onDialogButtonClick( QAbstractButton* button );
		};
	}
}

#endif // LOADGLYPHSHEETDIALOG_HPP