#include "NewTextureDialog.hpp"

ludii::texture_editor::NewTextureDialog::NewTextureDialog(QWidget *parent)
	: QDialog(parent)
{
	setupUi(this);

	QObject::connect( buttonBox, SIGNAL( clicked( QAbstractButton* ) ), this, SLOT( onDialogButtonClick( QAbstractButton* ) ) );
}

ludii::texture_editor::NewTextureDialog::~NewTextureDialog()
{

}

void ludii::texture_editor::NewTextureDialog::onDialogButtonClick( QAbstractButton* button )
{
	QPushButton* pressedButton = reinterpret_cast<QPushButton*>( button );
	if ( pressedButton == buttonBox->button( QDialogButtonBox::Ok ) )
	{
		onCreateNewTextureEventArgs args;
		args.width = widthSpin->value();
		args.height = heightSpin->value();
		
		emit onCreateNewTexture( args );
		close();
	}
	else if ( pressedButton == buttonBox->button( QDialogButtonBox::Cancel ) )
	{
		close();
	}
}
