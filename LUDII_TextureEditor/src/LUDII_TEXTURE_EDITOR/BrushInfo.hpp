#ifndef LUDII_TEXTURE_EDITOR_BRUSH_INFO_HPP
#define LUDII_TEXTURE_EDITOR_BRUSH_INFO_HPP

#include <LUDII/IntDefines.hpp>
#include <QColor>

namespace ludii
{
	namespace texture_editor
	{
		struct BrushInfo
		{
			ludii::uInt16	glyphIndex;

			QColor			foregroundColor;
			QColor			backgroundColor;
		};
	}
}

#endif // LUDII_TEXTURE_EDITOR_BRUSH_INFO_HPP