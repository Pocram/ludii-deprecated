#include "PencilTool.hpp"
#include <iostream>

ludii::texture_editor::PencilTool::PencilTool()
{
}

ludii::texture_editor::PencilTool::~PencilTool()
{
}

bool ludii::texture_editor::PencilTool::onMouseHold( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet )
{
	if( renderedTexture == nullptr || glyphSheet == nullptr )
	{
		return false;
	}

	ludii::Color f( brushInfo.foregroundColor.red(), brushInfo.foregroundColor.green(), brushInfo.foregroundColor.blue() );
	ludii::Color b( brushInfo.backgroundColor.red(), brushInfo.backgroundColor.green(), brushInfo.backgroundColor.blue() );
	ludii::TextureCell newCell( brushInfo.glyphIndex, f, b );

	texture.setCellAt( cellPosition.x, cellPosition.y, newCell );

	// Render the new cell in a 1x1 texture and then copy the image into the new image.
	ludii::Texture tempTexture( 1, 1 );
	tempTexture.setCellAt( 0, 0, newCell );
	sf::Image img = tempTexture.renderToImage( *glyphSheet );
	for ( ludii::uInt32 x = 0; x < img.getSize().x; x++ )
	{
		for ( ludii::uInt32 y = 0; y < img.getSize().y; y++ )
		{
			ludii::uInt32 xInOldImage = x + glyphSheet->getGlyphWidth() * cellPosition.x;
			ludii::uInt32 yInOldImage = y + glyphSheet->getGlyphHeight() * cellPosition.y;

			renderedTexture->setPixel( xInOldImage, yInOldImage, img.getPixel( x, y ) );
		}
	}

	return false;
}
