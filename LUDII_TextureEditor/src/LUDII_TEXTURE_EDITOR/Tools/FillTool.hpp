#ifndef LUDII_TEXTURE_EDITOR_TOOLS_FILL_TOOL_HPP
#define LUDII_TEXTURE_EDITOR_TOOLS_FILL_TOOL_HPP

#include "Tool.hpp"

namespace ludii
{
	namespace texture_editor
	{
		class FillTool : public Tool
		{
		public:
			FillTool();
			virtual ~FillTool();

			bool onMouseDown( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet ) override;
		
		
		private:
			void floodFill( ludii::Vector2i& position, const ludii::TextureCell& targetCell, const ludii::TextureCell& replacementCell, ludii::Texture& texture, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet );
			void setCellAt( const ludii::Vector2i& position, const ludii::TextureCell& cell, ludii::Texture& texture, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet );

			// Stack overflow? More like ... Stack over..no?
			ludii::TextureCell _startingCell;
			ludii::TextureCell _newCell;
			ludii::TextureCell _currentCell;

			ludii::Texture _currentCellTexture;
			sf::Image _currentRenderedCell;
		};
	}
}

#endif // LUDII_TEXTURE_EDITOR_TOOLS_PENCIL_TOOL_HPP