#include "Tool.hpp"

ludii::texture_editor::Tool::Tool()
{
}

ludii::texture_editor::Tool::~Tool()
{
}

bool ludii::texture_editor::Tool::onMouseDown( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet )
{
	return false;
}

bool ludii::texture_editor::Tool::onMouseHold( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet )
{
	return false;
}

bool ludii::texture_editor::Tool::onMouseRelease( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet )
{
	return false;
}

void ludii::texture_editor::Tool::onRightClick()
{
}

void ludii::texture_editor::Tool::onToolSelect()
{
}

void ludii::texture_editor::Tool::onToolDeselect()
{
}
