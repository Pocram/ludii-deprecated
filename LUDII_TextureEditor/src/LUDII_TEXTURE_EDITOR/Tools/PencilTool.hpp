#ifndef LUDII_TEXTURE_EDITOR_TOOLS_PENCIL_TOOL_HPP
#define LUDII_TEXTURE_EDITOR_TOOLS_PENCIL_TOOL_HPP

#include "Tool.hpp"

namespace ludii
{
	namespace texture_editor
	{
		class PencilTool : public Tool
		{
		public:
			PencilTool();
			virtual ~PencilTool();

			bool onMouseHold( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet ) override;
		};
	}
}

#endif // LUDII_TEXTURE_EDITOR_TOOLS_PENCIL_TOOL_HPP