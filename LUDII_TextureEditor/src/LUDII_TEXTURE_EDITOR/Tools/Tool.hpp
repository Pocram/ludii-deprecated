#ifndef LUDII_TEXTURE_EDITOR_TOOLS_TOOL_HPP
#define LUDII_TEXTURE_EDITOR_TOOLS_TOOL_HPP

#include <LUDII/Math/Vector2.hpp>
#include <LUDII/Graphics/TextureCell.hpp>
#include <LUDII/Graphics/Texture.hpp>
#include <LUDII_TEXTURE_EDITOR/BrushInfo.hpp>

#include <SFML/Graphics.hpp>
#include <LUDII/Graphics/GlyphSheet.hpp>

namespace ludii
{
	namespace texture_editor
	{
		class Tool
		{
		public:
			Tool();
			virtual ~Tool();

			virtual bool onMouseDown( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet );
			virtual bool onMouseHold( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet );
			virtual bool onMouseRelease( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet );

			virtual void onRightClick();

			virtual void onToolSelect();
			virtual void onToolDeselect();
		};
	}
}

#endif // LUDII_TEXTURE_EDITOR_TOOLS_TOOL_HPP