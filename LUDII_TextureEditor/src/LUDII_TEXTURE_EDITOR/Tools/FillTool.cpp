#include "FillTool.hpp"
#include <vector>
#include <iostream>

ludii::texture_editor::FillTool::FillTool()
{
}

ludii::texture_editor::FillTool::~FillTool()
{
}

bool ludii::texture_editor::FillTool::onMouseDown( ludii::Vector2i cellPosition, ludii::Texture& texture, ludii::texture_editor::BrushInfo brushInfo, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet )
{
	ludii::Color foregroundColor( brushInfo.foregroundColor.red(), brushInfo.foregroundColor.green(), brushInfo.foregroundColor.blue() );
	ludii::Color backgroundColor( brushInfo.backgroundColor.red(), brushInfo.backgroundColor.green(), brushInfo.backgroundColor.blue() );

	ludii::TextureCell targetCell;
	texture.getCellAt( cellPosition.x, cellPosition.y, targetCell );
	ludii::TextureCell replacementCell( brushInfo.glyphIndex, foregroundColor, backgroundColor );

	floodFill( cellPosition, targetCell, replacementCell, texture, renderedTexture, glyphSheet );

	return false;
}

void ludii::texture_editor::FillTool::floodFill( ludii::Vector2i& position, const ludii::TextureCell& targetCell, const ludii::TextureCell& replacementCell, ludii::Texture& texture, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet )
{
	if ( targetCell == replacementCell )
	{
		return;
	}

	ludii::TextureCell currentCell;
	texture.getCellAt( position.x, position.y, currentCell );
	if( currentCell != targetCell )
	{
		return;
	}

	std::vector<ludii::Vector2i> positionQueue;
	positionQueue.push_back( position );
	for( std::vector<ludii::Vector2i>::size_type i = 0; i < positionQueue.size(); ++i )
	{
		ludii::Vector2i pos( positionQueue[i] );
		texture.getCellAt( pos.x, pos.y, currentCell );

		ludii::TextureCell westCell;
		ludii::TextureCell eastCell;
		ludii::Vector2i westPosition = pos;
		ludii::Vector2i eastPosition = pos;

		// Find west-most fitting cell.
		while ( true )
		{
			if( texture.hasCoordinate( westPosition ) == false )
			{
				break;
			}

			texture.getCellAt( westPosition.x, westPosition.y, westCell );
			if( westCell != targetCell )
			{
				break;
			}
			westPosition.x--;
		}
		westPosition.x++;

		// Find east-most fitting cell.
		while ( true )
		{
			if ( texture.hasCoordinate( eastPosition ) == false )
			{
				break;
			}

			texture.getCellAt( eastPosition.x, eastPosition.y, eastCell );
			if ( eastCell != targetCell )
			{
				break;
			}
			eastPosition.x++;
		}
		eastPosition.x--;

		// Iterate over all cells between west-most and east-most cells.
		ludii::int32 width = eastPosition.x - westPosition.x + 1;
		for( ludii::int32 x = 0; x < width; x++ )
		{
			ludii::Vector2i p( westPosition.x + x, pos.y );
			if( texture.hasCoordinate( p ) == false )
			{
				continue;
			}

			setCellAt( p, replacementCell, texture, renderedTexture, glyphSheet );
			
			// Check north and south cells.
			ludii::Vector2i north( p.x, p.y + 1 );
			if( texture.hasCoordinate( north ) )
			{
				ludii::TextureCell c;
				texture.getCellAt( north.x, north.y, c );
				if ( c == targetCell )
				{
					positionQueue.push_back( north );
				}
			}
			ludii::Vector2i south( p.x, p.y - 1 );
			if ( texture.hasCoordinate( south ) )
			{
				ludii::TextureCell c;
				texture.getCellAt( south.x, south.y, c );
				if ( c == targetCell )
				{
					positionQueue.push_back( south );
				}
			}
		}
	}
}

void ludii::texture_editor::FillTool::setCellAt( const ludii::Vector2i& position, const ludii::TextureCell& cell, ludii::Texture& texture, sf::Image* renderedTexture, ludii::GlyphSheet* glyphSheet )
{
	texture.setCellAt( position.x, position.y, cell );

	// Render the new cell in a 1x1 texture and then copy the image into the new image.
	_currentCellTexture = ludii::Texture( 1, 1 );
	_currentCellTexture.setCellAt( 0, 0, cell );
	_currentRenderedCell = _currentCellTexture.renderToImage( *glyphSheet );
	for ( ludii::uInt32 x = 0; x < _currentRenderedCell.getSize().x; x++ )
	{
		for ( ludii::uInt32 y = 0; y < _currentRenderedCell.getSize().y; y++ )
		{
			ludii::uInt32 xInOldImage = x + glyphSheet->getGlyphWidth() * position.x;
			ludii::uInt32 yInOldImage = y + glyphSheet->getGlyphHeight() * position.y;

			renderedTexture->setPixel( xInOldImage, yInOldImage, _currentRenderedCell.getPixel( x, y ) );
		}
	}
}
