#include "GlyphSelector.hpp"

#include <LUDII/Graphics/GlyphSheet.hpp>
#include <LUDII/Graphics/Texture.hpp>
#include <LUDII_TEXTURE_EDITOR/BrushInfo.hpp>

#include <QResizeEvent>
#include <QApplication>

#include <iostream>
#include <sstream>


ludii::texture_editor::GlyphSelector::GlyphSelector( QWidget* parent ) :
	QSfmlCanvas( parent, QPoint( 0, 0 ), QSize( 256, 256 ) ),
	_glyphSheet( nullptr ),
	_currentScaleFactor( 1 ),
	_currentScrollOffset( 0 ),
	_foregroundColor( 255, 255, 255, 255 ),
	_backgroundColor( 0, 0, 0, 255 ),
	_wasLeftMouseButtonDownPrevious( false )
{
}

ludii::texture_editor::GlyphSelector::GlyphSelector( QWidget* parent, const QPoint& position, const QSize& size, unsigned frameTime ) :
	QSfmlCanvas( parent, position, size ),
	_glyphSheet( nullptr ),
	_currentScaleFactor( 1 ),
	_currentScrollOffset( 0 ),
	_foregroundColor( 255, 255, 255, 255 ),
	_backgroundColor( 0, 0, 0, 255 ),
	_wasLeftMouseButtonDownPrevious( false )
{
}

void ludii::texture_editor::GlyphSelector::onInit()
{
}

void ludii::texture_editor::GlyphSelector::onUpdate()
{
	RenderWindow::clear( sf::Color( _backgroundColor.red(), _backgroundColor.green(), _backgroundColor.blue(), 255 ) );

	sf::Event event;
	while ( pollEvent( event ) )
	{
		if( QApplication::activeModalWidget() == nullptr )
		{
			if ( event.type == sf::Event::MouseWheelScrolled )
			{
				if ( event.mouseWheelScroll.wheel == sf::Mouse::Wheel::VerticalWheel )
				{
					// SCALING
					if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Key::LControl ) )
					{
						ludii::int32 previousScale = _currentScaleFactor;
						_currentScaleFactor += event.mouseWheelScroll.delta;
						if ( _currentScaleFactor < 1 )
						{
							_currentScaleFactor = 1;
						}
						if ( previousScale != _currentScaleFactor )
						{
							std::cout << "New picker scale: " << _currentScaleFactor << "\n";
							createGlyphImages( _currentScaleFactor, _foregroundColor, _backgroundColor );
						}
					}
					// SCROLLING
					else
					{
						sf::Vector2u effectiveImageDimensions = _selectorBackgroundImage.getSize() * static_cast<sf::Uint32>( _currentScaleFactor );
						// Scrolling only makes sense if the image is taller than the widget.
						if ( effectiveImageDimensions.y > RenderWindow::getSize().y )
						{
							_currentScrollOffset -= event.mouseWheelScroll.delta;
							if ( _currentScrollOffset < 0 )
							{
								_currentScrollOffset = 0;
							}
						}
					}
				}
			}
		}
	}

	
	if( _glyphSheet != nullptr )
	{
		sf::Texture t;
		if ( t.loadFromImage( _selectorBackgroundImage ) )
		{
			sf::RectangleShape rs( static_cast<sf::Vector2f>( _selectorBackgroundImage.getSize() ) );
			rs.setTexture( &t );
			rs.setScale( _currentScaleFactor, _currentScaleFactor );
			rs.setPosition( 0.0f, -( _glyphSheet->getGlyphHeight() * _currentScaleFactor ) * _currentScrollOffset );
			RenderWindow::draw( rs );
		}

		if( QApplication::activeModalWidget() == nullptr )
		{
			if ( isCursorOverCanvas() )
			{
				if ( _glyphSheet->getGlyphCount() == 0 )
				{
					return;
				}

				sf::Vector2i glyphDimensions( _glyphSheet->getGlyphWidth() * _currentScaleFactor, _glyphSheet->getGlyphHeight() * _currentScaleFactor );
				ludii::int32 glyphsPerRow = std::trunc( size().width() / static_cast<float>( glyphDimensions.x ) );

				sf::Vector2f mousePosition( sf::Mouse::getPosition( *this ) );
				sf::Vector2u windowSize = getSize();

				mousePosition.x /= glyphDimensions.x;
				mousePosition.x = std::floor( mousePosition.x );
				mousePosition.y /= glyphDimensions.y;
				mousePosition.y = std::floor( mousePosition.y );

				if ( mousePosition.x > glyphsPerRow - 1 )
				{
					mousePosition.x = glyphsPerRow - 1;
				}

				ludii::uInt16 index = ( mousePosition.y + _currentScrollOffset ) * glyphsPerRow + mousePosition.x;
				if ( index > _glyphSheet->getGlyphCount() - 1 )
				{
					index = _glyphSheet->getGlyphCount() - 1;
				}

				// Derive the positions again from the index.
				// We do this, because it's easier to lock the index by the glyphCount.
				ludii::uInt32 glyphX = index % glyphsPerRow;
				ludii::uInt32 glyphY = index / glyphsPerRow - _currentScrollOffset;

				sf::RectangleShape cursorRect( sf::Vector2f( glyphDimensions.x, glyphDimensions.y ) );
				cursorRect.setFillColor( sf::Color::Transparent );
				cursorRect.setOutlineColor( sf::Color( 255, 255, 255, 127 ) );
				cursorRect.setOutlineThickness( 1.0f );
				cursorRect.setPosition( glyphX * glyphDimensions.x, glyphY * glyphDimensions.y );
				RenderWindow::draw( cursorRect );

				std::stringstream ss;
				ss << index;

				sf::Font font;
				if ( font.loadFromFile( "Resources/Fonts/OpenSans-Regular.ttf" ) )
				{
					sf::Text text( ss.str(), font, 12 );
					text.setColor( sf::Color( 0, 255, 0 ) );
					text.setOutlineColor( sf::Color( 0, 0, 0 ) );
					text.setOutlineThickness( 1.0f );
					text.setPosition( 5, 5 );
					RenderWindow::draw( text );
				}

				// Only register mouse down.
				bool isLeftMouseButtonDown = sf::Mouse::isButtonPressed( sf::Mouse::Button::Left );
				if ( isLeftMouseButtonDown && _wasLeftMouseButtonDownPrevious == false )
				{
					emit onGlyphSelect( index );
				}
				_wasLeftMouseButtonDownPrevious = isLeftMouseButtonDown;
			}
		}
	}
}

bool ludii::texture_editor::GlyphSelector::isCursorOverCanvas() const
{
	sf::Vector2f mousePosition( sf::Mouse::getPosition( *this ) );
	sf::Vector2u windowSize = getSize();

	mousePosition.x /= windowSize.x;
	mousePosition.y /= windowSize.y;

	return mousePosition.x >= 0 && mousePosition.x <= 1 &&
		   mousePosition.y >= 0 && mousePosition.y <= 1;
}

void ludii::texture_editor::GlyphSelector::setGlyphSheet( ludii::GlyphSheet* glyphSheet )
{
	_glyphSheet = glyphSheet;
	createGlyphImages( _currentScaleFactor, ludii::Color( 255, 255, 255 ), ludii::Color( 0, 0, 0 ) );
}

void ludii::texture_editor::GlyphSelector::createGlyphImages( ludii::uInt32 scale, const ludii::Color& foregroundColor, const ludii::Color& backgroundColor )
{
	if( _glyphSheet == nullptr )
	{
		return;
	}

	ludii::int32 widgetWidth = QWidget::size().width();
	if ( widgetWidth <= 0 )
	{
		return;
	}

	sf::Vector2u dimensionsPerGlyph( _glyphSheet->getGlyphWidth() * scale, _glyphSheet->getGlyphHeight() * scale );
	if( dimensionsPerGlyph.x > widgetWidth )
	{
		scale = static_cast<ludii::int32>( std::trunc( widgetWidth / _glyphSheet->getGlyphWidth() ) );
		_currentScaleFactor = scale;
		dimensionsPerGlyph = sf::Vector2u( _glyphSheet->getGlyphWidth() * scale, _glyphSheet->getGlyphHeight() * scale );
	}

	ludii::uInt32 glyphsPerRow = static_cast<ludii::uInt32>( std::trunc( static_cast<float>( widgetWidth ) / dimensionsPerGlyph.x ) );
	
	// The amount of glyphs that have to be appended at the end because they don't make a full row.
	ludii::uInt32 trailingGlyphs = _glyphSheet->getGlyphCount() % glyphsPerRow;
	ludii::uInt32 rows = std::trunc( _glyphSheet->getGlyphCount() / static_cast<float>( glyphsPerRow ) );
	if( trailingGlyphs != 0 )
	{
		rows++;
	}

	ludii::Texture glyphsImageTexture( glyphsPerRow, rows );
	glyphsImageTexture.setAllCells( ludii::TextureCell( 0, foregroundColor, backgroundColor ) );
	for( ludii::uInt32 i = 0; i < _glyphSheet->getGlyphCount(); i++ )
	{
		ludii::uInt32 x = i % glyphsPerRow;
		ludii::uInt32 y = i / glyphsPerRow;

		ludii::TextureCell cell(i, foregroundColor, backgroundColor);
		glyphsImageTexture.setCellAt( x, y, cell );
	}

	_selectorBackgroundImage = glyphsImageTexture.renderToImage( *_glyphSheet );
}

void ludii::texture_editor::GlyphSelector::createGlyphImages( ludii::uInt32 scale, const QColor& foregroundColor, const QColor& backgroundColor )
{
	createGlyphImages( _currentScaleFactor, ludii::Color( foregroundColor.red(), foregroundColor.green(), foregroundColor.blue(), foregroundColor.alpha() ), ludii::Color( backgroundColor.red(), backgroundColor.green(), backgroundColor.blue(), backgroundColor.alpha() ) );
}

void ludii::texture_editor::GlyphSelector::onBrushInfoChange( ludii::texture_editor::BrushInfo brushInfo )
{
	_foregroundColor = brushInfo.foregroundColor;
	_backgroundColor = brushInfo.backgroundColor;
	createGlyphImages( _currentScaleFactor, brushInfo.foregroundColor, brushInfo.backgroundColor );
}

void ludii::texture_editor::GlyphSelector::resizeEvent( QResizeEvent* event )
{
	QSize widgetSize = event->size();
	sf::Vector2f size( widgetSize.width(), widgetSize.height() );

	// Make sure the RenderWindow always fits the widget.
	setSize( static_cast<sf::Vector2u>( size ) );
	setView( sf::View( sf::FloatRect( 0, 0, size.x, size.y ) ) );
}
