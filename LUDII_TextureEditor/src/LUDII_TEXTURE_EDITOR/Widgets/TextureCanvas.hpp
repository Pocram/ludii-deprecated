#ifndef LUDII_TEXTURE_EDITOR_WIDGETS_TEXTURE_CANVAS_HPP
#define LUDII_TEXTURE_EDITOR_WIDGETS_TEXTURE_CANVAS_HPP

#include <LUDII_Texture_Editor/Widgets/QSfmlCanvas.hpp>
#include <LUDII_Texture_Editor/Dialogs/SetSizeDialog.hpp>

#include <LUDII/Graphics/Texture.hpp>
#include <LUDII_TEXTURE_EDITOR/BrushInfo.hpp>
#include <LUDII_TEXTURE_EDITOR/Tools/Tool.hpp>
#include <SFML/Graphics.hpp>


namespace ludii
{
	class GlyphSheet;
}

namespace ludii
{
	namespace texture_editor
	{
		class TextureCanvas : public QSfmlCanvas
		{
		public:
			TextureCanvas( QWidget* parent );
			TextureCanvas( QWidget* parent, const QPoint& position, const QSize& size );

			void onInit() override;
			void onUpdate() override;

			bool isCursorOverCanvas() const;

			ludii::Vector2u getEffectiveCellCountInWindow() const;

			const ludii::Texture& getTexture() const;

			void setTool( ludii::texture_editor::Tool* tool );


		public slots:
			const sf::Vector2u getGlyphDimensions() const;
			void setTexture( const ludii::Texture& texture );
			void setGlyphSheet( ludii::GlyphSheet* glyphSheet );
			void onBrushInfoChange( ludii::texture_editor::BrushInfo brushInfo );

			void setCanvasSize( SetSizeDialogOnSizeSetEventArgs args );

			void recenterTexture();


		protected:
			void resizeEvent( QResizeEvent* event ) override;


		private:
			sf::Image createBackgroundImage( unsigned int cellWidth, unsigned int cellHeight, const sf::Color& colorA, const sf::Color& colorB ) const;
			void resetBackgroundImage();

			void setDisplayGlyphDimensions( const sf::Vector2u& dimensions );

			void handlePanInput();
			void handlePanning();

			void drawTexture();
			void drawCursorAtPosition( ludii::uInt32 x, ludii::uInt32 y );

			sf::Vector2f getCursorCellPosition() const;

			bool _isPanning;
			sf::Vector2i _panStartingPosition;
			sf::Vector2i _panStartingTextureCellPosition;
			bool _middleMouseDownPrevious;

			sf::Vector2u _baseGlyphDimensions;
			sf::Vector2u _displayGlyphDimensions;

			sf::Image _backgroundImage;
			sf::Texture _backgroundTexture;
			sf::RectangleShape _backgroundRect;

			ludii::Texture _texture;
			sf::Image _textureImage;
			ludii::GlyphSheet* _glyphSheet;

			sf::Vector2i _textureCellPosition;
			ludii::int32 _currentCanvasScale;

			bool								_wasToolButtonDownPrevious;
			ludii::texture_editor::BrushInfo	_brushInfo;
			ludii::texture_editor::Tool*		_currentTool;
		};
	}
}

#endif // LUDII_TEXTURE_EDITOR_WIDGETS_TEXTURE_CANVAS_HPP