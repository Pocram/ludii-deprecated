﻿#ifndef LUDII_TEXTURE_EDITOR_WIDGETS_GLYPH_SELECTOR_HPP
#define LUDII_TEXTURE_EDITOR_WIDGETS_GLYPH_SELECTOR_HPP

#include <LUDII_Texture_Editor/Widgets/QSfmlCanvas.hpp>
#include <LUDII_TEXTURE_EDITOR/BrushInfo.hpp>
#include <LUDII/IntDefines.hpp>

namespace ludii
{
	class Color;
	class GlyphSheet;
}

namespace ludii
{
	namespace texture_editor
	{
		class GlyphSelector : public QSfmlCanvas
		{
			Q_OBJECT

		public:
			GlyphSelector( QWidget* parent );
			GlyphSelector( QWidget* parent, const QPoint& position, const QSize& size, unsigned frameTime );

			void onInit() override;
			void onUpdate() override;

			bool isCursorOverCanvas() const;


		signals:
			void onGlyphSelect( ludii::uInt16 glyphId );


		public slots:
			void setGlyphSheet( ludii::GlyphSheet* glyphSheet );
			void createGlyphImages( ludii::uInt32 scale, const ludii::Color& foregroundColor, const ludii::Color& backgroundColor );
			void createGlyphImages( ludii::uInt32 scale, const QColor& foregroundColor, const QColor& backgroundColor );
			void onBrushInfoChange( ludii::texture_editor::BrushInfo brushInfo );


		protected:
			void resizeEvent( QResizeEvent* event ) override;


		private:
			ludii::GlyphSheet*	_glyphSheet;
			sf::Image			_selectorBackgroundImage;
			ludii::int32		_currentScaleFactor;
			ludii::int32		_currentScrollOffset;

			bool _wasLeftMouseButtonDownPrevious;

			QColor _foregroundColor;
			QColor _backgroundColor;
		};
	}
}

#endif // LUDII_TEXTURE_EDITOR_WIDGETS_GLYPH_SELECTOR_HPP