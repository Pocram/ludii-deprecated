#include <LUDII_TEXTURE_EDITOR/Widgets/QSfmlCanvas.hpp>

#ifdef Q_WS_X11
#include <Qt/qx11info_x11.h>
#include <X11/Xlib.h>
#endif

ludii::texture_editor::QSfmlCanvas::QSfmlCanvas( QWidget* parent, const QPoint& position, const QSize& size, unsigned frameTime ) :
	QWidget( parent ),
	myInitialized( false )
{
	setAttribute( Qt::WA_PaintOnScreen );
	setAttribute( Qt::WA_OpaquePaintEvent );
	setAttribute( Qt::WA_NoSystemBackground );

	setFocusPolicy( Qt::StrongFocus );

	move( position );
	resize( size );

	myTimer.setInterval( frameTime );
}

ludii::texture_editor::QSfmlCanvas::~QSfmlCanvas()
{
}

void ludii::texture_editor::QSfmlCanvas::onInit()
{
}

void ludii::texture_editor::QSfmlCanvas::onUpdate()
{
}

QPaintEngine* ludii::texture_editor::QSfmlCanvas::paintEngine() const
{
	return nullptr;
}

void ludii::texture_editor::QSfmlCanvas::showEvent( QShowEvent* )
{
	if ( myInitialized == false )
	{
		#ifdef Q_WS_X11
		XFlush( QX11Info::display() );
		#endif

		RenderWindow::create( reinterpret_cast<sf::WindowHandle>( winId() ) );

		onInit();

		connect( &myTimer, SIGNAL( timeout() ), this, SLOT( repaint() ) );
		myTimer.start();

		myInitialized = true;
	}
}

void ludii::texture_editor::QSfmlCanvas::paintEvent( QPaintEvent* )
{
	onUpdate();
	RenderWindow::display();
}
