﻿#ifndef LUDII_TEXTURE_EDITOR_Q_SFML_CANVAS_HPP
#define LUDII_TEXTURE_EDITOR_Q_SFML_CANVAS_HPP

#include <QWidget>
#include <QTimer>
#include <SFML/Graphics.hpp>

namespace ludii
{
	namespace texture_editor
	{		
		class QSfmlCanvas : public QWidget, public sf::RenderWindow
		{
		public:
			QSfmlCanvas( QWidget* parent, const QPoint& position, const QSize& size, unsigned int frameTime = 0 );
			virtual ~QSfmlCanvas();

		private:
			virtual void onInit();
			virtual void onUpdate();
			QPaintEngine* paintEngine() const override;
			void showEvent( QShowEvent* ) override;
			void paintEvent( QPaintEvent* ) override;

			QTimer myTimer;
			bool myInitialized;
		};
	}
}

#endif // LUDII_TEXTURE_EDITOR_Q_SFML_CANVAS_HPP