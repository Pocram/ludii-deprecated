#include "TextureCanvas.hpp"

#include <LUDII/Graphics/GlyphSheet.hpp>

#include <QResizeEvent>
#include <QApplication>

#include <iostream>
#include <sstream>
#include <cmath>
#include <PencilTool.hpp>

ludii::texture_editor::TextureCanvas::TextureCanvas( QWidget* parent ) :
	QSfmlCanvas( parent, QPoint( 0, 0 ), QSize( 256, 256 ) ),
	_texture( 8, 8 ),
	_glyphSheet( nullptr ),
	_currentCanvasScale( 1 ),
	_isPanning( false ),
	_middleMouseDownPrevious( false ),
	_currentTool( new ludii::texture_editor::PencilTool() ),
	_wasToolButtonDownPrevious( false )
{
	_brushInfo.glyphIndex = 0;
	_brushInfo.foregroundColor = QColor( 255, 255, 255 );
	_brushInfo.backgroundColor = QColor( 0, 0, 0 );
}

ludii::texture_editor::TextureCanvas::TextureCanvas( QWidget* parent, const QPoint& position, const QSize& size ) :
	QSfmlCanvas( parent, position, size ),
	_texture( 8, 8 ),
	_glyphSheet( nullptr ),
	_currentCanvasScale( 1 ),
	_isPanning( false ),
	_middleMouseDownPrevious( false ),
	_currentTool( new ludii::texture_editor::PencilTool() ),
	_wasToolButtonDownPrevious( false )
{
	_brushInfo.glyphIndex = 0;
	_brushInfo.foregroundColor = QColor( 255, 255, 255 );
	_brushInfo.backgroundColor = QColor( 0, 0, 0 );
}

void ludii::texture_editor::TextureCanvas::onInit()
{
	setDisplayGlyphDimensions( sf::Vector2u( 8, 8 ) );
	resetBackgroundImage();
}

void ludii::texture_editor::TextureCanvas::onUpdate()
{
	sf::Event event;
	while( pollEvent( event ) )
	{
		if( QApplication::activeModalWidget() == nullptr )
		{
			if ( event.type == sf::Event::MouseWheelScrolled )
			{
				if ( event.mouseWheelScroll.wheel == sf::Mouse::Wheel::VerticalWheel )
				{
					ludii::int32 previousScale = _currentCanvasScale;
					_currentCanvasScale += event.mouseWheelScroll.delta;
					if ( _currentCanvasScale < 1 )
					{
						_currentCanvasScale = 1;
					}
					if ( previousScale != _currentCanvasScale )
					{
						_displayGlyphDimensions = _baseGlyphDimensions * static_cast<unsigned int>( _currentCanvasScale );
						std::cout << "(" << _displayGlyphDimensions.x << ", " << _displayGlyphDimensions.y << ")\n";
						resetBackgroundImage();

						std::cout << "New canvas scale: " << _currentCanvasScale << "\n";
					}
				}
			}
		}
	}

	handlePanInput();
	handlePanning();

	RenderWindow::clear( sf::Color( 0, 0, 0 ) );
	RenderWindow::draw( _backgroundRect );

	drawTexture();

	if( QApplication::activeModalWidget() != nullptr )
	{
		return;
	}

	sf::Vector2f mousePosition = getCursorCellPosition();
	sf::Vector2f mousePositionInTexture = mousePosition;
	mousePositionInTexture.x -= _textureCellPosition.x;
	mousePositionInTexture.y -= _textureCellPosition.y;

	drawCursorAtPosition( mousePosition.x, mousePosition.y );

	if( _currentTool != nullptr )
	{
		if ( _texture.getDimensions().x > 0 && _texture.getDimensions().y > 0 &&
			mousePositionInTexture.x >= 0 && mousePositionInTexture.y >= 0 &&
			mousePositionInTexture.x < _texture.getDimensions().x && mousePositionInTexture.y < _texture.getDimensions().y )
		{
			bool isToolButtonDown = sf::Mouse::isButtonPressed( sf::Mouse::Button::Left );

			ludii::TextureCell& cell = ludii::TextureCell();
			_texture.getCellAt( mousePositionInTexture.x, mousePositionInTexture.y, cell );
			bool hasTextureBeenChanged = false;
			
			if( _wasToolButtonDownPrevious == false && isToolButtonDown )
				hasTextureBeenChanged = _currentTool->onMouseDown( ludii::Vector2i( mousePositionInTexture.x, mousePositionInTexture.y ), _texture, _brushInfo, &_textureImage, _glyphSheet );
			else if( _wasToolButtonDownPrevious && isToolButtonDown )
				hasTextureBeenChanged = _currentTool->onMouseHold( ludii::Vector2i( mousePositionInTexture.x, mousePositionInTexture.y ), _texture, _brushInfo, &_textureImage, _glyphSheet );
			else if( _wasToolButtonDownPrevious && isToolButtonDown == false )
				hasTextureBeenChanged = _currentTool->onMouseRelease( ludii::Vector2i( mousePositionInTexture.x, mousePositionInTexture.y ), _texture, _brushInfo, &_textureImage, _glyphSheet );

			if ( hasTextureBeenChanged && _glyphSheet != nullptr )
			{
				_textureImage = _texture.renderToImage( *_glyphSheet );
			}
			
			_wasToolButtonDownPrevious = isToolButtonDown;
		}
		
	}

	sf::Font font;
	if ( font.loadFromFile( "Resources/Fonts/OpenSans-Regular.ttf" ) )
	{
		std::stringstream ss;
		ss << "(" << mousePositionInTexture.x << ", " << mousePositionInTexture.y << ")";

		sf::Text text( ss.str(), font, 12 );
		text.setColor( sf::Color( 0, 255, 0 ) );
		text.setOutlineColor( sf::Color( 0, 0, 0 ) );
		text.setOutlineThickness( 1.0f );
		text.setPosition( 15, 15 );
		RenderWindow::draw( text );
	}
}

bool ludii::texture_editor::TextureCanvas::isCursorOverCanvas() const
{
	sf::Vector2f mousePosition( sf::Mouse::getPosition( *this ) );
	sf::Vector2u windowSize = getSize();

	mousePosition.x /= windowSize.x;
	mousePosition.y /= windowSize.y;

	return mousePosition.x >= 0 && mousePosition.x <= 1 &&
		   mousePosition.y >= 0 && mousePosition.y <= 1;
}

ludii::Vector2u ludii::texture_editor::TextureCanvas::getEffectiveCellCountInWindow() const
{
	if( _glyphSheet == nullptr )
	{
		return ludii::Vector2u( 0, 0 );
	}

	QSize widgetSize = QWidget::size();
	if( widgetSize.width() <= 0 || widgetSize.height() <= 0 )
	{
		return ludii::Vector2u( 0, 0 );
	}

	ludii::Vector2u glyphDimensions = ludii::Vector2u( _displayGlyphDimensions.x, _displayGlyphDimensions.y );

	float glyphsPerRow = widgetSize.width() / glyphDimensions.x;
	float glyphsPerColumn = widgetSize.height() / glyphDimensions.y;
	return ludii::Vector2u( std::floor( glyphsPerRow ), std::floor( glyphsPerColumn ) );
}

const ludii::Texture& ludii::texture_editor::TextureCanvas::getTexture() const
{
	return _texture;
}

void ludii::texture_editor::TextureCanvas::setTool( ludii::texture_editor::Tool* tool )
{
	if( _currentTool != nullptr )
	{
		_currentTool->onToolDeselect();
		delete _currentTool;
	}

	_currentTool = tool;
	_currentTool->onToolSelect();
}

void ludii::texture_editor::TextureCanvas::setDisplayGlyphDimensions( const sf::Vector2u& dimensions )
{
	if( _glyphSheet == nullptr )
	{
		_baseGlyphDimensions = dimensions;
	}

	_displayGlyphDimensions = dimensions;
	resetBackgroundImage();
}

void ludii::texture_editor::TextureCanvas::recenterTexture()
{
	ludii::Vector2u widgetCellCount = getEffectiveCellCountInWindow();
	ludii::Vector2u textureDimensions = _texture.getDimensions();
	ludii::int32 newXPosition = ( std::round( widgetCellCount.x / 2.0f ) - 1 ) - ( std::round( textureDimensions.x / 2.0f ) - 1 );
	ludii::int32 newYPosition = ( std::round( widgetCellCount.y / 2.0f ) - 1 ) - ( std::round( textureDimensions.y / 2.0f ) - 1 );
	_textureCellPosition = sf::Vector2i( newXPosition, newYPosition );
}

void ludii::texture_editor::TextureCanvas::handlePanInput()
{
	bool isMiddleMouseButtonDown = sf::Mouse::isButtonPressed( sf::Mouse::Button::Middle );
	if( isMiddleMouseButtonDown )
	{
		// You may only start panning when you are on the canvas.
		if( _middleMouseDownPrevious == false &&_isPanning == false && isCursorOverCanvas() )
		{
			_panStartingPosition = sf::Mouse::getPosition( *this );
			_panStartingTextureCellPosition = _textureCellPosition;
			_isPanning = true;
		}
	}
	else
	{
		_isPanning = false;
	}

	_middleMouseDownPrevious = isMiddleMouseButtonDown;
}

void ludii::texture_editor::TextureCanvas::handlePanning()
{
	if( _isPanning == false )
	{
		return;
	}

	sf::Vector2i mousePosition = sf::Mouse::getPosition( *this );
	sf::Vector2i panDelta = mousePosition - _panStartingPosition;
	sf::Vector2i cellPanDelta( panDelta.x / static_cast<int>( _displayGlyphDimensions.x ), panDelta.y / static_cast<int>( _displayGlyphDimensions.y ) );
	_textureCellPosition = _panStartingTextureCellPosition + cellPanDelta;
}

void ludii::texture_editor::TextureCanvas::drawTexture()
{
	if ( _glyphSheet != nullptr )
	{
		sf::Texture texture;
		if ( texture.loadFromImage( _textureImage ) )
		{
			float averageGlyphDimensions = ( getGlyphDimensions().x + getGlyphDimensions().y ) / 2.0f;

			sf::RectangleShape rect( sf::Vector2f( _textureImage.getSize().x, _textureImage.getSize().y ) );
			rect.setPosition( sf::Vector2f( static_cast<float>( _displayGlyphDimensions.x ) * _textureCellPosition.x, static_cast<float>( _displayGlyphDimensions.y ) * _textureCellPosition.y ) );
			rect.setTexture( &texture );
			rect.setScale( sf::Vector2f( 1, 1 ) * static_cast<float>( _currentCanvasScale ) );
			rect.setOutlineColor( sf::Color( 0, 0, 0, 65 ) );
			rect.setOutlineThickness( averageGlyphDimensions / 2.0f );

			RenderWindow::draw( rect );
		}
	}
}

void ludii::texture_editor::TextureCanvas::drawCursorAtPosition( ludii::uInt32 x, ludii::uInt32 y )
{
	if ( isCursorOverCanvas() )
	{
		sf::RectangleShape cursorRect( sf::Vector2f( _displayGlyphDimensions.x, _displayGlyphDimensions.y ) );
		cursorRect.setFillColor( sf::Color::Transparent );
		cursorRect.setOutlineColor( sf::Color( 255, 255, 255, 127 ) );
		cursorRect.setOutlineThickness( 1.0f );
		cursorRect.setPosition( x * _displayGlyphDimensions.x, y * _displayGlyphDimensions.y );

		sf::Texture tex;
		if ( _glyphSheet != nullptr )
		{
			ludii::Color f( _brushInfo.foregroundColor.red(), _brushInfo.foregroundColor.green(), _brushInfo.foregroundColor.blue() );
			ludii::Color b( _brushInfo.backgroundColor.red(), _brushInfo.backgroundColor.green(), _brushInfo.backgroundColor.blue() );
			ludii::TextureCell c( _brushInfo.glyphIndex, f, b );
			ludii::Texture t( 1, 1 );
			t.setCellAt( 0, 0, c );
			sf::Image i = t.renderToImage( *_glyphSheet );
			if ( tex.loadFromImage( i ) )
			{
				cursorRect.setFillColor( sf::Color::White );
				cursorRect.setTexture( &tex );
			}
		}

		RenderWindow::draw( cursorRect );
	}
}

sf::Vector2f ludii::texture_editor::TextureCanvas::getCursorCellPosition() const
{
	sf::Vector2f mousePosition( sf::Mouse::getPosition( *this ) );
	mousePosition.x /= _displayGlyphDimensions.x;
	mousePosition.x = std::floor( mousePosition.x );
	mousePosition.y /= _displayGlyphDimensions.y;
	mousePosition.y = std::floor( mousePosition.y );

	return mousePosition;
}

const sf::Vector2u ludii::texture_editor::TextureCanvas::getGlyphDimensions() const
{
	if( _glyphSheet == nullptr )
	{
		return sf::Vector2u( 0, 0 );
	}
	return sf::Vector2u( _glyphSheet->getGlyphWidth(), _glyphSheet->getGlyphHeight() );
}

void ludii::texture_editor::TextureCanvas::setTexture( const ludii::Texture& texture )
{
	std::cout << "TEXTURE MOUNTED AND LOADED\n";
	_texture = texture;

	if( _glyphSheet != nullptr )
	{
		_textureImage = _texture.renderToImage( *_glyphSheet );
	}

	_textureCellPosition = sf::Vector2i( 0, 0 );
}

void ludii::texture_editor::TextureCanvas::setGlyphSheet( ludii::GlyphSheet* glyphSheet )
{
	std::cout << "GLYPH SHEET MOUNTED AND LOADED\n";
	_glyphSheet = glyphSheet;
	setDisplayGlyphDimensions( sf::Vector2u( _glyphSheet->getGlyphWidth(), _glyphSheet->getGlyphHeight() ) );
	_textureImage = _texture.renderToImage( *_glyphSheet );
}

void ludii::texture_editor::TextureCanvas::onBrushInfoChange( ludii::texture_editor::BrushInfo brushInfo )
{
	_brushInfo = brushInfo;
}

void ludii::texture_editor::TextureCanvas::setCanvasSize( SetSizeDialogOnSizeSetEventArgs args )
{
	ludii::Texture newTexture( args.width, args.height );

	// Calculate centered positions up here for the sake of readability. Otherwise we'd have to use scopes in the case statements which make the code an ugly clusterfuck of braces.
	ludii::uInt32 centeredXPosition = ( std::round( newTexture.getDimensions().x / 2.0f ) - 1 ) - ( std::round( _texture.getDimensions().x / 2.0f ) - 1 );
	ludii::uInt32 centeredYPosition = ( std::round( newTexture.getDimensions().y / 2.0f ) - 1 ) - ( std::round( _texture.getDimensions().y / 2.0f ) - 1 );

	ludii::Vector2u textureOriginInNewTexture( 0, 0 );
	switch ( args.anchor )
	{
		case ludii::texture_editor::SizeSetAnchor::TopLeft: 
			textureOriginInNewTexture = ludii::Vector2u( 0, 0 );
			break;
		case ludii::texture_editor::SizeSetAnchor::TopMiddle:
			textureOriginInNewTexture = ludii::Vector2u( centeredXPosition, 0 );
			break;
		case ludii::texture_editor::SizeSetAnchor::TopRight:
			textureOriginInNewTexture = ludii::Vector2u( newTexture.getDimensions().x - _texture.getDimensions().x, 0 );
			break;
		case ludii::texture_editor::SizeSetAnchor::MiddleLeft:
			textureOriginInNewTexture = ludii::Vector2u( 0, centeredYPosition );
			break;
		case ludii::texture_editor::SizeSetAnchor::MiddleRight:
			textureOriginInNewTexture = ludii::Vector2u( newTexture.getDimensions().x - _texture.getDimensions().x, centeredYPosition );
			break;
		case ludii::texture_editor::SizeSetAnchor::BottomLeft:
			textureOriginInNewTexture = ludii::Vector2u( 0, newTexture.getDimensions().y - _texture.getDimensions().y);
			break;
		case ludii::texture_editor::SizeSetAnchor::BottomMiddle:
			textureOriginInNewTexture = ludii::Vector2u( centeredXPosition, newTexture.getDimensions().y - _texture.getDimensions().y);
			break;
		case ludii::texture_editor::SizeSetAnchor::BottomRight:
			textureOriginInNewTexture = ludii::Vector2u( newTexture.getDimensions().x - _texture.getDimensions().x, newTexture.getDimensions().y - _texture.getDimensions().y );
			break;
		default: 
			// Default or center.
			textureOriginInNewTexture = ludii::Vector2u( centeredXPosition, centeredYPosition );
			break;
	}

	// Copy old texture into new texture.
	for ( ludii::uInt32 x = 0; x < _texture.getDimensions().x; x++ )
	{
		for ( ludii::uInt32 y = 0; y < _texture.getDimensions().y; y++ )
		{
			// Check for bounds.
			ludii::int32 newX = textureOriginInNewTexture.x + x;
			ludii::int32 newY = textureOriginInNewTexture.y + y;
			if ( newX < 0 || newX >= newTexture.getDimensions().x || 
				 newY < 0 || newY >= newTexture.getDimensions().y )
			{
				continue;
			}

			ludii::TextureCell c;
			_texture.getCellAt( x, y, c );
			newTexture.setCellAt( newX, newY, c );
		}
	}

	setTexture( newTexture );
}

void ludii::texture_editor::TextureCanvas::resizeEvent( QResizeEvent* event )
{
	QSize widgetSize = event->size();
	sf::Vector2f size( widgetSize.width(), widgetSize.height() );
	
	// Make sure the RenderWindow always fits the widget.
	setSize( static_cast<sf::Vector2u>(size) );
	setView( sf::View( sf::FloatRect( 0, 0, size.x, size.y ) ) );

	// Make sure the background always fills the screen.
	_backgroundRect.setPosition( 0, 0 );
	_backgroundRect.setSize( size );
	_backgroundRect.setTextureRect( sf::IntRect( 0, 0, size.x, size.y ) );
}

sf::Image ludii::texture_editor::TextureCanvas::createBackgroundImage( unsigned int cellWidth, unsigned int cellHeight, const sf::Color& colorA, const sf::Color& colorB ) const
{
	sf::Image outImage;
	sf::Vector2u imageDimensions( cellWidth * 2, cellHeight * 2 );
	
	unsigned int pixelsLength = imageDimensions.x * imageDimensions.y * 4;
	sf::Uint8* pixels = new sf::Uint8[pixelsLength];
	memset( pixels, 0, pixelsLength );

	for ( unsigned int x = 0; x < imageDimensions.x; x++ )
	{
		for ( unsigned int y = 0; y < imageDimensions.y; y++ )
		{
			// Screw performance, this method is only called once anyway.
			sf::Color cellColor = x < cellWidth ? colorA : colorB;
			if( y >= cellHeight )
			{
				cellColor = x < cellWidth ? colorB : colorA;
			}

			pixels[( x + y * imageDimensions.x ) * 4 + 0] = cellColor.r;
			pixels[( x + y * imageDimensions.x ) * 4 + 1] = cellColor.g;
			pixels[( x + y * imageDimensions.x ) * 4 + 2] = cellColor.b;
			pixels[( x + y * imageDimensions.x ) * 4 + 3] = cellColor.a;
		}
	}

	outImage.create( imageDimensions.x, imageDimensions.y, pixels );
	delete[] pixels;
	return outImage;
}

void ludii::texture_editor::TextureCanvas::resetBackgroundImage()
{
	_backgroundRect.setSize( static_cast<sf::Vector2f>( getSize() ) );
	_backgroundImage = createBackgroundImage( _displayGlyphDimensions.x, _displayGlyphDimensions.y, sf::Color( 28, 28, 28 ), sf::Color( 25, 25, 25 ) );
	_backgroundTexture.setRepeated( true );
	if ( _backgroundTexture.loadFromImage( _backgroundImage ) )
	{
		_backgroundRect.setTexture( &_backgroundTexture );
		_backgroundRect.setTextureRect( sf::IntRect( 0, 0, _backgroundRect.getSize().x, _backgroundRect.getSize().y ) );
	}
}
