﻿#pragma once

#include <LUDII/Application/GameObjects/GameObject.hpp>

class MyObject : public ludii::GameObject
{
public:
	MyObject();

protected:
	void onUpdate( ludii::TimeDelta elapsedTime ) override;
};
