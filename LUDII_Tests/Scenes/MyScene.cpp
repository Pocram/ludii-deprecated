﻿#include "MyScene.hpp"
#include <iostream>

#include <LUDII/Application/GameObjects/GameObject.hpp>
#include "MyObject.hpp"

MyScene::MyScene()
{
	MyObject* myObj = new MyObject();
	myObj->getTransform().setGlobalCellPosition( ludii::Vector2i( 7, -11 ) );
	instantiateGameObject( myObj );

	std::cout << "Hi!\n";
}
