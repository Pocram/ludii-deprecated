﻿#include "MyTexture.hpp"

MyTexture::MyTexture()
{
	create( 5, 5 );

	setAllCells( ludii::TextureCell( 3, ludii::Color( 63, 63, 63 ), ludii::Color( 0, 0, 0 ) ) );
	setCellAt( 0,	0,	ludii::TextureCell( 76,	ludii::Color( 255, 255, 255 ),	ludii::Color( 63, 63, 127 ) ) );
	setCellAt( 1,	1,	ludii::TextureCell( 85,	ludii::Color( 255, 255, 255 ),	ludii::Color( 63, 63, 127 ) ) );
	setCellAt( 2,	2,	ludii::TextureCell( 68,	ludii::Color( 255, 255, 255 ),	ludii::Color( 63, 63, 127 ) ) );
	setCellAt( 3,	3,	ludii::TextureCell( 73,	ludii::Color( 255, 255, 255 ),	ludii::Color( 63, 63, 127 ) ) );
	setCellAt( 4,	4,	ludii::TextureCell( 73,	ludii::Color( 255, 255, 255 ),	ludii::Color( 63, 63, 127 ) ) );
}
