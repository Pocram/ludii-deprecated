#include <stdio.h>

#include <LUDII/Application/Game.hpp>
#include "Scenes/MyScene.hpp"
#include "Scenes/MyTexture.hpp"
#include <LUDII/FileSystem/FileManager.hpp>

int main(int argc, char* argv[])
{
	ludii::Game& game = ludii::Game::getInstance();
	game.initialize( argc, argv );

	//MyTexture* mt = new MyTexture();
	//mt->saveToFile( ludii::file_system::FileManager::getPathInAssetsFolder( "haha.ludtex" ) );
	
	game.loadScene( new MyScene() );
	
	game.start();

	getchar();
	return 0;
}
