#include <stdio.h>

namespace test 
{
    namespace lol {
    class MyClass
    {
    LUDII_REFLECTABLE

    public:
        MyClass();

        void myTestMethod(int a);

        int myValue; // Hello!
        char* myOtherValue;
        ludii::uInt32 age;
        std::vector<std::string> names;
    };
    }
}

namespace Cats {class Hello {public:Hello();private:char myChar;};}