#ifndef LUDIIEDITORMAINWINDOW_HPP
#define LUDIIEDITORMAINWINDOW_HPP

#include <QtWidgets/QMainWindow>
#include "ui_LudiiEditorMainWindow.h"

#include <LUDII_Editor/Dialogs/MainWindow/MainNewProjectDialog.hpp>
#include <LUDII_Editor/Dialogs/MainWindow/MainOpenProjectDialog.hpp>

#include <LUDII_Editor/Project.hpp>

namespace ludii
{
	namespace editor
	{
		class LudiiEditorMainWindow : public QMainWindow
		{
			Q_OBJECT

		public:
			LudiiEditorMainWindow( QWidget *parent = 0 );
			~LudiiEditorMainWindow();

			ludii::editor::Project& getCurrentProject();
			const ludii::editor::Project& getCurrentProject() const;


		public slots:
			void setProject( ludii::editor::Project project );
			void onProjectHasBeenCreated( QString pathToProject );


		private:
			void unloadCurrentProject();

			void initializeMenuBarTriggers();

			void onMenuClick_newProject();
			void onMenuClick_openProject();

			Ui::LudiiEditorMainWindowClass ui;

			// Menu bar trigger map
			typedef void( ludii::editor::LudiiEditorMainWindow::*menuTriggerActionFunction )( void );
			std::map<QString, menuTriggerActionFunction> _menuTriggerActions;
			
			// Dialogs
			ludii::editor::MainNewProjectDialog*	_newProjectDialog;
			ludii::editor::MainOpenProjectDialog*	_openProjectDialog;

			// Project
			ludii::editor::Project* _currentProject;


		private slots:
			void onMenuBarTriggered( QAction* action );
		};
	}
}

#endif // LUDIIEDITORMAINWINDOW_HPP
