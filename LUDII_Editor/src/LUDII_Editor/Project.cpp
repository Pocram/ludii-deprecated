#include "Project.hpp"

#include <QFileInfo>
#include <QStringList>
#include <QDir>

#include <boost/filesystem.hpp>

#include <LUDII/ThirdParty/SimpleIni/SimpleIni.h>
#include <LUDII/Application/Ini/GraphicsIniFile.hpp>

#include <LUDII/ThirdParty/RapidXml/rapidxml.hpp>
#include <LUDII/ThirdParty/RapidXml/rapidxml_utils.hpp>
#include <LUDII/ThirdParty/RapidXml/rapidxml_print.hpp>
#include <LUDII/ThirdParty/RapidXml/RapidXmlUtility.hpp>

#include <LUDII/Utility/String.hpp>

#include <iostream>
#include <fstream>
#include <sstream>

ludii::editor::Project::Project()
{
	_rootNodeFunctions.insert_or_assign( "project_name", &ludii::editor::Project::handleRootNode_name );
}

bool ludii::editor::Project::loadFromFile( const std::string& path )
{
	std::cout << "Attempting to load project: \"" << path << "\"\n";

	QFileInfo fileInfo( path.c_str() );
	if( fileInfo.isFile() == false )
	{
		std::cout << "Error: Could not open project file - Path does not lead to a file.\n";
		return false;
	}

	_projectFileName = fileInfo.fileName().toStdString();
	_pathToProjectDirectory = fileInfo.canonicalPath().toStdString();

	rapidxml::xml_document<> document;
	rapidxml::file<> xmlFile( path.c_str() );
	document.parse<0>( xmlFile.data() );

	rapidxml::xml_node<> *rootNode = document.first_node( "ludiiproject" );
	if( rootNode == nullptr )
	{
		std::cout << "Error: Could not open project file - Could not find root node \"ludiiproject\".\n";
		return false;
	}

	for ( rapidxml::xml_node<> *child = rootNode->first_node(); child; child = child->next_sibling() )
	{
		if( child == nullptr )
		{
			continue;
		}

		if ( _rootNodeFunctions.find( child->name() ) != _rootNodeFunctions.end() )
		{
			xmlNodeValueFunction function = _rootNodeFunctions[child->name()];
			( this->*function )( child->value() );
		}
		else
		{
			std::cout << "Node \"" << child->name() << "\" unrecognized!\n";
		}
	}

	std::cout << "Successfully loaded project.\n";
	return true;
}

bool ludii::editor::Project::loadFromDirectory( const std::string& path )
{
	if( doesDirectoryContainProject( path ) == false )
	{
		return false;
	}

	QString qPath( path.c_str() );
	QStringList nameFilter( "*.ludiiproject" );
	QDir directory( qPath );
	QStringList projectFiles = directory.entryList( nameFilter );

	// We know there is at least one entry; see function call above.
	QString fileName = projectFiles[0];
	std::stringstream fullPathStream;
	fullPathStream << directory.absolutePath().toStdString() << "/" << fileName.toStdString();
	
	return loadFromFile( fullPathStream.str() );
}

bool ludii::editor::Project::saveToFile( const std::string& path, bool isNewProject ) const
{
	if ( createBasicFolderStructure( path ) == false )
	{
		return false;
	}

	bool ludiiProjectCreated = createProjectFile( path );
	if( isNewProject == false )
	{
		// No need to set up new environment if it's not new!
		return ludiiProjectCreated;
	}

	return ludiiProjectCreated && createEnvironment( path );
}

std::string ludii::editor::Project::getName() const
{
	return _name;
}

void ludii::editor::Project::setName( const std::string& name )
{
	std::cout << "Setting project name to \"" << name << "\"\n";
	_name = name;
}

bool ludii::editor::Project::doesDirectoryContainProject( const std::string& path )
{
	QString qPath( path.c_str() );
	QStringList nameFilter( "*.ludiiproject" );
	QDir directory( qPath );
	QStringList projectFiles = directory.entryList( nameFilter );
	return projectFiles.size() > 0;
}

bool ludii::editor::Project::createProjectFile( const std::string& path ) const
{
	QString qPath( path.c_str() );
	qPath += "\\" + QString( getName().c_str() ) + ".ludiiproject";

	rapidxml::xml_document<> document;
	rapidxml::xml_node<>* rootNode = document.allocate_node( rapidxml::node_element, "ludiiproject" );
	document.append_node( rootNode );

	char* projectNameNodeName = document.allocate_string( getName().c_str() );
	rapidxml::xml_node<>* projectNameNode = document.allocate_node( rapidxml::node_element, "project_name", projectNameNodeName );
	rootNode->append_node( projectNameNode );

	// Save file.
	std::ofstream filetoSave;
	filetoSave.open( qPath.toStdString() );
	filetoSave << document;
	bool didSave = false;
	if ( filetoSave ) didSave = true;
	filetoSave.close();
	return didSave;
}

bool ludii::editor::Project::createEnvironment( const std::string& path ) const
{
#if defined(LUDII_SYSTEM_WINDOWS)
	return createVisualStudio2015Environment( path );
#else
	return true;
#endif
}

#if defined(LUDII_SYSTEM_WINDOWS)
bool ludii::editor::Project::createVisualStudio2015Environment( const std::string& path ) const
{
	if( createVisualStudio2015ProjectFile( path ) == false )
	{
		return false;
	}

	if( createBasicCodeFiles( path ) == false )
	{
		return false;
	}

	if ( createIniFiles( path ) == false )
	{
		return false;
	}

	if( createDlls( path ) == false )
	{
		return false;
	}

	return true;
}

bool ludii::editor::Project::createVisualStudio2015ProjectFile( const std::string& path ) const
{
	QString executionPath = QDir::currentPath();
	QString targetPath( path.c_str() );
	targetPath += "/" + QString( getName().c_str() ) + ".vcxproj";
	QString ludiiProjectPath = path.c_str() + QString( "/" ) + QString( getName().c_str() ) + ".ludiiproject";
	bool projectWasCopied = QFile::copy( executionPath + "/Resources/ProjectTemplates/vs14/vs14.vcxproj", targetPath );
	if( projectWasCopied == false )
	{
		std::cout << "Error: Could not copy project template from \"" << executionPath.toStdString() << "/Resources/ProjectTemplates/vs14/vs14.vcxproj\" to \"" << targetPath.toStdString() << "\"!\n";
		return false;
	}

	// Load directories from ini file.
	CSimpleIniA configIni;
	configIni.SetUnicode();
	SI_Error result = configIni.LoadFile( ( executionPath + "/Resources/LEConfig.ini" ).toStdString().c_str() );
	if( result < 0 )
	{
		std::cout << "Error: LEConfig.ini could not be found at \"" << ( executionPath + "/Resources/LEConfig.ini" ).toStdString() << "\"!\n";
		return false;
	}

	const char* ludiiApiIncludeDirectory = configIni.GetValue( "libraries", "ludii_include" );
	if( ludiiApiIncludeDirectory == nullptr )
	{
		std::cout << "Error: No include directory was specified for the LUDII API!\n";
		return false;
	}

	const char* ludiiApiLibDirectory = configIni.GetValue( "libraries", "ludii_lib" );
	if ( ludiiApiLibDirectory == nullptr )
	{
		std::cout << "Error: No lib directory was specified for the LUDII API!\n";
		return false;
	}

	const char* sfmlIncludeDirectory = configIni.GetValue( "libraries", "sfml_include" );
	if ( sfmlIncludeDirectory == nullptr )
	{
		std::cout << "Error: No include directory was specified for SFML!\n";
		return false;
	}

	const char* sfmlLibDirectory = configIni.GetValue( "libraries", "sfml_lib" );
	if ( sfmlLibDirectory == nullptr )
	{
		std::cout << "Error: No lib directory was specified for SFML!\n";
		return false;
	}

	const char* boostIncludeDirectory = configIni.GetValue( "libraries", "boost_include" );
	if ( boostIncludeDirectory == nullptr )
	{
		std::cout << "Error: No include directory was specified for boost!\n";
		return false;
	}

	const char* boostLibDirectory = configIni.GetValue( "libraries", "boost_lib" );
	if ( boostLibDirectory == nullptr )
	{
		std::cout << "Error: No lib directory was specified for boost!\n";
		return false;
	}

	// Replace placeholders in project file with directories above.
	std::ofstream tempProjectFile( ( targetPath + ".temp" ).toStdString() );
	if( tempProjectFile.is_open() )
	{
		std::ifstream projectFile( targetPath.toStdString() );
		if ( projectFile.is_open() )
		{
			std::string line;
			while ( std::getline( projectFile, line ) )
			{
				ludii::replaceFirstOccurenceInString( line, "{LUDII_API_DIR}", ludiiApiIncludeDirectory );
				ludii::replaceFirstOccurenceInString( line, "{LUDII_API_LIB}", ludiiApiLibDirectory );

				ludii::replaceFirstOccurenceInString( line, "{SFML_DIR}", sfmlIncludeDirectory );
				ludii::replaceFirstOccurenceInString( line, "{SFML_LIB}", sfmlLibDirectory );

				ludii::replaceFirstOccurenceInString( line, "{BOOST_DIR}", boostIncludeDirectory );
				ludii::replaceFirstOccurenceInString( line, "{BOOST_LIB}", boostLibDirectory );
				
				ludii::replaceFirstOccurenceInString( line, "{LUDII_ENGINE_DIR}", executionPath.toStdString() );
				ludii::replaceFirstOccurenceInString( line, "{PROJECT_FILE}", ludiiProjectPath.toStdString() );

				tempProjectFile << line << '\n';
			}

			projectFile.close();
			tempProjectFile.close();

			QFile qProjectFile( targetPath );
			qProjectFile.remove();
			QFile qTempProjectFile( targetPath + ".temp" );
			qTempProjectFile.rename( targetPath );
		}
		else
		{
			std::cout << "Error: Could not open project file for reading!\n";
			return false;
		}
	}
	else
	{
		std::cout << "Error: Could not create temporary project file!\n";
		return false;
	}

	return projectWasCopied;
}
#endif

bool ludii::editor::Project::createBasicCodeFiles( const std::string& path ) const
{
	QString executionPath = QDir::currentPath();
	QString targetPath = path.c_str() + QString( "/" ) + "src";
	bool mainFileCopied = QFile::copy( executionPath + "/Resources/ProjectTemplates/vs14/Main.cpp", targetPath + "/Main.cpp" );
	return mainFileCopied;
}

bool ludii::editor::Project::createDlls( const std::string& path ) const
{
	QString dllTargetPath = path.c_str();
	dllTargetPath += "/DLL";
	QString executionPath = QDir::currentPath();

	// Load directories from ini file.
	CSimpleIniA configIni;
	configIni.SetUnicode();
	SI_Error result = configIni.LoadFile( ( executionPath + "/Resources/LEConfig.ini" ).toStdString().c_str() );
	if ( result < 0 )
	{
		std::cout << "Error: LEConfig.ini could not be found at \"" << ( executionPath + "/Resources/LEConfig.ini" ).toStdString() << "\"!\n";
		return false;
	}

	const char* ludiiApiDllDirectory = configIni.GetValue( "libraries", "ludii_dll" );
	if ( ludiiApiDllDirectory == nullptr )
	{
		std::cout << "Error: No DLL directory was specified for the LUDII API!\n";
		return false;
	}

	const char* sfmlDllDirectory = configIni.GetValue( "libraries", "sfml_dll" );
	if ( sfmlDllDirectory == nullptr )
	{
		std::cout << "Error: No DLL directory was specified for SFML!\n";
		return false;
	}


	if ( QFile::copy( QString( ludiiApiDllDirectory ) + "/LUDII_API.dll", dllTargetPath + "/LUDII_API.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"LUDII_API.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-system-2.dll", dllTargetPath + "/sfml-system-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-system-2.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-graphics-2.dll", dllTargetPath + "/sfml-graphics-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-graphics-2.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-window-2.dll", dllTargetPath + "/sfml-window-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-window-2.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-audio-2.dll", dllTargetPath + "/sfml-audio-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-audio-2.dll\" to project path.\n";
		return false;
	}


	if ( QFile::copy( QString( ludiiApiDllDirectory ) + "/LUDII_API_d.dll", dllTargetPath + "/LUDII_API_d.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"LUDII_API_d.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-system-d-2.dll", dllTargetPath + "/sfml-system-d-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-system-2-d.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-graphics-d-2.dll", dllTargetPath + "/sfml-graphics-d-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-graphics-2-d.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-window-d-2.dll", dllTargetPath + "/sfml-window-d-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-window-2-d.dll\" to project path.\n";
		return false;
	}

	if ( QFile::copy( QString( sfmlDllDirectory ) + "/sfml-audio-d-2.dll", dllTargetPath + "/sfml-audio-d-2.dll" ) == false )
	{
		std::cout << "Error: Unable to copy \"sfml-audio-2-d.dll\" to project path.\n";
		return false;
	}

	return true;
}

bool ludii::editor::Project::createIniFiles( const std::string& path ) const
{
	ludii::ini::GraphicsIniFile graphicsIni;
	return graphicsIni.initialize( path + "/Build/Win32/Data/LDGraphics.ini" );
}

bool ludii::editor::Project::createBasicFolderStructure( const std::string& path ) const 
{
	if ( boost::filesystem::exists( path ) == false )
	{
		return false;
	}

	std::string sourcePath = path + "/src";
	if ( boost::filesystem::exists( sourcePath ) == false && boost::filesystem::create_directory( sourcePath ) == false )
	{
		return false;
	}

	std::string buildPath = path + "/Build";
	if ( boost::filesystem::exists( buildPath ) == false && boost::filesystem::create_directory( buildPath ) == false )
	{
		return false;
	}

	std::string buildWin32Path = buildPath + "/Win32";
	if ( boost::filesystem::exists( buildWin32Path ) == false && boost::filesystem::create_directory( buildWin32Path ) == false )
	{
		return false;
	}

	std::string buildWin32AssetsPath = buildWin32Path + "/Assets";
	if ( boost::filesystem::exists( buildWin32AssetsPath ) == false && boost::filesystem::create_directory( buildWin32AssetsPath ) == false )
	{
		return false;
	}

	std::string buildWin32ConfigPath = buildWin32Path + "/Data";
	if ( boost::filesystem::exists( buildWin32ConfigPath ) == false && boost::filesystem::create_directory( buildWin32ConfigPath ) == false )
	{
		return false;
	}

	std::string dllPath = path + "/DLL";
	if ( boost::filesystem::exists( dllPath ) == false && boost::filesystem::create_directory( dllPath ) == false )
	{
		return false;
	}

	std::string sourceProjectPath = sourcePath + "/Project";
	if ( boost::filesystem::exists( sourceProjectPath ) == false && boost::filesystem::create_directory( sourceProjectPath ) == false )
	{
		return false;
	}

	std::string sourceGeneratedPath = sourcePath + "/Generated";
	if ( boost::filesystem::exists( sourceGeneratedPath ) == false && boost::filesystem::create_directory( sourceGeneratedPath ) == false )
	{
		return false;
	}

	return true;
}

void ludii::editor::Project::handleRootNode_name( std::string value )
{
	setName( value );
}
