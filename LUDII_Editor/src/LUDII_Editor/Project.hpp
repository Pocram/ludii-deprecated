#ifndef LUDII_EDITOR_PROJECT_HPP
#define LUDII_EDITOR_PROJECT_HPP

#include <LUDII/Config.hpp>

#include <string>
#include <map>

namespace ludii
{
	namespace editor
	{
		class Project
		{
		public:
			Project();

			bool loadFromFile( const std::string& path );
			bool loadFromDirectory( const std::string& path );

			bool saveToFile( const std::string& path, bool isNewProject = false ) const;

			std::string getName() const;
			void setName( const std::string& name );

			static bool doesDirectoryContainProject( const std::string& path );


		private:
			bool createProjectFile( const std::string& path ) const;
			bool createEnvironment( const std::string& path ) const;

			bool createBasicFolderStructure( const std::string& path ) const;
			bool createBasicCodeFiles( const std::string& path ) const;
			bool createDlls( const std::string& path ) const;
			bool createIniFiles( const std::string& path ) const;

			#if defined(LUDII_SYSTEM_WINDOWS)
			bool createVisualStudio2015Environment( const std::string& path ) const;
			bool createVisualStudio2015ProjectFile( const std::string& path ) const;
			#endif

			void handleRootNode_name( std::string value );

			std::string _name;
			std::string _pathToProjectDirectory;
			std::string _projectFileName;

			// XML root node map
			typedef void( ludii::editor::Project::*xmlNodeValueFunction )( std::string );
			std::map<std::string, xmlNodeValueFunction> _rootNodeFunctions;
		};
	}
}

#endif // LUDII_EDITOR_PROJECT_HPP