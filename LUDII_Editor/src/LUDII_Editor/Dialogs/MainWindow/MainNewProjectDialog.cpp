#include "MainNewProjectDialog.hpp"

#include <QAbstractButton>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>

#include <QFileInfo>

#include <LUDII_Editor/Project.hpp>

#include <iostream>
#include <sstream>

ludii::editor::MainNewProjectDialog::MainNewProjectDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	QObject::connect( ui.buttonBox, &QDialogButtonBox::clicked, this, &ludii::editor::MainNewProjectDialog::onDialogButtonClick );
	QObject::connect( ui.projectPathButton, &QToolButton::clicked, this, &ludii::editor::MainNewProjectDialog::onPathButtonClick );
}

ludii::editor::MainNewProjectDialog::~MainNewProjectDialog()
{
}

void ludii::editor::MainNewProjectDialog::onDialogButtonClick( QAbstractButton* button )
{
	QPushButton* pressedButton = reinterpret_cast<QPushButton*>( button );
	if ( pressedButton == ui.buttonBox->button( QDialogButtonBox::Ok ) )
	{
		std::cout << "Clicked \"Ok\"\n";

		bool doesPathHaveProject = ludii::editor::Project::doesDirectoryContainProject( ui.projectPathEdit->text().toStdString() );
		if ( doesPathHaveProject )
		{
			std::stringstream warningMessageStream;
			warningMessageStream << "The given location at \"" << ui.projectPathEdit->text().toStdString() << "\" already contains a LUDII project!";
			std::cout << "Error: " << warningMessageStream.str() << "\n";

			QMessageBox warningPopup;
			warningPopup.critical( this, "Error!", warningMessageStream.str().c_str() );
			warningPopup.show();
		}
		else
		{
			ludii::editor::Project project;
			project.setName( ui.projectNameEdit->text().toStdString() );
			if ( project.saveToFile( ui.projectPathEdit->text().toStdString(), true ) )
			{
				std::cout << "New project file has successfully been created at \"" << ui.projectPathEdit->text().toStdString() << "\"\n";
				emit onProjectCreatedSuccessfully( ui.projectPathEdit->text() );
			}
			else
			{
				std::cout << "Error: There has been an unexpected error while creating the new project file!\n";
				QMessageBox warningPopup;
				warningPopup.critical( this, "Error!", "There has been an error while creating the new project file!" );
				warningPopup.show();
			}

			close();
		}
	}
	else if ( pressedButton == ui.buttonBox->button( QDialogButtonBox::Cancel ) )
	{
		std::cout << "Clicked \"Cancel\"\n";
		close();
	}
}

void ludii::editor::MainNewProjectDialog::onPathButtonClick( bool checked )
{
	std::cout << "Clicked \"projectPathButton\"\n";

	QString startingPath = QStandardPaths::locate( QStandardPaths::DocumentsLocation, QString(), QStandardPaths::LocateDirectory );
	QString path = QFileDialog::getExistingDirectory( this, "Choose Project Location", startingPath );
	ui.projectPathEdit->setText( path );
}
