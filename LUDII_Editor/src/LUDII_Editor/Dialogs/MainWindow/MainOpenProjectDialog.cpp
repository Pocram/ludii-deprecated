#include "MainOpenProjectDialog.hpp"

#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>

#include <iostream>
#include <sstream>

ludii::editor::MainOpenProjectDialog::MainOpenProjectDialog( QWidget *parent ) :
	QDialog( parent )
{
	ui.setupUi( this );

	QObject::connect( ui.buttonBox, &QDialogButtonBox::clicked, this, &ludii::editor::MainOpenProjectDialog::onDialogButtonClick );
	QObject::connect( ui.pathButton, &QToolButton::clicked, this, &ludii::editor::MainOpenProjectDialog::onPathButtonClick );
}

ludii::editor::MainOpenProjectDialog::~MainOpenProjectDialog()
{
}

void ludii::editor::MainOpenProjectDialog::onDialogButtonClick( QAbstractButton* button )
{
	QPushButton* pressedButton = reinterpret_cast<QPushButton*>( button );
	if ( pressedButton == ui.buttonBox->button( QDialogButtonBox::Ok ) )
	{
		std::cout << "Clicked \"Ok\"\n";

		bool doesPathHaveProject = ludii::editor::Project::doesDirectoryContainProject( ui.pathEdit->text().toStdString() );
		if ( doesPathHaveProject == false )
		{
			std::stringstream warningMessageStream;
			warningMessageStream << "The given location at \"" << ui.pathEdit->text().toStdString() << "\" does not contain a LUDII project!";
			std::cout << "Error: " << warningMessageStream.str() << "\n";

			QMessageBox warningPopup;
			warningPopup.critical( this, "Error!", warningMessageStream.str().c_str() );
			warningPopup.show();
		}
		else
		{
			ludii::editor::Project project;
			if ( project.loadFromDirectory( ui.pathEdit->text().toStdString() ) )
			{
				emit onProjectOpen( project );
			}
			else
			{
				QMessageBox warningPopup;
				warningPopup.critical( this, "Error!", "There has been an error when opening the project file!" );
				warningPopup.show();
			}
			close();
		}
	}
	else if ( pressedButton == ui.buttonBox->button( QDialogButtonBox::Cancel ) )
	{
		std::cout << "Clicked \"Cancel\"\n";
		close();
	}
}

void ludii::editor::MainOpenProjectDialog::onPathButtonClick( bool checked )
{
	std::cout << "Clicked \"pathButton\"\n";

	QString startingPath = QStandardPaths::locate( QStandardPaths::DocumentsLocation, QString(), QStandardPaths::LocateDirectory );
	QString path = QFileDialog::getExistingDirectory( this, "Choose Project Location", startingPath );
	ui.pathEdit->setText( path );
}
