#ifndef MAINOPENPROJECTDIALOG_HPP
#define MAINOPENPROJECTDIALOG_HPP

#include <QDialog>
#include "ui_MainOpenProjectDialog.h"

#include <LUDII_Editor/Project.hpp>

namespace ludii
{
	namespace editor
	{
		class MainOpenProjectDialog : public QDialog
		{
			Q_OBJECT

		public:
			MainOpenProjectDialog( QWidget *parent = 0 );
			~MainOpenProjectDialog();


		signals:
			void onProjectOpen( ludii::editor::Project project );


		private:
			Ui::MainOpenProjectDialog ui;


		private slots:
			void onDialogButtonClick( QAbstractButton* button );
			void onPathButtonClick( bool checked = false );
		};
	}
}

#endif // MAINOPENPROJECTDIALOG_HPP
