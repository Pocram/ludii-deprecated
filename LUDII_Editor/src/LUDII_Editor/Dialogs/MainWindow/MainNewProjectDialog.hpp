#ifndef MAINNEWPROJECTDIALOG_HPP
#define MAINNEWPROJECTDIALOG_HPP

#include <QDialog>
#include "ui_MainNewProjectDialog.h"

namespace ludii
{
	namespace editor
	{
		class MainNewProjectDialog : public QDialog
		{
			Q_OBJECT

		public:
			MainNewProjectDialog( QWidget *parent = 0 );
			~MainNewProjectDialog();


		signals:
			void onProjectCreatedSuccessfully( QString pathToProjectFile );


		private:
			Ui::MainNewProjectDialog ui;


		private slots:
			void onDialogButtonClick( QAbstractButton* button );
			void onPathButtonClick( bool checked = false );
		};
	}
}

#endif // MAINNEWPROJECTDIALOG_HPP
