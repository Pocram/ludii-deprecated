#ifndef GAMEOBJECTHIERARCHYWIDGET_HPP
#define GAMEOBJECTHIERARCHYWIDGET_HPP

#include <QWidget>
#include "ui_GameObjectHierarchyWidget.h"

namespace ludii
{
	namespace editor
	{
		class GameObjectHierarchyWidget : public QWidget
		{
			Q_OBJECT

		public:
			GameObjectHierarchyWidget( QWidget *parent = 0 );
			~GameObjectHierarchyWidget();

		private:
			Ui::GameObjectHierarchyWidget ui;
		};
	}
}

#endif // GAMEOBJECTHIERARCHYWIDGET_HPP
