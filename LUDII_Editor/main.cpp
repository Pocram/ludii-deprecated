#include "LudiiEditorMainWindow.hpp"
#include <QtWidgets/QApplication>

#include <iostream>

#include <QFile>
#include <QTextStream>

int main(int argc, char *argv[])
{
	QApplication application( argc, argv );

	QFile styleSheet( "Resources/skins/qdarkStyle/style.qss" );
	if ( styleSheet.exists() )
	{
		styleSheet.open( QFile::ReadOnly | QFile::Text );
		QTextStream ts( &styleSheet );
		application.setStyleSheet( ts.readAll() );
	}
	else
	{
		std::cout << "Stylesheet could not be loaded. Enjoy being blinded!\n";
	}


	ludii::editor::LudiiEditorMainWindow mainWindow;
	mainWindow.show();
	
	return application.exec();
}
