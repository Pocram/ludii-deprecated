/********************************************************************************
** Form generated from reading UI file 'GameObjectHierarchyWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMEOBJECTHIERARCHYWIDGET_H
#define UI_GAMEOBJECTHIERARCHYWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GameObjectHierarchyWidget
{
public:
    QGridLayout *gridLayout;
    QTreeWidget *treeWidget;

    void setupUi(QWidget *GameObjectHierarchyWidget)
    {
        if (GameObjectHierarchyWidget->objectName().isEmpty())
            GameObjectHierarchyWidget->setObjectName(QStringLiteral("GameObjectHierarchyWidget"));
        GameObjectHierarchyWidget->resize(841, 458);
        gridLayout = new QGridLayout(GameObjectHierarchyWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        treeWidget = new QTreeWidget(GameObjectHierarchyWidget);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(__qtreewidgetitem);
        new QTreeWidgetItem(treeWidget);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setStyleSheet(QStringLiteral(""));
        treeWidget->setFrameShape(QFrame::NoFrame);
        treeWidget->setTabKeyNavigation(true);
        treeWidget->setAllColumnsShowFocus(false);
        treeWidget->setHeaderHidden(true);
        treeWidget->header()->setVisible(false);

        gridLayout->addWidget(treeWidget, 0, 0, 1, 1);


        retranslateUi(GameObjectHierarchyWidget);

        QMetaObject::connectSlotsByName(GameObjectHierarchyWidget);
    } // setupUi

    void retranslateUi(QWidget *GameObjectHierarchyWidget)
    {
        GameObjectHierarchyWidget->setWindowTitle(QApplication::translate("GameObjectHierarchyWidget", "GameObjectHierarchyWidget", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("GameObjectHierarchyWidget", "1", 0));

        const bool __sortingEnabled = treeWidget->isSortingEnabled();
        treeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("GameObjectHierarchyWidget", "TestObject", 0));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(0, QApplication::translate("GameObjectHierarchyWidget", "AChild!", 0));
        QTreeWidgetItem *___qtreewidgetitem3 = treeWidget->topLevelItem(1);
        ___qtreewidgetitem3->setText(0, QApplication::translate("GameObjectHierarchyWidget", "OtherObject", 0));
        treeWidget->setSortingEnabled(__sortingEnabled);

    } // retranslateUi

};

namespace Ui {
    class GameObjectHierarchyWidget: public Ui_GameObjectHierarchyWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMEOBJECTHIERARCHYWIDGET_H
