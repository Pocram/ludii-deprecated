/****************************************************************************
** Meta object code from reading C++ file 'MainNewProjectDialog.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/LUDII_Editor/Dialogs/MainWindow/MainNewProjectDialog.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainNewProjectDialog.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ludii__editor__MainNewProjectDialog_t {
    QByteArrayData data[9];
    char stringdata0[154];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ludii__editor__MainNewProjectDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ludii__editor__MainNewProjectDialog_t qt_meta_stringdata_ludii__editor__MainNewProjectDialog = {
    {
QT_MOC_LITERAL(0, 0, 35), // "ludii::editor::MainNewProject..."
QT_MOC_LITERAL(1, 36, 28), // "onProjectCreatedSuccessfully"
QT_MOC_LITERAL(2, 65, 0), // ""
QT_MOC_LITERAL(3, 66, 17), // "pathToProjectFile"
QT_MOC_LITERAL(4, 84, 19), // "onDialogButtonClick"
QT_MOC_LITERAL(5, 104, 16), // "QAbstractButton*"
QT_MOC_LITERAL(6, 121, 6), // "button"
QT_MOC_LITERAL(7, 128, 17), // "onPathButtonClick"
QT_MOC_LITERAL(8, 146, 7) // "checked"

    },
    "ludii::editor::MainNewProjectDialog\0"
    "onProjectCreatedSuccessfully\0\0"
    "pathToProjectFile\0onDialogButtonClick\0"
    "QAbstractButton*\0button\0onPathButtonClick\0"
    "checked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ludii__editor__MainNewProjectDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   37,    2, 0x08 /* Private */,
       7,    1,   40,    2, 0x08 /* Private */,
       7,    0,   43,    2, 0x28 /* Private | MethodCloned */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void,

       0        // eod
};

void ludii::editor::MainNewProjectDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainNewProjectDialog *_t = static_cast<MainNewProjectDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onProjectCreatedSuccessfully((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->onDialogButtonClick((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 2: _t->onPathButtonClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->onPathButtonClick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainNewProjectDialog::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainNewProjectDialog::onProjectCreatedSuccessfully)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject ludii::editor::MainNewProjectDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ludii__editor__MainNewProjectDialog.data,
      qt_meta_data_ludii__editor__MainNewProjectDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ludii::editor::MainNewProjectDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ludii::editor::MainNewProjectDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ludii__editor__MainNewProjectDialog.stringdata0))
        return static_cast<void*>(const_cast< MainNewProjectDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ludii::editor::MainNewProjectDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void ludii::editor::MainNewProjectDialog::onProjectCreatedSuccessfully(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
