/****************************************************************************
** Meta object code from reading C++ file 'LudiiEditorMainWindow.hpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../LudiiEditorMainWindow.hpp"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LudiiEditorMainWindow.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ludii__editor__LudiiEditorMainWindow_t {
    QByteArrayData data[10];
    char stringdata0[153];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ludii__editor__LudiiEditorMainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ludii__editor__LudiiEditorMainWindow_t qt_meta_stringdata_ludii__editor__LudiiEditorMainWindow = {
    {
QT_MOC_LITERAL(0, 0, 36), // "ludii::editor::LudiiEditorMai..."
QT_MOC_LITERAL(1, 37, 10), // "setProject"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 22), // "ludii::editor::Project"
QT_MOC_LITERAL(4, 72, 7), // "project"
QT_MOC_LITERAL(5, 80, 23), // "onProjectHasBeenCreated"
QT_MOC_LITERAL(6, 104, 13), // "pathToProject"
QT_MOC_LITERAL(7, 118, 18), // "onMenuBarTriggered"
QT_MOC_LITERAL(8, 137, 8), // "QAction*"
QT_MOC_LITERAL(9, 146, 6) // "action"

    },
    "ludii::editor::LudiiEditorMainWindow\0"
    "setProject\0\0ludii::editor::Project\0"
    "project\0onProjectHasBeenCreated\0"
    "pathToProject\0onMenuBarTriggered\0"
    "QAction*\0action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ludii__editor__LudiiEditorMainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       5,    1,   32,    2, 0x0a /* Public */,
       7,    1,   35,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, 0x80000000 | 8,    9,

       0        // eod
};

void ludii::editor::LudiiEditorMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LudiiEditorMainWindow *_t = static_cast<LudiiEditorMainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setProject((*reinterpret_cast< ludii::editor::Project(*)>(_a[1]))); break;
        case 1: _t->onProjectHasBeenCreated((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->onMenuBarTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject ludii::editor::LudiiEditorMainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ludii__editor__LudiiEditorMainWindow.data,
      qt_meta_data_ludii__editor__LudiiEditorMainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ludii::editor::LudiiEditorMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ludii::editor::LudiiEditorMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ludii__editor__LudiiEditorMainWindow.stringdata0))
        return static_cast<void*>(const_cast< LudiiEditorMainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ludii::editor::LudiiEditorMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
