/********************************************************************************
** Form generated from reading UI file 'MainNewProjectDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINNEWPROJECTDIALOG_H
#define UI_MAINNEWPROJECTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_MainNewProjectDialog
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QLineEdit *projectNameEdit;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *projectPathEdit;
    QToolButton *projectPathButton;

    void setupUi(QDialog *MainNewProjectDialog)
    {
        if (MainNewProjectDialog->objectName().isEmpty())
            MainNewProjectDialog->setObjectName(QStringLiteral("MainNewProjectDialog"));
        MainNewProjectDialog->setWindowModality(Qt::WindowModal);
        MainNewProjectDialog->resize(480, 99);
        MainNewProjectDialog->setMinimumSize(QSize(480, 99));
        MainNewProjectDialog->setMaximumSize(QSize(480, 99));
        gridLayout = new QGridLayout(MainNewProjectDialog);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        buttonBox = new QDialogButtonBox(MainNewProjectDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 2, 0, 1, 1);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(MainNewProjectDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        projectNameEdit = new QLineEdit(MainNewProjectDialog);
        projectNameEdit->setObjectName(QStringLiteral("projectNameEdit"));

        horizontalLayout->addWidget(projectNameEdit);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);

        label_2 = new QLabel(MainNewProjectDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        projectPathEdit = new QLineEdit(MainNewProjectDialog);
        projectPathEdit->setObjectName(QStringLiteral("projectPathEdit"));

        horizontalLayout_2->addWidget(projectPathEdit);

        projectPathButton = new QToolButton(MainNewProjectDialog);
        projectPathButton->setObjectName(QStringLiteral("projectPathButton"));

        horizontalLayout_2->addWidget(projectPathButton);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout_2);


        gridLayout->addLayout(formLayout, 0, 0, 1, 1);


        retranslateUi(MainNewProjectDialog);

        QMetaObject::connectSlotsByName(MainNewProjectDialog);
    } // setupUi

    void retranslateUi(QDialog *MainNewProjectDialog)
    {
        MainNewProjectDialog->setWindowTitle(QApplication::translate("MainNewProjectDialog", "New Project", 0));
        label->setText(QApplication::translate("MainNewProjectDialog", "Project Name:", 0));
        label_2->setText(QApplication::translate("MainNewProjectDialog", "Location:", 0));
        projectPathButton->setText(QApplication::translate("MainNewProjectDialog", "...", 0));
    } // retranslateUi

};

namespace Ui {
    class MainNewProjectDialog: public Ui_MainNewProjectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINNEWPROJECTDIALOG_H
