/********************************************************************************
** Form generated from reading UI file 'LudiiEditorMainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LUDIIEDITORMAINWINDOW_H
#define UI_LUDIIEDITORMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "src/LUDII_Editor/Widgets/MainWindow/GameObjectHierarchyWidget.hpp"

QT_BEGIN_NAMESPACE

class Ui_LudiiEditorMainWindowClass
{
public:
    QAction *filePreferences;
    QAction *fileNewProject;
    QAction *fileOpenProject;
    QAction *fileSave;
    QAction *fileExit;
    QAction *actionPreferences_2;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_5;
    QWidget *gameView;
    QTabWidget *tabWidget_4;
    QWidget *assetBrowser;
    QWidget *output;
    QWidget *objectGroup;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    ludii::editor::GameObjectHierarchyWidget *gameObjectTree;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_4;
    QWidget *gameObjectInspector;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuHelp;
    QMenu *menuProject;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *LudiiEditorMainWindowClass)
    {
        if (LudiiEditorMainWindowClass->objectName().isEmpty())
            LudiiEditorMainWindowClass->setObjectName(QStringLiteral("LudiiEditorMainWindowClass"));
        LudiiEditorMainWindowClass->resize(924, 649);
        QIcon icon;
        icon.addFile(QStringLiteral("C:/Users/Marco/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        LudiiEditorMainWindowClass->setWindowIcon(icon);
        filePreferences = new QAction(LudiiEditorMainWindowClass);
        filePreferences->setObjectName(QStringLiteral("filePreferences"));
        fileNewProject = new QAction(LudiiEditorMainWindowClass);
        fileNewProject->setObjectName(QStringLiteral("fileNewProject"));
        fileOpenProject = new QAction(LudiiEditorMainWindowClass);
        fileOpenProject->setObjectName(QStringLiteral("fileOpenProject"));
        fileSave = new QAction(LudiiEditorMainWindowClass);
        fileSave->setObjectName(QStringLiteral("fileSave"));
        fileExit = new QAction(LudiiEditorMainWindowClass);
        fileExit->setObjectName(QStringLiteral("fileExit"));
        actionPreferences_2 = new QAction(LudiiEditorMainWindowClass);
        actionPreferences_2->setObjectName(QStringLiteral("actionPreferences_2"));
        centralWidget = new QWidget(LudiiEditorMainWindowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(3);
        sizePolicy.setVerticalStretch(3);
        sizePolicy.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy);
        groupBox_3->setMinimumSize(QSize(650, 400));
        gridLayout_5 = new QGridLayout(groupBox_3);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gameView = new QWidget(groupBox_3);
        gameView->setObjectName(QStringLiteral("gameView"));
        gameView->setStyleSheet(QStringLiteral("background: #000;"));

        gridLayout_5->addWidget(gameView, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox_3, 0, 1, 2, 1);

        tabWidget_4 = new QTabWidget(centralWidget);
        tabWidget_4->setObjectName(QStringLiteral("tabWidget_4"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(tabWidget_4->sizePolicy().hasHeightForWidth());
        tabWidget_4->setSizePolicy(sizePolicy1);
        tabWidget_4->setMinimumSize(QSize(0, 100));
        tabWidget_4->setMovable(true);
        assetBrowser = new QWidget();
        assetBrowser->setObjectName(QStringLiteral("assetBrowser"));
        tabWidget_4->addTab(assetBrowser, QString());
        output = new QWidget();
        output->setObjectName(QStringLiteral("output"));
        tabWidget_4->addTab(output, QString());

        gridLayout->addWidget(tabWidget_4, 2, 1, 1, 1);

        objectGroup = new QWidget(centralWidget);
        objectGroup->setObjectName(QStringLiteral("objectGroup"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(1);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(objectGroup->sizePolicy().hasHeightForWidth());
        objectGroup->setSizePolicy(sizePolicy2);
        objectGroup->setMinimumSize(QSize(250, 0));
        objectGroup->setMaximumSize(QSize(400, 16777215));
        verticalLayout_2 = new QVBoxLayout(objectGroup);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(objectGroup);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(0, 150));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gameObjectTree = new ludii::editor::GameObjectHierarchyWidget(groupBox);
        gameObjectTree->setObjectName(QStringLiteral("gameObjectTree"));

        gridLayout_3->addWidget(gameObjectTree, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox);

        groupBox_2 = new QGroupBox(objectGroup);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(2);
        sizePolicy3.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy3);
        groupBox_2->setMinimumSize(QSize(0, 250));
        gridLayout_4 = new QGridLayout(groupBox_2);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gameObjectInspector = new QWidget(groupBox_2);
        gameObjectInspector->setObjectName(QStringLiteral("gameObjectInspector"));

        gridLayout_4->addWidget(gameObjectInspector, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox_2);


        gridLayout->addWidget(objectGroup, 0, 2, 3, 1);

        LudiiEditorMainWindowClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(LudiiEditorMainWindowClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 924, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        menuProject = new QMenu(menuBar);
        menuProject->setObjectName(QStringLiteral("menuProject"));
        LudiiEditorMainWindowClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(LudiiEditorMainWindowClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        LudiiEditorMainWindowClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuProject->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(fileNewProject);
        menuFile->addAction(fileOpenProject);
        menuFile->addAction(fileSave);
        menuFile->addSeparator();
        menuFile->addAction(filePreferences);
        menuFile->addSeparator();
        menuFile->addAction(fileExit);
        menuProject->addAction(actionPreferences_2);

        retranslateUi(LudiiEditorMainWindowClass);

        tabWidget_4->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LudiiEditorMainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *LudiiEditorMainWindowClass)
    {
        LudiiEditorMainWindowClass->setWindowTitle(QApplication::translate("LudiiEditorMainWindowClass", "LUDII (Unnamed Project)", 0));
        filePreferences->setText(QApplication::translate("LudiiEditorMainWindowClass", "Preferences...", 0));
        fileNewProject->setText(QApplication::translate("LudiiEditorMainWindowClass", "New Project...", 0));
        fileOpenProject->setText(QApplication::translate("LudiiEditorMainWindowClass", "Open Project...", 0));
        fileSave->setText(QApplication::translate("LudiiEditorMainWindowClass", "Save", 0));
        fileExit->setText(QApplication::translate("LudiiEditorMainWindowClass", "Exit", 0));
        actionPreferences_2->setText(QApplication::translate("LudiiEditorMainWindowClass", "Preferences...", 0));
        groupBox_3->setTitle(QApplication::translate("LudiiEditorMainWindowClass", "Scene View", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(assetBrowser), QApplication::translate("LudiiEditorMainWindowClass", "Asset Browser", 0));
        tabWidget_4->setTabText(tabWidget_4->indexOf(output), QApplication::translate("LudiiEditorMainWindowClass", "Output", 0));
        groupBox->setTitle(QApplication::translate("LudiiEditorMainWindowClass", "Game Objects", 0));
        groupBox_2->setTitle(QApplication::translate("LudiiEditorMainWindowClass", "Inspector", 0));
        menuFile->setTitle(QApplication::translate("LudiiEditorMainWindowClass", "File", 0));
        menuEdit->setTitle(QApplication::translate("LudiiEditorMainWindowClass", "Edit", 0));
        menuHelp->setTitle(QApplication::translate("LudiiEditorMainWindowClass", "Help", 0));
        menuProject->setTitle(QApplication::translate("LudiiEditorMainWindowClass", "Project", 0));
    } // retranslateUi

};

namespace Ui {
    class LudiiEditorMainWindowClass: public Ui_LudiiEditorMainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LUDIIEDITORMAINWINDOW_H
