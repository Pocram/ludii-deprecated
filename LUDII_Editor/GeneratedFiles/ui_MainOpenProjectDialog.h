/********************************************************************************
** Form generated from reading UI file 'MainOpenProjectDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINOPENPROJECTDIALOG_H
#define UI_MAINOPENPROJECTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_MainOpenProjectDialog
{
public:
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QLineEdit *pathEdit;
    QToolButton *pathButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *MainOpenProjectDialog)
    {
        if (MainOpenProjectDialog->objectName().isEmpty())
            MainOpenProjectDialog->setObjectName(QStringLiteral("MainOpenProjectDialog"));
        MainOpenProjectDialog->setWindowModality(Qt::WindowModal);
        MainOpenProjectDialog->resize(465, 71);
        MainOpenProjectDialog->setMinimumSize(QSize(465, 71));
        MainOpenProjectDialog->setMaximumSize(QSize(465, 71));
        gridLayout = new QGridLayout(MainOpenProjectDialog);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(MainOpenProjectDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pathEdit = new QLineEdit(MainOpenProjectDialog);
        pathEdit->setObjectName(QStringLiteral("pathEdit"));

        horizontalLayout->addWidget(pathEdit);

        pathButton = new QToolButton(MainOpenProjectDialog);
        pathButton->setObjectName(QStringLiteral("pathButton"));

        horizontalLayout->addWidget(pathButton);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout);


        gridLayout->addLayout(formLayout, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(MainOpenProjectDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);


        retranslateUi(MainOpenProjectDialog);

        QMetaObject::connectSlotsByName(MainOpenProjectDialog);
    } // setupUi

    void retranslateUi(QDialog *MainOpenProjectDialog)
    {
        MainOpenProjectDialog->setWindowTitle(QApplication::translate("MainOpenProjectDialog", "Open Project", 0));
        label->setText(QApplication::translate("MainOpenProjectDialog", "Project Location:", 0));
        pathButton->setText(QApplication::translate("MainOpenProjectDialog", "...", 0));
    } // retranslateUi

};

namespace Ui {
    class MainOpenProjectDialog: public Ui_MainOpenProjectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINOPENPROJECTDIALOG_H
