#include "LudiiEditorMainWindow.hpp"
#include <iostream>

ludii::editor::LudiiEditorMainWindow::LudiiEditorMainWindow( QWidget *parent ) :
	QMainWindow( parent ),
	_currentProject( nullptr ),
	_newProjectDialog( nullptr ),
	_openProjectDialog( nullptr )
{
	ui.setupUi(this);

	QObject::connect( ui.menuBar, &QMenuBar::triggered, this, &ludii::editor::LudiiEditorMainWindow::onMenuBarTriggered );

	initializeMenuBarTriggers();
}

ludii::editor::LudiiEditorMainWindow::~LudiiEditorMainWindow()
{

}

ludii::editor::Project& ludii::editor::LudiiEditorMainWindow::getCurrentProject()
{
	return *_currentProject;
}

const ludii::editor::Project& ludii::editor::LudiiEditorMainWindow::getCurrentProject() const
{
	return *_currentProject;
}

void ludii::editor::LudiiEditorMainWindow::setProject( ludii::editor::Project project )
{
	_currentProject = new ludii::editor::Project( project );
	QMainWindow::setWindowTitle( "LUDII (" + QString( project.getName().c_str() ) + ")" );
}

void ludii::editor::LudiiEditorMainWindow::onProjectHasBeenCreated( QString pathToProject )
{
	ludii::editor::Project newProject;
	if ( newProject.loadFromDirectory( pathToProject.toStdString() ) )
	{
		setProject( newProject );
	}
}

void ludii::editor::LudiiEditorMainWindow::unloadCurrentProject()
{
	_currentProject = nullptr;
}

void ludii::editor::LudiiEditorMainWindow::initializeMenuBarTriggers()
{
	_menuTriggerActions.insert_or_assign( "fileNewProject", &ludii::editor::LudiiEditorMainWindow::onMenuClick_newProject );
	_menuTriggerActions.insert_or_assign( "fileOpenProject", &ludii::editor::LudiiEditorMainWindow::onMenuClick_openProject );
}

void ludii::editor::LudiiEditorMainWindow::onMenuClick_newProject()
{
	delete _newProjectDialog;
	_newProjectDialog = new ludii::editor::MainNewProjectDialog( this );
	QObject::connect( _newProjectDialog, &ludii::editor::MainNewProjectDialog::onProjectCreatedSuccessfully, this, &ludii::editor::LudiiEditorMainWindow::onProjectHasBeenCreated );
	
	_newProjectDialog->show();
}

void ludii::editor::LudiiEditorMainWindow::onMenuClick_openProject()
{
	delete _openProjectDialog;
	_openProjectDialog = new ludii::editor::MainOpenProjectDialog( this );
	QObject::connect( _openProjectDialog, &ludii::editor::MainOpenProjectDialog::onProjectOpen, this, &ludii::editor::LudiiEditorMainWindow::setProject );

	_openProjectDialog->show();
}

void ludii::editor::LudiiEditorMainWindow::onMenuBarTriggered( QAction* action )
{
	std::cout << "Clicked menu: \"" << action->objectName().toStdString() << "\"\n";
	if ( _menuTriggerActions.find( action->objectName() ) != _menuTriggerActions.end() )
	{
		menuTriggerActionFunction function = _menuTriggerActions[action->objectName()];
		( this->*function )( );
	}
}
