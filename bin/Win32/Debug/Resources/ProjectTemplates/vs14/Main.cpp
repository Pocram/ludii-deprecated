#include <iostream>

#include <LUDII/Application/Game.hpp>

int main( int argc, char* argv[] ) 
{
    ludii::Game& game = ludii::Game::getInstance();
    game.initialize(argc, argv);
    game.start();
    return 0;
}